#### 监控文件或目录
```shell
inotifywait -mrq --timefmt '%y-%m-%d %H:%M' --format '%T %e %w%f' -e create -e delete -e move -e moved_to -e moved_from -e delete_self  /datac/img/
# 监控后台运行并输出到文件
inotifywait -mrqd --timefmt '%y-%m-%d %H:%M' --format '%T %e %w%f' -e create -e delete -e move -e moved_to -e moved_from -e delete_self -o /var/log/data  /datac/img/
```
#### 隐藏进程
```
mount -o bind /tmp/moniter/ /proc/20113
#抓出隐藏的进程
cat /proc/$$/mountinfo
cat /proc/mounts
#现形
umount /proc/20113

```
#### systemd最新版修改DNS
```
vi /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved
resolvectl status
resolvectl query dns.rubyfish.cn -t A
```
### ssh登录和文件上传
```
#文件上传
scp -P 51985 caddy_v1.0.0_linux_amd64.tar.gz root@148.70.194.119:/var/www/html/caddy_v1.0.0_linux_amd64.tar.gz
#-r 参数表示递归复制
scp -P 2222 -r /home/lnmp0.4/ root@www.vpser.net:/root/lnmp0.4/
#文件下载
scp -P 2222 root@www.vpser.net:/root/lnmp0.4.tar.gz /home/lnmp0.4.tar.gz
#登录
ssh -p 51985 root@148.70.194.119
#代理远程服务器端口到本地
##run at localhost 
ssh -CqTnN -L 0.0.0.0:6379:remoteIp:6379 remoteUser@remoteIp -p remoteSSHPort
##connect redis local
localhost:6379
#在本地连接A服务器然后访问B服务器
ssh -fN -L3307:192.168.3.51:3306 -p5122 wz@111.112.113.114
ssh -fN -L(要绑定到的本地端口):(服务器B的Host):(服务器B上要访问的端口号) -p(服务器A的端口，默认为22) (服务器A的账户):(服务器A的Host)

```
### 在线关闭一个tcp socket连接
```
1.使用 netstat 或 ps 找到进程
2.使用 lsof 找到进程45059打开的所有文件描述符，并找到对应的socket连接
3.gdb 连接到进程 gdb -p 45059
4.关闭socket连接 (gdb) call close(12u)
```

### 解压提取electron的asar文件
```
 npx  asar extract .\electron.asar ./
```

### 查看 Shell 中我们最常用的 10 条命令

```bash
$ history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10
 1  429  16.6473%    ls
 2  235  9.11913%    vim
 3  204  7.91618%    gss
 4  186  7.21769%    yarn
 5  169  6.55801%    cd
 6  86   3.33721%    j
 7  75   2.91036%    git
 8  64   2.48351%    subl
 9  49   1.90144%    rg
10  49   1.90144%    glog
```

### 配置历史命令中有详细的时间戳
编辑/etc/profile文件，在里面加入下面内容：
```bash
export  HISTTIMEFORMAT="`whoami` : %F %T :"
```

### ssh登陆卡住(expecting SSH2_MSG_KEX_ECDH_REPLY）
```bash
ifconfig 网卡名 mtu 1200
```
改成1400也可。