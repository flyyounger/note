#### 清除残余配置文件

```
dpkg -l |grep ^rc|awk '{print $2}' |sudo xargs dpkg -P
```
#### 修改这个值控制系统保留内存

可以达到让服务器看着有很多内存但没法用的效果
```
sysctl -a|grep vm.min_free
vm.min_free_kbytes
sysctl -w "vm.min_free_kbytes = 67584"
```
#### 设置CPU为省电模式

```
sudo apt-get install cpufrequtils
cpufreq-info
cpufreq-set -g powersave
```
#### 让基于GTK3的应用不报service not found的错误

```
export XDG_CONFIG_DIRS=1
```
#### 开启BBR
```
#首先确认内核版本大于4.9
uname -r
vi /etc/sysctl.conf
# TCP-BBR
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr
sysctl -p
# 查看当前网络堆栈的默认排队机制
sysctl net.core.default_qdisc
# 查看当前TCP BBR拥塞控制算法
sysctl net.ipv4.tcp_congestion_control
# 查看tcp_bbr内核模块是否启动
lsmod | grep bbr
```
#### 修改文件监听数

```
sudo sysctl fs.inotify.max_user_watches=524288
sudo sysctl -p --system
echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
```
#### 关闭无线网络省电模式防止断线

```
#查看正在使用的无线网络连接
ifconfig
#关闭该连接的节电模式（wlp3s0就是对应无线网络链接的名字）
sudo iw dev wlp3s0 set power_save off
```
#### 丢弃输出,后台执行

```
nohup java -jar xxxx.jar >/dev/null 2>&1 &
```
#### 让终端输入密码有星号回显

```
编辑 /etc/sudoers
找到下面这行：
Defaults env_reset
在该行的末尾添加一个额外的单词 ,pwfeedback
```
#### sudo命令执行太慢的解决方案
用"hostname"命令查看当前主机的主机名称，用vi打开"/etc/hosts"文件，并将主机名加入到 "127.0.0.1"这行中

#### 把文件打包拷贝到远程

```
# 通常做法
scp -r dir/ user@remote-server:/
# 骚操作
tar -cJvf - dir/ | ssh user@remote-server "tar -xJvf -"
```
#### 拷贝公钥到远程服务器

```
# 通常做法
scp ~/.ssh/id_rsa.pub user@remote-server:~
ssh user@remote-server
mkdir .ssh && cat id_rsa.pub >> .ssh/authorized_keys

# 骚操作
ssh-copy-id user@remote-server
```

#### 批量搜索文件内容

```
find 目录名 -type f | xargs cat | grep 要搜索的内容
```
#### 把后一个文件的时间修改成和前一个相同
```
touch -acmr /bin/ls /etc/sh.conf
```

#### linux下特殊文件解压
```
#bin文件解压
# 从268行起提取二进制文件
tail -n +268 jre-for-linux.bin >install.sfx
# 因为是sfx格式，就用7z解压
7z x install.sfx

#解压appImage文件
./typora.AppImage --appimage-extract

#解压deb文件
dpkg -x xxx.deb softd
或者
ar -vx xx.deb
这会解压出3个文件debian-binary，control.tar.gz，data.tar.gz
然后再
tar -xzvf data.tar.gz

#以.rpm为扩展名的文件解压缩：
rpm2cpio file.rpm | cpio -div
```
#### 自动输入密码登录SSH
```shell
#!/bin/expect

set timeout 10
spawn ssh -l root 47.105.105.92
expect "*password*"
send "Zongka1234\r"
interact
```
#### 修改文件扩展属性
```
# 只能修改user命名空间下的数据
setfattr -n user.checksum -v "3baf" foo.txt
getfattr -d foo.txt
getfattr -d sfd2017-matrix.pdf
setfattr -n user.xdg.origin.url sfd2017-matrix.pdf
setfattr -n user.xdg.referrer.url sfd2017-matrix.pdf
```
#### 调整ext4预留空间
```
# By default, 5% of the filesystem blocks will be reserved for the super-user
tune2fs -m 1.0 /dev/sdb
tune2fs -r 128000 /dev/sdb
```
#### 限单个文件夹大小
```
#创建一个指定大小的磁盘镜像文件：
dd if=/dev/zero of=/root/disk.img bs=2M count=10
#这样就创建了一个大小为20M的磁盘镜像。
#挂载为设备
touch /dev/loop0
losetup /dev/loop0 /root/disk.img
#格式化设备
mkfs.ext4 /dev/loop0
#挂载为文件夹
mkdir /test
mount -t ext4 /dev/loop0 /test
#这样/mnt/disk1这个文件夹只能使用20M的空间。
#卸载
umount /test
losetup -d /dev/loop0
rm -f /root/disk.img
```
#### 硬件探测取证
```
#!/bin/bash
# lshw -c system|grep serial|awk -F ':' '{print $2}'|sed s/[[:space:]]//g
product_serial=$(sudo cat /sys/class/dmi/id/product_serial)
echo "操作系统序列号： $product_serial"
board_serial=$(sudo cat /sys/class/dmi/id/board_serial)
echo "主板序列号： $board_serial"
# sudo hdparm -I /dev/sda|grep "Serial Number"|awk -F ':' '{print $2}'|sed s/[[:space:]]//g
sno=$(hwinfo|grep ID_SERIAL_SHORT|sort -r|head -n 1|awk -F '=' '{print $2}')
echo "硬盘序列号： $sno"
machine_id=$(cat /etc/machine-id)
echo "操作系统uuid： $machine_id"
product=$(cat /sys/class/dmi/id/sys_vendor)
echo "系统提供商： $product"

# dbus-uuidgen --ensure=/etc/machine-id
# lshw -xml
# dmidecode|grep Serial
# dmesg
# hwinfo
```
#### 用vim编辑文件时，去掉其中的^M
```
:%s/^M$//g
注意命令中的^M 是通过键入"CTRL-V CTRL-M"生成的！
```
### redis安全
```
chmod 600 /etc/redis/redis.conf  #假设这是你redis配置文件
#开启Redis密码认证，并设置高复杂度密码
requirepass ed4c39b015b0e46f074dbfd0a9a
#禁用或重命名危险命令
rename-command CONFIG CONFIG_b9fc8327c4dee7
rename-command SHUTDOWN SHUTDOWN_b9fc8327c4dee7
rename-command FLUSHDB ""
rename-command FLUSHALL ""
rename-command KEYS ""
rename-command EVAL ""
#只监听内网
bind 127.0.0.1 192.168.13.12
```