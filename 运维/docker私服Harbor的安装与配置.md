Harbor 官方地址：https://github.com/goharbor/harbor

准备操作命令：

```bash
#安装docker-compose
yum install epel-release
yum install docker-compose
#查看版本
docker-compose version
#下载离线安装包，约600M
wget -c https://github.com/goharbor/harbor/releases/download/v1.9.4/harbor-offline-installer-v1.9.4.tgz
```

解压，修改配置。

**一定要修改默认的port参数，不能是80，而且要修改harbor.yml和docker-compose.yml这两个文件**

```bash
tar -xf harbor-offline-installer-v1.9.4.tgz
cd harbor
#修改配置文件
vim harbor.yml
#以下两个地方需要修改
hostname: chenwork2
port: 8000
vim docker-compose.yml
#这里ports参数也需要修改
volumes:
      - ./common/config/nginx:/etc/nginx:z
    networks:
      - harbor
    dns_search: .
    ports:
      - 8000:8080
    depends_on:
      - registry
      - core
      - portal
      - log
```
安装
```bash
#安装
./install.sh
#重启
docker-compose ps
docker-compose down -v
docker-compose up -d
#重装
docker-compose down -v
./prepare
docker-compose up -d
#启动停止
docker-compose start/stop
```

安装成功，地址：[http://10.180.249.73:8000](http://10.180.249.73:8000/)

登陆，默认用户名 admin，默认密码 Habor12345

添加一个用户

创建一个项目叫 rancher

客户端需要配置下docker

**因为docker默认不支持HTTP，所以需要修改让它支持。但是配置HTTPS比较麻烦，而且需要每个客户端安装证书，更麻烦（除非使用权威证书，而不是自签名证书）**

```bash
#查找 docker.service 所在目录
[root@localhost harbor]# find / -name docker.service -type f
/usr/lib/systemd/system/docker.service
#增加 --insecure-registry harbor.phpdev.com:9010
[root@localhost harbor]# vim /usr/lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd --insecure-registry 10.180.249.73:8000  -H fd:// --containerd=/run/containerd/containerd.soc
#重新加载配置、重启docker
systemctl daemon-reload
systemctl restart docker
```
使用：
```bash
docker login 10.180.249.73:8000
docker tag da86e6ba6ca1 10.180.249.73:8000/rancher/pause:3.1
docker push 10.180.249.73:8000/rancher/pause:3.1
```

