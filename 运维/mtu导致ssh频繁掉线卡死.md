前几天，刚买的HK VPS老是ssh登陆不了，频繁重试很多次才能成功，刚一开始也没太当回事，感觉是网络不稳定被方教授墙了，但80端口还能访问，心想单独针对SSH墙不大可能，于是又一次SSH登录失败的时候，加了-vvv选项看了下，发现卡在了这里
```bash
debug2: mac_setup: found hmac-md5
debug1: kex: server->client aes128-ctr hmac-md5 none
debug2: mac_setup: found hmac-md5
debug1: kex: client->server aes128-ctr hmac-md5 none
debug1: sending SSH2_MSG_KEX_ECDH_INIT
debug1: expecting SSH2_MSG_KEX_ECDH_REPLY
```
就是这一行debug1: expecting SSH2_MSG_KEX_ECDH_REPLY,搜了下，发现是MTU导致的。

怎样探测MTU呢？

```bash
linux下探测MTU值：
ping -s 1460 -M do baidu.com

windows下探测MTU值：
ping -f -l 1460 baidu.com
```

怎样修改MTU呢？

```bash
ifconfig eth0 mtu 1492
```
查看MTU也可用ifconfig命令。

参考：http://www.snailbook.com/faq/mtu-mismatch.auto.html

这么干并不优雅， 可以参考Arch Wiki 这个内容（https://wiki.archlinux.org/title/Sysctl#Enable_MTU_probing）。这里面提到一个内核参数 —— tcp_mtu_probing，启用这项内核功能可以让操作系统根据网络状况自动的调节 MTU 大小，从而在性能与稳定性之间找到一个微妙的平衡，具体的介绍可以看 Cloudflare 这篇介绍（https://blog.cloudflare.com/path-mtu-discovery-in-practice/）

```
vi /etc/sysctl.d/99-sysctl.conf
net.ipv4.tcp_mtu_probing = 1
sudo sysctl --system
```

ref:https://www.cnblogs.com/JacZhu/p/11006738.html

补充：SSH频繁掉线还跟客户端有问题，因为xshell不允许用于商业目的，并且严查版权问题，公司使用的是MobaXterm，默认是没开“ssh keepalive”的，这就需要调整sshd配置，并且打开客户端keep alive机制。