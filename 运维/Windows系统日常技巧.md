#### TAKEOWN
这个软件是win7自带的工具，利用它可以获得对文件或文件夹的所属权，操作如下
```
takeown   /f 文件名
或者
takeown /f /r /d n 文件夹
```
#### ICACLS
这个也是win7下自带的工具，利用它可以获得文件或文件夹的控制权，操作如下
```
icacls c:\windows\* /save AclFile /T  //保存
icacls file /grant Administrator:(D,WDAC)    //赋权
icacls c:\windows\ /restore AclFile     //恢复
```

#### 控制CapsLock灯的明灭：

```
set WshShell = CreateObject("WScript.Shell")
WshShell.SendKeys "{CAPSLOCK}"
```
#### 自定义通用URL协议实现在浏览器中打开本机任意程序
```
Windows Registry Editor Version 5.00
[HKEY_CLASSES_ROOT\ff]
"URL Protocol"=""
@="Genaral Call"
[HKEY_CLASSES_ROOT\ff\DefaultIcon]
@=""
[HKEY_CLASSES_ROOT\ff\shell]
[HKEY_CLASSES_ROOT\ff\shell\open]
[HKEY_CLASSES_ROOT\ff\shell\open\command]
@="cmd /v:on /k set m=%1 &&call set n=%%m:ff://=%%&call set n=%%n:,= %% &&start !n! &exit"

```
用法
在任意浏览器中输入 ff://参数1[,参数2,参数3]

其中参数1是要打开的本地程序的完整路径；参数2，参数3是要传递给该本地程序的参数，可选。

例如：

在浏览器中输入ff://C:/Windows/System32/notepad.exe 打开记事本程序

在浏览器中输入ff://C:/Windows/System32/notepad.exe,c:/apk_44.conf打本记事本程序，并且会自动在记事本中打开c:/apk_44.conf这个文件

需要特别注意的是：

参数如果是一个路径，那么路径分隔符需要使用/，而不是windows默认的\
参数如果是一个路径，那么路径里不允许出现空格、中文。如果路径里的确存在空格、中文，可以通过快捷方式间接解决。如：想打开C:\Program Files\Microsoft VS Code\Code.exe，我们可以创建一个快捷方式指向它，然后去调用这个快捷方式，ff://c:/Code.exe.lnk
参数如果不是一个路径，但有中文，那么被调用的客户端程序接收到参数将是unicode格式的，需要客户端支持识别
#### Windows UWF（统一写保护）
Windows UWF（统一写保护）组件，功能：
1、当内存盘用，加强性能，提高速度；
2、省SSD写入；
3、当还原卡用，防熊孩子
##### 开启方法
1、程序和功能→启用或关闭Windows功能，勾选"统一写入筛选器"
2、UWF设置
手动启动及设定 (要重新启动系统才生效，关机再开是没用的) :
用管理员模式打开控制台，挑选下列可使用命令行
```
uwfmgr get-config         (检查UWF现时及下次启动后的状态)
uwfmgr filter disable     (要安装程序或者更新的话 ，关闭uwf)
uwfmgr filter enable      （开启uwf）
uwfmgr overlay set-size 3072    (设置使用的内存大小）
uwfmgr volume protect c:        (我选择了保护c盘）
uwfmgr file add-exclusion c:\users    (排除用户文件夹）
uwfmgr servicing Update-Windows (容許系统更新)


如要使用windows内建的防毒 (Windows Defender) 可依以下设定 (不过我没用防毒) :
uwfmgr file add-exclusion C:\Program Files\Windows Defender
uwfmgr file add-exclusion C:\ProgramData\Microsoft\Windows Defender
uwfmgr file add-exclusion C:\Windows\WindowsUpdate.log
uwfmgr file add-exclusion C:\Windows\Temp\MpCmdRun.log
uwfmgr.exe registry add-exclusion HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender
```
最后，如果需要更改系统或安装软件，先关闭UWF后重启。

#### windows获取UUID

```bash
wmic path win32_computersystemproduct get uuid
```
#### windows隐藏服务

```bash
sc create mrxn binPath=C:/Users/mrxn.net/Desktop/mrxn.exe start=auto
#然后以管理员权限打开一个 powershell 窗口，运行安全描述符定义语言(SDDL)命令来隐藏我们创建的 mrxn 服务
& $env:SystemRoot\System32\sc.exe sdset mrxn "D:(D;;DCLCWPDTSD;;;IU)(D;;DCLCWPDTSD;;;SU)(D;;DCLCWPDTSD;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWLOCRRC;;;SU)(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
#取消隐藏
& $env:SystemRoot\System32\sc.exe sdset mrxn "D:(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWLOCRRC;;;SU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
```
原理：https://www.sans.org/blog/red-team-tactics-hiding-windows-services/

找出隐藏的服务
```bash
powershell -c "IEX (New-Object Net.WebClient).DownloadString('https://u.nu/1o-e3')"
#source code
Compare-Object -ReferenceObject `
(Get-Service | Select-Object -ExpandProperty Name | 
% { $_ -replace "_[0-9a-f]{2,8}$" })`
-DifferenceObject (gci -path hklm:\system\currentcontrolset\services | 
% { $_.Name -Replace "HKEY_LOCAL_MACHINE\\","HKLM:\" } | 
? { Get-ItemProperty -Path "$_" -name objectname -erroraction 'ignore' } | 
% { $_.substring(40) }) -PassThru | ?{$_.sideIndicator -eq "=>"}
```