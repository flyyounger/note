ActiveMQ使用java写的，所以天然跨平台，windows，各种类Unix系统都可运行，只需要下载对应的分发包即可。当前AciveMQ的最新版本是5.14.0。我目前在自己机子上安装的版本是5.12.0。下载地址：http://activemq.apache.org/ 安装跳过，windows和linux下需要下载不同的安装包，然后进bin目录执行。

管理界面：localhost:8161/admin/【默认用户名密码是admin/admin，你也可以修改配置，其在ActiveMQ安装目录下的libexec/conf/jetty-real.properties文件中】

ActiveMQ的默认端口是61616

ActiveMQ支持xml文件格式对其进行配置。其实我们运行activemq start时，ActiveMQ就是默认使用了其安装目录下的libexec/conf/activemq.xml文件
*************************************************
发送者代码：
```Java
package com.study.mq;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
/**
 * 消息的生产者（用于向Active MQ 发送消息）
 *
 * @author zhuss
 *
 */
public class ProducSender {
	public static void main(String[] args) throws JMSException {
		/**
		 * ActiveMQConnectionFactory:ActiveMQ的连接的工厂类
		 *
		 * ActiveMQConnection.DEFAULT_USER:ActivieMQ默认的用户名
		 * ActiveMQConnection.DEFAULT_PASSWORD:ActiveMQ默认的密码
		 */
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_USER,
				ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");
		/**
		 *从链接工厂中获得链接（Connection）
		 */
		Connection connection = connectionFactory.createConnection();
		connection.start();

		/**
		 * 从连接中获得Session
		 */
		Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);

		//&nbsp;Destination&nbsp;：消息的目的地;消息发送给谁.&nbsp;&nbsp;
		//&nbsp;获取session注意参数值my-queue是Query的名字
		Destination destination = session.createQueue("zhushunshan");
		/**
		 * 创建消息生产者用于发送消息
		 */
		MessageProducer messageProducer = session.createProducer(destination);
		/**
		 * 设置投递方式
		 * DeliveryMode.NON_PERSISTENT:不持久化
		 *
		 * DeliveryMode.PERSISTENT:持久化
		 */
		messageProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);


		for (int i = 0; i < 100; i++) {
			/**
			 * 创建出一条文本消息
			 */
			TextMessage textMessage = session.createTextMessage("我是消息："+i);
			//发送文本消息
			messageProducer.send(textMessage);
		}
		/**
		 * 消息提交
		 */
		session.commit();
		connection.close();

	}
}
```
接受者代码：

```
package com.study.mq;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
public class ConsumReceiver {

	public static void main(String[] args) throws JMSException {
		/**
		 * ActiveMQConnectionFactory:ActiveMQ的连接的工厂类
		 *
		 * ActiveMQConnection.DEFAULT_USER:ActivieMQ默认的用户名
		 * ActiveMQConnection.DEFAULT_PASSWORD:ActiveMQ默认的密码
		 */
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_USER,
				ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");
		/**
		 *从链接工厂中获得链接（Connection）
		 */
		Connection connection = connectionFactory.createConnection();
		connection.start();

		/**
		 * 从连接中获得Session
		 */
		Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);

		//&nbsp;Destination&nbsp;：消息的目的地;消息发送给谁.&nbsp;&nbsp;
		//&nbsp;获取session注意参数值my-queue是Query的名字
		Destination destination = session.createQueue("zhushunshan");
		/**
		 * 创建消息生产者用于发送消息
		 */
		MessageConsumer messageConsumer = session.createConsumer(destination);


		for (;;) {
			TextMessage textMessage = (TextMessage)messageConsumer.receive(1000);
			if(textMessage != null) System.out.println(textMessage.getText());
			else break;
		}
		/**
		 * 消息提交
		 */
		session.close();
		connection.close();

	}

}
```
*******************************
### 消息主题订阅

服务端：

```
package mq;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import com.sun.xml.internal.ws.wsdl.writer.UsingAddressing;
public class Publisher {

	static int i = 0;
	public static void main(String[] args) throws JMSException, InterruptedException {
		/**
		 * 创建ActiveMQ的连接工厂
		 * ActiveMQConnection.DEFAULT_USER, 默认用户名
		 * ActiveMQConnection.DEFAULT_PASSWORD,  默认密码
		 * "tcp://127.0.0.1:61616" 连接地址
		 */
		ConnectionFactory connectionFactory =
				new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");

		/**
		 * 从连接工厂中获得JMS连接并启动
		 */
		Connection connection = connectionFactory.createConnection();
        connection.start();
        /**
         * 创建一个连接的session
         * 第一个参数设定是否需要事务支持
         */
        final Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        /**
         * 发送消息 而订阅者接收消息必须有个topic，名字也叫msg.714303584
         */
        Destination sendTopic = new ActiveMQTopic("msg.714303584");

        /**
         * 订阅消息 而订阅者发送消息必须有个receive，名字也叫msg.714303584
         */
        Destination sendReceive = new ActiveMQTopic("msg.receive");

        /**
         * 根据主题创建一个发送者
         */
        final MessageProducer  producer = session.createProducer(sendTopic);

        /**
         * 根据主题创建一个接收者
         */
        final MessageConsumer consumer = session.createConsumer(sendReceive);


        Thread sender = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					/**
		        	 * TextMessage: 创建一个字符串消息
		        	 *				使用producer发送消息，session提交消息
		        	 */
					try {
						TextMessage smsg = session.createTextMessage("我是服务端："+i++);
						System.out.println("发送消息"+i);
						producer.send(smsg);
						session.commit();
						Thread.sleep(1000);
					} catch (JMSException | InterruptedException e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}

				}

			}
		});


        Thread receiver = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					/**
		        	 *consumer 接收消息
		        	 *记得要commit提交事务否则会出现重复读取
		        	 */
					try {
						TextMessage gmsg = (TextMessage) consumer.receive();
						System.out.println("我是服务端的接受"+gmsg.getText());
						session.commit();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

			}
		});

        sender.start();

        receiver.start();


	}

}
```
客户端


```
package mq;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
public class Subscriber {

    static int i = 0;

	public static void main(String[] args) throws JMSException, InterruptedException {
		/**
		 * 创建ActiveMQ的连接工厂
		 * ActiveMQConnection.DEFAULT_USER, 默认用户名
		 * ActiveMQConnection.DEFAULT_PASSWORD,  默认密码
		 * "tcp://127.0.0.1:61616" 连接地址
		 */
		ConnectionFactory connectionFactory =
				new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");

		/**
		 * 从连接工厂中获得JMS连接并启动
		 */
		Connection connection = connectionFactory.createConnection();
        connection.start();
        /**
         * 创建一个连接的session
         * 第一个参数设定是否需要事务支持
         */
        final Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        /**
         * 发送消息 而订阅者接收消息必须有个topic，名字也叫msg.714303584
         */
        Destination sendTopic = new ActiveMQTopic("msg.receive");

        /**
         * 订阅消息 而订阅者发送消息必须有个receive，名字也叫msg.714303584
         */
        Destination sendReceive = new ActiveMQTopic("msg.714303584");

        /**
         * 根据主题创建一个发送者
         */
        final MessageProducer  producer = session.createProducer(sendTopic);
        /**
         * 根据主题创建一个接收者
         */
        final MessageConsumer consumer = session.createConsumer(sendReceive);


        Thread sender = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					/**
		        	 * TextMessage: 创建一个字符串消息
		        	 *				使用producer发送消息，session提交消息
		        	 */
					try {
						TextMessage smsg = session.createTextMessage("我是客户端："+i++);
						producer.send(smsg);
						session.commit();
						Thread.sleep(1000);
					} catch (JMSException | InterruptedException e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}

				}

			}
		});


        Thread receiver = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					/**
		        	 *consumer 读取消息
		        	 *注意commit提交
		        	 */
					try {
						TextMessage gmsg = (TextMessage) consumer.receive();
						System.out.println("我是客户端的接受"+gmsg.getText());
						session.commit();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

			}
		});

        sender.start();

        receiver.start();

	}
}
```
### 与Spring结合
首先需要在项目中引入依赖库。

spring-core:用于启动Spring容器，加载bean。

spring-jms:使用Spring JMS提供的API。

activemq-all:使用ActiveMQ提供的API。


```
<dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
      <dependency>
          <groupId>org.apache.activemq</groupId>
          <artifactId>activemq-all</artifactId>
          <version>5.9.0</version>
      </dependency>
      <dependency>
          <groupId>org.springframework</groupId>
          <artifactId>spring-jms</artifactId>
          <version>4.0.2.RELEASE</version>
      </dependency>
      <dependency>
          <groupId>org.springframework</groupId>
          <artifactId>spring-core</artifactId>
          <version>4.0.2.RELEASE</version>
      </dependency>
  </dependencies>
```

接下来配置与ActiveMQ的连接，以及一个自定义的MessageSender

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
 http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location">
            <value>application.properties</value>
        </property>
    </bean>
    <!-- Activemq connection factory -->
    <bean id="amqConnectionFactory" class="org.apache.activemq.ActiveMQConnectionFactory">
        <constructor-arg index="0" value="${jms.broker.url}"/>
    </bean>
    <!-- ConnectionFactory Definition -->
    <bean id="connectionFactory" class="org.springframework.jms.connection.CachingConnectionFactory">
        <constructor-arg ref="amqConnectionFactory"/>
    </bean>
    <!--  Default Destination Queue Definition-->
    <bean id="defaultDestination" class="org.apache.activemq.command.ActiveMQQueue">
        <constructor-arg index="0" value="${jms.queue.name}"/>
    </bean>
    <!-- JmsTemplate Definition -->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="defaultDestination" ref="defaultDestination"/>
    </bean>
    <!-- Message Sender Definition -->
    <bean id="messageSender" class="huangbowen.net.jms.MessageSender">
        <constructor-arg index="0" ref="jmsTemplate"/>
    </bean>
</beans>
```
在此配置文件中，我们配置了一个ActiveMQ的connection factory,使用的是ActiveMQ提供的ActiveMQConnectionFactory类。然后又配置了一个Spring JMS提供的CachingConnectionFactory。我们定义了一个ActiveMQQueue作为消息的接收Queue。并创建了一个JmsTemplate，使用了之前创建的ConnectionFactory和Message Queue作为参数。最后自定义了一个MessageSender，使用该JmsTemplate进行消息发送。

以下MessageSender的实现：

```
package huangbowen.net.jms;
import org.springframework.jms.core.JmsTemplate;
public class MessageSender {
    private final JmsTemplate jmsTemplate;
    public MessageSender(final JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }
    public void send(final String text) {
        jmsTemplate.convertAndSend(text);
    }
}
```
这个MessageSender很简单，就是通过jmsTemplate发送一个字符串信息。

我们还需要配置一个Listener来监听和处理当前的Message Queue。

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
 http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- Message Receiver Definition -->
    <bean id="messageReceiver" class="huangbowen.net.jms.MessageReceiver">
    </bean>
    <bean class="org.springframework.jms.listener.SimpleMessageListenerContainer">
        <property name="connectionFactory" ref="connectionFactory"/>
        <property name="destinationName" value="${jms.queue.name}"/>
        <property name="messageListener" ref="messageReceiver"/>
    </bean>
</beans>
```
在上述xml文件中,我们自定义了一个MessageListener，并且使用Spring提供的SimpleMessageListenerContainer作为Container。

以下是MessageLinser的具体实现：

```
package huangbowen.net.jms;
import javax.jms.*;
public class MessageReceiver implements MessageListener {
    public void onMessage(Message message) {
        if(message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                String text = textMessage.getText();
                System.out.println(String.format("Received: %s",text));
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}
```
这个MessageListener也相当的简单，就是从Queue中读取出消息以后输出到当前控制台中。
另外有关ActiveMQ的url和所使用的Message Queue的配置在application.properties文件中。

```
jms.broker.url=tcp://localhost:61616
jms.queue.name=bar
```
整代码可以参考这里：https://github.com/huangbowen521/SpringJMSSample