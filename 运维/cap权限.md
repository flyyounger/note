### cap权限
通常来说，Linux运行一个程序，是使用当前运行这个程序的用户权限，这当然是合理的。但是有一些程序比较特殊，比如我们常用的ping命令。

ping需要发送ICMP报文，而这个操作需要发送Raw Socket。在Linux 2.2引入CAPABILITIES前，使用Raw Socket是需要root权限的（当然不是说引入CAPABILITIES就不需要权限了，而是可以通过其他方法解决，这个后说），所以你如果在一些老的系统里ls -al $(which ping)，可以发现其权限是-rwsr-xr-x，其中有个s位，这就是suid：
```
root@linux:~# ls -al /bin/ping
-rwsr-xr-x 1 root root 44168 May 7 2014 /bin/ping
```
如果你的输出不是上面这样，不慌，继续往下看。
suid全称是Set owner User ID up on execution。这是Linux给可执行文件的一个属性，上述情况下，普通用户之所以也可以使用ping命令，原因就在我们给ping这个可执行文件设置了suid权限

设置了s位的程序在运行时，其Effective UID将会设置为这个程序的所有者。比如，/bin/ping这个程序的所有者是0（root），它设置了s位，那么普通用户在运行ping时其Effective UID就是0，等同于拥有了root权限。

这里引入了一个新的概念Effective UID。Linux进程在运行时有三个UID：

Real UID 执行该进程的用户实际的UID
Effective UID 程序实际操作时生效的UID（比如写入文件时，系统会检查这个UID是否有权限）
Saved UID 在高权限用户降权后，保留的其原本UID（本文中不对这个UID进行深入探讨）
通常情况下Effective UID和Real UID相等，所以普通用户不能写入只有UID=0号才可写的/etc/passwd；有suid的程序启动时，Effective UID就等于二进制文件的所有者，此时Real UID就可能和Effective UID不相等了。

有的同学说某某程序只要有suid权限，就可以提权，这个说法其实是不准确的。只有这个程序的所有者是0号或其他super user，同时拥有suid权限，才可以提权。

如果你查看ping看到的输出是这样,那就是今天要讲的CAP了
```
[root@k8s ~]# ls -al /bin/ping
.rwxr-xr-x root root 64.6 KB Fri Aug  4 04:01:04 2017  ping
```
Linux 2.2以后增加了capabilities的概念，可以理解为水平权限的分离。以往如果需要某个程序的某个功能需要特权，我们就只能使用root来执行或者给其增加SUID权限，一旦这样，我们等于赋予了这个程序所有的特权，这是不满足权限最小化的要求的；在引入capabilities后，root的权限被分隔成很多子权限，这就避免了滥用特权的问题，我们可以在capabilities(7) - Linux manual page中看到这些特权的说明。

类似于ping和nmap这样的程序，他们其实只需要网络相关的特权即可。所以，如果你在比较新的Linux下查看ping命令的capabilities，你会看到一个cap_net_raw：
```
[root@k8s ~]# ls -al /bin/ping
.rwxr-xr-x root root 64.6 KB Fri Aug  4 04:01:04 2017  ping
[root@k8s ~]# getcap /bin/ping
/bin/ping = cap_net_admin,cap_net_raw+p
```



```
[root@hadoop html]# chmod +s 64bit-checker.exe
[root@hadoop html]# setcap cap_net_raw,cap_net_admin,cap_net_bind_service+eip ./64bit-checker.exe 
[root@hadoop html]# getcap ./64bit-checker.exe
[root@hadoop html]# setcap cap_net_raw,cap_net_admin,cap_net_bind_service-eip ./64bit-checker.exe

```
程序文件的 capabilities
在可执行文件的属性中有三个集合来保存三类 capabilities，它们分别是：

Permitted
Inheritable
Effective
在进程执行时，Permitted 集合中的 capabilites 自动被加入到进程的 Permitted 集合中。
Inheritable 集合中的 capabilites 会与进程的 Inheritable 集合执行与操作，以确定进程在执行 execve 函数后哪些 capabilites 被继承。
Effective 只是一个 bit。如果设置为开启，那么在执行 execve 函数后，Permitted 集合中新增的 capabilities 会自动出现在进程的 Effective 集合中。

进程的 capabilities
进程中有五种 capabilities 集合类型，分别是：

Permitted
Inheritable
Effective
Bounding
Ambient
相比文件的 capabilites，进程的 capabilities 多了两个集合，分别是 Bounding 和 Ambient。
/proc/[pid]/status 文件中包含了进程的五个 capabilities 集合的信息，我们可以通过下面的命名查看当前进程的 capabilities 信息：
```
[root@hadoop html]# cat /proc/$$/status | grep 'Cap'
CapInh:	0000000000000000
CapPrm:	0000001fffffffff
CapEff:	0000001fffffffff
CapBnd:	0000001fffffffff
CapAmb:	0000000000000000
```
但是这中方式获得的信息无法阅读，我们需要使用 capsh 命令把它们转义为可读的格式：
```
[root@hadoop html]# capsh --decode=0000003fffffffff
0x0000003fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36,37
```

下面是从 [capabilities man page](http://man7.org/linux/man-pages/man7/capabilities.7.html) 中摘取的 capabilites 列表：

| capability 名称      | 描述                                                         |
| -------------------- | ------------------------------------------------------------ |
| CAP_AUDIT_CONTROL    | 启用和禁用内核审计；改变审计过滤规则；检索审计状态和过滤规则 |
| CAP_AUDIT_READ       | 允许通过 multicast netlink 套接字读取审计日志                |
| CAP_AUDIT_WRITE      | 将记录写入内核审计日志                                       |
| CAP_BLOCK_SUSPEND    | 使用可以阻止系统挂起的特性                                   |
| CAP_CHOWN            | 修改文件所有者的权限                                         |
| CAP_DAC_OVERRIDE     | 忽略文件的 DAC 访问限制                                      |
| CAP_DAC_READ_SEARCH  | 忽略文件读及目录搜索的 DAC 访问限制                          |
| CAP_FOWNER           | 忽略文件属主 ID 必须和进程用户 ID 相匹配的限制               |
| CAP_FSETID           | 允许设置文件的 setuid 位                                     |
| CAP_IPC_LOCK         | 允许锁定共享内存片段                                         |
| CAP_IPC_OWNER        | 忽略 IPC 所有权检查                                          |
| CAP_KILL             | 允许对不属于自己的进程发送信号                               |
| CAP_LEASE            | 允许修改文件锁的 FL_LEASE 标志                               |
| CAP_LINUX_IMMUTABLE  | 允许修改文件的 IMMUTABLE 和 APPEND 属性标志                  |
| CAP_MAC_ADMIN        | 允许 MAC 配置或状态更改                                      |
| CAP_MAC_OVERRIDE     | 覆盖 MAC(Mandatory Access Control)                           |
| CAP_MKNOD            | 允许使用 mknod() 系统调用                                    |
| CAP_NET_ADMIN        | 允许执行网络管理任务                                         |
| CAP_NET_BIND_SERVICE | 允许绑定到小于 1024 的端口                                   |
| CAP_NET_BROADCAST    | 允许网络广播和多播访问                                       |
| CAP_NET_RAW          | 允许使用原始套接字                                           |
| CAP_SETGID           | 允许改变进程的 GID                                           |
| CAP_SETFCAP          | 允许为文件设置任意的 capabilities                            |
| CAP_SETPCAP          | 参考 [capabilities man page](http://man7.org/linux/man-pages/man7/capabilities.7.html) |
| CAP_SETUID           | 允许改变进程的 UID                                           |
| CAP_SYS_ADMIN        | 允许执行系统管理任务，如加载或卸载文件系统、设置磁盘配额等   |
| CAP_SYS_BOOT         | 允许重新启动系统                                             |
| CAP_SYS_CHROOT       | 允许使用 chroot() 系统调用                                   |
| CAP_SYS_MODULE       | 允许插入和删除内核模块                                       |
| CAP_SYS_NICE         | 允许提升优先级及设置其他进程的优先级                         |
| CAP_SYS_PACCT        | 允许执行进程的 BSD 式审计                                    |
| CAP_SYS_PTRACE       | 允许跟踪任何进程                                             |
| CAP_SYS_RAWIO        | 允许直接访问 /devport、/dev/mem、/dev/kmem 及原始块设备      |
| CAP_SYS_RESOURCE     | 忽略资源限制                                                 |
| CAP_SYS_TIME         | 允许改变系统时钟                                             |
| CAP_SYS_TTY_CONFIG   | 允许配置 TTY 设备                                            |
| CAP_SYSLOG           | 允许使用 syslog() 系统调用                                   |
| CAP_WAKE_ALARM       | 允许触发一些能唤醒系统的东西(比如 CLOCK_BOOTTIME_ALARM 计时器) |

参考：
https://www.leavesongs.com/PENETRATION/linux-suid-privilege-escalation.html
https://www.cnblogs.com/sparkdev/p/11417781.html
