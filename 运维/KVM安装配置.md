# KVM安装配置
KVM(Kernel-based Virtual Machine, 即内核级虚拟机) 是一个开源的系统虚拟化模块。

QEMU是个独立的虚拟化解决方案，从这个角度它并不依赖KVM。而KVM是另一套虚拟化解决方案，不过因为这个方案实际上只实现了内核中对处理器（Intel VT）, AMD SVM)虚拟化特性的支持，换言之，它缺乏设备虚拟化以及相应的用户空间管理虚拟机的工具，所以它借用了QEMU的代码并加以精简，连同KVM一起构成了另一个独立的虚拟化解决方案：KVM + QEMU。

要使用起来，需要硬件支持，并且需要加载相应的模块。按以下的步骤去检测安装即可。
```
# 检测宿主机cpu是否支持虚拟化，如果flags里有vmx 或者svm就说明支持VT
$ grep -E "(vmx|svm)" --color=always /proc/cpuinfo

# 检查内核的KVM和VirtIO模块是否可用
$ zgrep KVM /proc/config.gz
$ zgrep VIRTIO /proc/config.gz 

# 查看内核模块是否装载
$ lsmod | grep kvm 
$ lsmod | grep virtio

# 手动加载内核模块
$ sudo modprobe virtio

# 新建一个文件
sudo nano /etc/modules-load.d/virtio-net.conf
# Load virtio-net.ko at boot
virtio-net      #网络设备
virtio-blk      #块设备
virtio-scsi     #控制器设备
virtio-serial       #串行设备
# 当前用户加入组kvm
$ sudo usermod -a -G kvm lixiang

# 安装qemu以及图形化客户端
$ sudo pacman -S qemu
$ sudo pacman -S libvirt virt-manager

# 要连接网络，还要安装包
$ sudo pacman -S ebtables dnsmasq bridge-utils openbsd-netcat

# 设置授权
$ sudo vim /etc/polkit-1/rules.d/50-libvirt.rules
/* Allow users in kvm group to manage the libvirt daemon without authentication */
polkit.addRule(function(action, subject) {
    if (action.id == "org.libvirt.unix.manage" &&
        subject.isInGroup("kvm")) {
            return polkit.Result.YES;
    }
});

# 启动服务
$ sudo systemctl enable libvirtd
$ sudo systemctl start libvirtd
```

使用举例：
```
# mkdir -p /home/josepy/Libvirt/images
# qemu-img create -f qcow2 /home/josepy/Libvirt/images/centos-server.qcow2 10G
# virt-install 
--name CentOS7 
--ram 1024 
--disk path=/home/josepy/Libvirt/images/centos-server.qcow2 
--vcpus 1 
--os-type linux 
--os-variant rhel7 
--graphics none 
--console pty,target_type=serial 
--location /home/josepy/Kvm images/CentOS-7-x86_64-Minimal-1503-01.iso 
--extra-args 'console=ttyS0,115200n8 serial'
#带图形界面
virt-install \
--name=centos7_test \  #虚拟机的名字
--ram=1024 \  #虚拟机的内存，以M为单位。需要注意在KVM的新版本中--ram被--memory所取代
--vcpus=2 \  #CPU核心数
--check-cpu \  #检查虚拟机CPU数量是否超过物理机
--arch=x86_64  \  #CPU架构
--os-type=linux \  #系统类型
--virt-type=kvm \  #使用的hypervisor，如kvm、qemu、xen等
--cdrom=/usr/local/src/CentOS-7-x86_64-Minimal-1708.iso \  #指定镜像路径
--disk=/kvm/centos7.qcow2 \  #虚拟机硬盘路径
--network bridge=br0 \   #指定虚拟机网络
--graphics vnc,listen=0.0.0.0 \  #指定图形显示相关配置，默认是vnc
--noautoconsole \  #禁止自动连接虚拟机控制台
--cpu=host-passthrough  #CPU配置模式，这里使用的是将物理机的CPU暴露给虚拟机的方式
```
其他操作
```
#添加虚拟网卡
virsh attach-interface liwei --type bridge --source br0  
#桥接
#!/bin/bash
brctl addbr br0  #增加一块网卡
brctl addif br0 eth0  #让eth0网卡成为br0网卡的一员
ip addr del dev eth0 192.168.1.100/24  #给br0配置IP信息，这里的信息就是之前eth0网卡的信息
route add default gw 192.168.1.1
```
参考：http://www.linuxe.cn/post-413.html
https://www.sysgeek.cn/install-configure-kvm-ubuntu-18-04/
