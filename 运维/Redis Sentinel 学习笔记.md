# Redis Sentinel 学习笔记
### 概述

Redis Sentinel 是用来实现 Redis 高可用的一套解决方案。Redis Sentinel 由两个部分组成：由一个或者多个 Sentinel 实例组成 Sentinel 系统；由一个主 Redis 服务器（master redis）和多个从 Redis 服务器（slave redis）组成主从备份的 Redis 系统。Sentinel 系统本身是一个分布式的系统，它的作用是监视 redis 服务器，在 master redis 下线时，自动将某个 slave redis 上升为新的主服务器。Redis 系统由 master redis 处理客户端的命令请求，slave redis 作为主服务器的备份而存在。Redis Sentinel 的系统架构如图 1 所示。
![image](http://note.youdao.com/yws/public/resource/bed9f003fb227100e713e71f3d60e6d7/xmlnote/WEBRESOURCE7241fb3e5543198a156e52fd3d10fa85/847)
图1：Redis Sentinel 架构图

假设某个时刻，原来的 master redis 进入下线状态，Sentinel 系统就会监控到这个 master redis 已下线，接下来，Sentinel 系统就会进行故障转移操作：

1. 首先，Sentinel 系统会挑选出原来的其中一个 slave redis，让这个 slave redis 上升为 master redis。
2. 之后，Sentinel 系统会向其他所有的 slave redis 发送新的复制命令，让它们成为新 master redis 的 slave redis。
3. 另外，Sentinel 系统还会继续监控已下线的 redis，当它重新上线时，会将设置为新 master redis 的 slave redis。

需要指出的是，Redis Sentinel 只是一种主从备份的高可用方案，它的目的是支持 Redis 提供高可用的服务，但 Redis Sentinel 系统本身并没有对 Redis 的承载能力进行扩容。理论上说，如果 slave redis 并没有对外提供读写服务，Redis Sentinel 的负载能力与单点 Redis 的负载能力是一样的。有些同学使用了 Redis Sentinel 方案后，可以有效降低 Redis 单机版的负载问题，这种想法是不现实的。这是因为在 Redis Sentinel 系统中，还是只有一个 redis 服务器在提供服务，其负载能力还是受单个 redis 服务器的限制。

如果需要对 redis的承载能力进行扩容，可以使用其他的分布式 redis 解决方案，例如 Codis，或者 Twemproxy。

### 实验

为了进一步了解 Redis Sentinel 系统如何实现系统高可用的原理，我们进行以下实验。

### 实验环境

为了操作和描述的简单起见，我们搭建的 Redis Sentinel 系统，只包含一个 master redis 和一个 slave redis，以及两个 sentinel 实例。生产环境下，建议 slave redis 至少部署两个，sentinel 实例则至少三个。
服务器部署环境如图 2 所示。![image](http://note.youdao.com/yws/public/resource/bed9f003fb227100e713e71f3d60e6d7/xmlnote/WEBRESOURCE5e18226710284283f438f0b59b714fde/848)
图2：Redis Sentinel 部署环境

操作系统：CentOS 6.6

Redis 版本：release 3.2.8

### 实验过程

#### 1. 搭建 Redis 系统
在 redis 官网（https://redis.io/download）下载 redis后对其解压，然后修改 redis 目录下的配置文件。Master redis 使用的配置 redis.conf 如下（限于篇幅只列出关键的几个配置，其他配置使用默认值即可）：

```
# master redis 绑定的IP地址
bind 192.168.174.137
# 监听的端口，使用默认的 2679
port 6379
# 以后台进程运行
daemonize yes
# 日志输出位置，便查看实验结果
logfile "/home/lihao/redis-stable/logs/redis.log"
```
Slave redis 使用的配置 redis.conf 如下：

```
# slave redis 绑定的IP地址
bind 192.168.174.143
# 监听的端口，使用默认的 2679
port 6379
# 以后台进程运行
daemonize yes
# 日志输出位置，便查看实验结果
logfile "/home/lihao/redis-stable/logs/redis.log"
# 作为 master redis 的 slave redis
slaveof 192.168.174.137 6379
```
修改好配置后，先后启动 master redis 和 slave redis：
启动命令：

```
$ ./src/redis-server redis.conf
```
这样， 主从备份的 Redis 系统就搭建好了。

#### 2. 搭建 Sentinel 系统

第一个 sentinel 实例的配置 sentinel.conf 配置如下：

```
# 绑定的IP地址
bind 192.168.174.137
# 监听的端口
port 26379
# 监控的 master redis 信息，其中的数字 2 表示判断
# master redis 客观下线所需要的 sentinel 实例数量
sentinel monitor mymaster 192.168.174.137 6379 2
# 判断 master redis 主观下线的时长
sentinel down-after-milliseconds mymaster 30000
```
第二个 sentinel 实例的配置如下：

```
#绑定的IP地下
bind 192.168.174.143
#监听的端口
port 26379
#监控的 master redis 信息，其中的数字 2 表示判断
#master redis 客观下线所需要的 sentinel 实例数量
sentinel monitor mymaster 192.168.174.137 6379 2
#判断 master redis 主观下线的时长
sentinel down-after-milliseconds mymaster 30000
#master有密码则需要告诉哨兵
sentinel auth-pass mymaster 123456
```
然后分别在两台机器上执行以下命令启动 sentinel 实例。
```
./src/redis-sentinel sentinel.conf
```
这样，Sentinel 系统也搭建完成了。

#### 3. 测试 Redis 系统主从切换

在机器 192.168.174.137 上执行以下命令，获取其主从角色信息：

```
$ ./src/redis-cli -h 192.168.174.137 info Replication
```
得到以下输出信息，表明 192.168.174.137 上的 redis 是 master 角色。

```
# Replication
role:master
connected_slaves:1
slave0:ip=192.168.174.143,port=6379,state=online,offset=15,lag=1

```
机器 192.168.174.143 上的 redis 则是 slave 角色：

```
# Replication
role:slave
master_host:192.168.174.137
master_port:6379
```
对 192.168.174.137 master redis 执行下线命令操作：

```
192.168.174.137:6379> shutdown
```
等待一会儿，可以看到 192.168.174.143 变成了 master 的角色。

```
# Replication
role:master
connected_slaves:0
```
192.168.174.137 的 redis 重新上线后，可以看到其已变成新的 master redis 的 slave redis。

```
# Replication
role:slave
master_host:192.168.174.143
master_port:6379
```
### 实验小结

从实验过程可以看到，Sentinel 系统会对已下线的 master redis 执行故障转移操作。这个故障操作实际包含三个步骤：

- （1）在已下线 master redis 属下的所有 slave redis 里面，挑选出一个 slave redis ，并将其转换为 master redis 角色。例如，上述实验中，将 192.168.174.143 的 redis 转换为 master 的角色。
- （2）让其他的所有的 slave redis 改为复制新的 master redis。
- （3）将已下线的 master redis 设置为新 master redis 的 slave redis。在上述实验中，192.168.174.137 上下线的 redis 在重新上线后，变成了 192.168.174.143 上 redis 的 slave redis。

### 总结

Redis Sentinel 系统内部通过执行故障转移操作，保证 Redis 系统在 master redis 下线后，仍然可以继续提供对外服务，从而达到系统高可用的目的。 

参考资料

https://redis.io/topics/sentinel
http://www.cnblogs.com/LiZhiW/p/4851631.html
https://segmentfault.com/a/1190000002680804
http://blog.csdn.net/moyu_2012/article/details/47856949
http://www.oschina.net/p/codis/
https://github.com/twitter/twemproxy/
