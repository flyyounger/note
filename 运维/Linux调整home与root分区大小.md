```bash
#先查看分区列表
df -hT
#卸载 /home 分区,不要在/home目录下操作
tar cvf /tmp/home.tar /home #先备份，为空则不需要
#如果中途重启，需要修改/etc/fstab，注释分区
umount /home
#删除/home所在的lv
lvremove /dev/mapper/openeuler-home
#创建50G大小的/home的lv
lvcreate -L 50G -n /dev/mapper/openeuler-home
#创建文件系统快速格式化，必须带如下参数，否则巨慢无比
mkfs.ext4 -E lazy_itable_ini=1 -E nodiscard  /dev/mapper/openeuler-home
#挂载
mount /dev/mapper/openeuler-home
df -h
#恢复home文件
tar xvf /tmp/home.tar -C /
#增加root分区大小
lvextend -L +350G /dev/mapper/openeuler-root
df -h
#扩展/root文件系统
resize2fs /dev/mapper/openeuler-root
df -h
```

注意，如果是XFS系统，最后一步扩展的命令是

```bash
xfs_growfs /dev/mapper/cl-root
```

ext2/ext3/ext4文件系统的调整命令是resize2fs（增大和减小都支持）

xfs文件系统只支持增大分区空间的情况，不支持减小的情况（切记！！！！！）。

减小分区命令

```bash
# lvreduce -L -40G /dev/mapper/centos-home
```



直接减小/home分区，增加/分区，可行否？测试未成功（可能命令敲错了），还需要继续测试。