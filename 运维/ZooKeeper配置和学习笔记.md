### ZooKeeper介绍
Zookeeper 分布式服务框架是 Apache Hadoop 的一个子项目，它主要是用来解决分布式应用中经常遇到的一些数据管理问题，如：统一命名服务、状态同步服务、集群管理、分布式应用配置项的管理等。本文将从使用者角度详细介绍 Zookeeper 的安装和配置文件中各个配置项的意义，以及分析 Zookeeper 的典型的应用场景（配置文件的管理、集群管理、同步锁、Leader 选举、队列管理等）。

ZooKeeper有三种工作模式：
- Standalone：单点模式，有单点故障问题。
- 伪分布式：在一台机器同时运行多个ZooKeeper实例，仍然有单点故障问题，当然，其中配置的端口号要错开的，适合实验环境模拟集群使用。
- 完全分布式：在多台机器上部署ZooKeeper集群，适合线上环境使用。

三种端口号:
- 端口X：客户端连接ZooKeeper集群使用的监听端口号
- 端口Y：leader和follower之间数据同步使用的端口号
- 端口Z：leader选举专用的端口号

单点分析：

在每个ZooKeeper节点当中，ZooKeeper维护了一个类似linux的树状文件系统结构，可以把一些配置信息，数据等存放到ZooKeeper当中，也可以把ZooKeeper当中的一个目录节点当做一个锁的互斥文件来实现并发安全控制，你看到这就先把ZooKeeper理解为一个在操作系统上运行的一个虚拟文件系统，只不过他不是像HDFS那样真的用来存放文件的，他是利用文件系统的节点作为底层实现来提供分布式环境很常用的功能，这在后面的实战使用会具体讲解.

集群分析:

ZooKeeper中的每个节点都是上面的单点分析那样工作的，但是集群多节点之间到底如何进行协商和通信呢？

- 首先，类似mysql读写分离那样，ZooKeeper的每个节点都存放相同的数据，因此访问ZooKeeper的时候会被分流道各个节点实现高并发，多节点也顺便实现了高可用。
- ZooKeeper的节点之间也有主次关系，集群启动完成之后，ZooKeeper会运行选举程序（端口Z）从集群中选择一个leader节点，而其他的节点就是follower节点，对于ZooKeeper的写操作，会被转发到leader节点，而follower节点和leader节点的数据同步（端口Y）也在后台自动实现，读操作则每个节点都能提供，负载均衡~
- 容灾机制：follower节点挂了比较不要紧，有leader协调，整个集群还不至于发生致命影响。但是leader节点要是挂了，就群龙无首了，但是其实这个状态和ZooKeeper集群启动前期还没确定leader节点的时候是一样的状态，下面讲解这个状态如何进行leader的 选举。
由于每个节点配置文件中都维护了整个ZooKeeper集群的所有节点列表，因此在没确定leader节点或者leader节点挂掉的时候，每个节点向leader的通信必然是失败的，follower节点就是这么发现leader节点挂了的，这个时候他就能启动选举程序进行leader竞选，和其他的所有follower节点进行通信，根据某种算法方案确定leader的节点之后，被选中的节点就启动leader的程序，化身leader重新领导整个集群。

客户端访问高可用：

- 读操作：每个节点都能响应
- 写操作：不管向哪个节点请求都会转发到leader节点执行，再把数据同步到各个follower节点。
- 访问follower节点宕机：客户端会保存一个地址列表，会自动使用另一个地址进行访问，实在不行还有leader节点分配地址再次访问呢，而且一旦客户端和服务器的连接被断开，客户端就会自动去连接另一个节点的地址进行请求。
- 访问leader节点宕机：也是使用这个地址列表，如果是读操作，则leader选举出来之前都能访问，但是如果是写操作，就要等leader选举完成之后才能进行操作。
我之前有疑问的地方就是以为客户端只维护一个节点的地址，这样的话导致了这个节点宕机之后客户端就和ZooKeeper集群断开了且无法重新连接了，但是后来知道了客户端是保存了节点地址列表，那所有问题就很好理解啦。

### ZooKeeper环境部署和和简单测试

安装步骤总结：

1. 创建缓存目录；
2. 配置zookeeper配置文件zoo.cfg，设置缓存目录、监听客户端端口号、server列表配置等
3. bin目录下的zkServer.sh启动即可，还能查看集群中的leader、follower之间的关系

单机模式：

- 创建缓存目录;
- 解压ZooKeeper安装包，在conf目录下的zoo_sample.cfg文件复制一个副本zoo.cfg，在里面进行ZooKeeper整体配置：
```
tickTime=2000
dataDir=/opt/zookeeper1
clientPort=2181
```
tickTime：这个时间是作为 Zookeeper 服务器之间或客户端与服务器之间维持心跳的时间间隔，也就是每个 tickTime 时间就会发送一个心跳。

dataDir：顾名思义就是 Zookeeper 保存数据的目录，默认情况下，Zookeeper 将写数据的日志文件也保存在这个目录里。

clientPort：这个端口就是客户端连接 Zookeeper 服务器的端口，Zookeeper 会监听这个端口，接受客户端的访问请求。

集群模式：

- 创建缓存目录;
- 解压ZooKeeper安装包，在conf目录下的zoo_sample.cfg文件复制一个副本zoo.cfg，在里面进行ZooKeeper整体配置：

```
tickTime=2000
dataDir=/opt/zookeeper1
clientPort=2181
initLimit=5
syncLimit=2
server.1=192.168.211.1:2888:3888
server.2=192.168.211.2:2888:3888
```
上面前三个的配置和单机模式一样，不多赘述了。
initLimit：这个配置项是用来配置 Zookeeper 接受客户端（这里所说的客户端不是用户连接 Zookeeper 服务器的客户端，而是 Zookeeper 服务器集群中连接到 Leader 的 Follower 服务器）初始化连接时最长能忍受多少个心跳时间间隔数。当已经超过 10 个心跳的时间（也就是 tickTime）长度后 Zookeeper 服务器还没有收到客户端的返回信息，那么表明这个客户端连接失败。总的时间长度就是 52000=10 秒

syncLimit：这个配置项标识 Leader 与 Follower 之间发送消息，请求和应答时间长度，最长不能超过多少个 tickTime 的时间长度，总的时间长度就是 22000=4 秒
server.A=B：C：D：其中 A 是一个数字，表示这个是第几号服务器；B 是这个服务器的 ip 地址；C 表示的是这个服务器与集群中的 Leader 服务器交换信息的端口(上面的端口Y)；D 表示的是万一集群中的 Leader 服务器挂了，需要一个端口来重新进行选举，选出一个新的 Leader，而这个端口就是用来执行选举时服务器相互通信的端口(上面的端口Z)。如果是伪集群的配置方式，由于 B 都是一样，所以不同的 Zookeeper 实例通信端口号不能一样，所以要给它们分配不同的端口号。

在上面配置的dataDir目录下创建myid文件，填写这个节点上的id号，就是server.A=B：C：D配置的A那个号码

```
[root@VM_68_145_centos zookeeper]# cat /opt/zookeeper1/myid
1
[root@VM_68_145_centos zookeeper]# cat /opt/zookeeper2/myid
2
[root@VM_68_145_centos zookeeper]# cat /opt/zookeeper3/myid
3
[root@VM_68_145_centos zookeeper]# cat /opt/zookeeper4/myid
4
```
将配置好的整个安装包复制到每个节点机器上，scp命令复制过去，我这里是采用伪分布式，直接使用cp命令即可。

到每个节点上zookeeper/bin目录下的zkServer.sh执行即可启动

简单说明这样的配置ZooKeeper集群是怎么工作的:

- 首先，每个节点上zoo.cfg配置文件中都有整个集群的列表，所以每个节点之间的通信都是可行的。
- 然后是dataDir目录下的myid标记了这个机器上的这个节点是zoo.cfg上的集群列表的哪个记录，从这个就能知道当前的这个节点所处的位置，也能知道当前机器的节点是不是leader，以便于执行leader该执行的程序。
- 关于配置的三个端口号：
1. 端口X：监听客户端连接的，没什么可说的
2. 端口Y：follower和leader进行数据同步通信用的，这个是长连接随时同步数据，健康情况下正常运行，leader宕机就无法正常执行，此时触发选举程序选择新的leader。
3. 端口Z：选举时各个follower节点之间两两可以相互通信的，以便于成功选择出leader。

- 踩坑记录：我刚开始以为端口X和端口Y是同一个，导致了我就设置了同一个端口号，所以显然是无法启动成功的，后来知道这两个端口号完成的是不同的功能，所以改正完成就能启动成功了。

##### 命令行方式操纵ZooKeeper：

```
# 进入本地的ZooKeeper：
/usr/java/zookeeper/zookeeper1/bin/zkCli.sh
# 进入远程的ZooKeeper：
/usr/java/zookeeper/zookeeper1/bin/zkCli.sh -server {ip}:{port}
```
这样就进入了ZooKeeper的命令行客户端，就能访问指定的ZooKeeper集群中的数据，支持以下的操作（在命令行下输入错误命令就能看到提示了。。。）：

```
stat path [watch]
set path data [version]
ls path [watch]
delquota [-n|-b] path
ls2 path [watch]
setAcl path acl
setquota -n|-b val path
history
redo cmdno
printwatches on|off
delete path [version]
sync path
listquota path
rmr path
get path [watch]
create [-s] [-e] path data acl
addauth scheme auth
quit
getAcl path
close
connect host:port
```


#### 操作示例

```
# 创建节点/testInput，节点中的数值为inputData
create /testInput "inputData"
# 查询刚才插入的节点
get /testInput
# 运行结果
inputData
cZxid = 0x300000006
ctime = Sun Dec 18 21:47:21 CST 2016
mZxid = 0x300000006
mtime = Sun Dec 18 21:47:21 CST 2016
pZxid = 0x300000006
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 9
numChildren = 0
```


#### Java客户端操纵ZooKeeper


```
public class ZooKeeperClient {
//同步互斥变量，用来阻塞等待ZooKeeper连接完成之后再进行ZooKeeper的操作命令
private static CountDownLatch connectedSemaphore = new CountDownLatch( 1 );
public static void main(String[] args) throws Exception {
   // 创建一个与服务器的连接
   ZooKeeper zk = new ZooKeeper("119.29.153.56:2181",3000, new Watcher() {//这个是服务器连接完成回调的监听器
       // 监控所有被触发的事件
       public void process(WatchedEvent event) {
           System.out.println("已经触发了" + event.getType() + "事件！");
           if ( Event.KeeperState.SyncConnected == event.getState() ) {
               //连接完成的同步事件，互斥变量取消，下面的阻塞停止，程序继续执行
               connectedSemaphore.countDown();
           }
       }
   });
   //如果和ZooKeeper服务器的TCP连接还没完全建立，就阻塞等待
   connectedSemaphore.await();
   // 创建一个目录节点
   zk.create("/testRootPath", "testRootData".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,
           CreateMode.PERSISTENT);
   // 创建一个子目录节点
   zk.create("/testRootPath/testChildPathOne", "testChildDataOne".getBytes(),
           ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
   System.out.println(new String(zk.getData("/testRootPath",false,null)));
   // 取出子目录节点列表
   System.out.println(zk.getChildren("/testRootPath",true));
   // 修改子目录节点数据
   zk.setData("/testRootPath/testChildPathOne","modifyChildDataOne".getBytes(),-1);
   System.out.println("目录节点状态：["+zk.exists("/testRootPath",true)+"]");
   // 创建另外一个子目录节点
   zk.create("/testRootPath/testChildPathTwo", "testChildDataTwo".getBytes(),
           ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
   System.out.println(new String(zk.getData("/testRootPath/testChildPathTwo",true,null)));
   // 删除子目录节点
   zk.delete("/testRootPath/testChildPathTwo",-1);
   zk.delete("/testRootPath/testChildPathOne",-1);
   // 删除父目录节点
   zk.delete("/testRootPath",-1);
   // 关闭连接
   zk.close();
}
}
```
输出结果：


```
已经触发了None事件！
testRootData
[testChildPathOne]
目录节点状态：[17179869200,17179869200,1482114759242,1482114759242,0,1,0,0,12,1,17179869201
]
已经触发了NodeChildrenChanged事件！
testChildDataTwo
已经触发了NodeDeleted事件！
已经触发了NodeDeleted事件！
```
解释：

第一个添加节点/testRootPath，触发None事件，之后再获取这个节点数据、其子节点列表、节点状态

接下来创建第二个子节点/testRootPath/testChildPathTwo，触发NodeChildrenChanged事件

然后删除两个子节点，触发两次NodeDeleted事件

#### 常用接口介绍：
几个可能的疑问：
- ACL：这个是ZooKeeper本身提供的简单的权限控制模型，有一些简单的权限控制策略，可以稍做了解。
- OP：面向对象的思想嘛，一个ZooKeeper的操作命令也抽象为一个操作对象，不过它只是个抽象类，具体的实现有Delete、Check等子类才是实际的具体操作对象。
- CreateMode：创建节点的模式枚举，四个成员分别是：PERSISTENT（持久化）、PERSISTENT_SEQUENTIAL（持久化并序号自增）、EPHEMERAL（临时，当前session有效）、EPHEMERAL_SEQUENTIAL（临时，当前session有效，序号自增）
- ZooDefs.Ids：提供了上面ACL的常用的权限策略常量列表。
- Event.KeeperState：ZooKeeper的事件类型状态常量列表。
- 有了上面的就基本对ZooKeeper的API有了初步的认识，剩下的问题看官方文档就很好理解了：http://people.apache.org/~larsgeorge/zookeeper-1215258/build/docs/dev-api/

特别注意：ZooKeeper是支持Watcher监听的，你可以用它监听某个节点的值是否存在、是否改变、是否被删除等之类的动作，当他触发了相关的动作就会进行回调的，有点是异步编程，也是特别棒的东西。

### 实战使用
1. 统一命名服务（Name Service）：在ZooKeeper的树形结构下你可以创建一个统一的不重复的命名，比如create创建一个节点即可，再创建一个相同名称的节点是不允许的。
2. 配置管理（Configuration Management）：意思就是分布式应用的配置可以交给ZooKeeper来管理，不然一旦修改配置，就得每台机器上的配置都做相应的修改，如果交给ZooKeeper管理的话，只需要修改ZooKeeper上的节点值即可。
3. 集群管理（Group Membership）：
- Zookeeper 不仅能够帮你维护当前的集群中机器的服务状态，而且能够帮你选出一个“总管”，让这个总管来管理集群，这就是 Zookeeper 的另一个功能 Leader Election。
- 它们的实现方式都是在 Zookeeper上创建一个 EPHEMERAL 类型的目录节点，然后每个 Server 在它们创建目录节点的父目录节点上调用 getChildren(String path, boolean watch) 方法并设置 watch 为 true，由于是 EPHEMERAL 目录节点，当创建它的 Server 死去，这个目录节点也随之被删除，所以 Children 将会变化，这时 getChildren上的 Watch 将会被调用，所以其它 Server 就知道已经有某台 Server 死去了。新增 Server 也是同样的原理。
- Zookeeper 如何实现 Leader Election，也就是选出一个 Master Server。和前面的一样每台 Server 创建一个 EPHEMERAL 目录节点，不同的是它还是一个 SEQUENTIAL 目录节点，所以它是个 EPHEMERAL_SEQUENTIAL 目录节点。之所以它是 EPHEMERAL_SEQUENTIAL 目录节点，是因为我们可以给每台 Server 编号，我们可以选择当前是最小编号的 Server 为 Master，假如这个最小编号的 Server 死去，由于是 EPHEMERAL 节点，死去的 Server 对应的节点也被删除，所以当前的节点列表中又出现一个最小编号的节点，我们就选择这个节点为当前 Master。这样就实现了动态选择 Master，避免了传统意义上单 Master 容易出现单点故障的问题。

4. 共享锁（Locks）：共享锁在同一个进程中很容易实现，但是在跨进程或者在不同 Server 之间就不好实现了。Zookeeper 却很容易实现这个功能，实现方式也是需要获得锁的 Server 创建一个 EPHEMERAL_SEQUENTIAL 目录节点，然后调用 getChildren方法获取当前的目录节点列表中最小的目录节点是不是就是自己创建的目录节点，如果正是自己创建的，那么它就获得了这个锁，如果不是那么它就调用 exists(String path, boolean watch) 方法并监控 Zookeeper 上目录节点列表的变化，一直到自己创建的节点是列表中最小编号的目录节点，从而获得锁，释放锁很简单，只要删除前面它自己所创建的目录节点就行了。

5. 队列管理：
- 同步队列：
当一个队列的成员都聚齐时，这个队列才可用，否则一直等待所有成员到达，这种是同步队列。
队列按照 FIFO 方式进行入队和出队操作，例如实现生产者和消费者模型。
同步队列用 Zookeeper 实现的实现思路如下：
创建一个父目录 /synchronizing，每个成员都监控标志（Set Watch）位目录 /synchronizing/start 是否存在，然后每个成员都加入这个队列，加入队列的方式就是创建 /synchronizing/member_i 的临时目录节点，然后每个成员获取 / synchronizing 目录的所有目录节点，也就是 member_i。判断 i 的值是否已经是成员的个数，如果小于成员个数等待 /synchronizing/start 的出现，如果已经相等就创建 /synchronizing/start。
- FIFO队列：
实现的思路也非常简单，就是在特定的目录下创建 SEQUENTIAL 类型的子目录 /queue_i，这样就能保证所有成员加入队列时都是有编号的，出队列时通过 getChildren( ) 方法可以返回当前所有的队列中的元素，然后消费其中最小的一个，这样就能保证 FIFO。

### Hadoop 如何使用zk
- 之前的Hadoop 2.x高可用配置使用了ZooKeeper，但是我根本不知道这个外部添加的ZooKeeper工具是怎么被Hadoop调用的，当时只是知道按照教程一步步下来就能成功运行，不过现在好好把ZooKeeper研究完了，就该来好好回顾一下这个问题了。
这个是之前的Hadoop 2.x的高可用安装配置教程：http://www.coselding.cn/article/2016-05-31/hadoop2-high-available.html
- 具体的过程就不在这里重复说明了，只挑要点讲解：
Hadoop 2.x启动前要求先把ZooKeeper配置好并启动起来，这个时候ZooKeeper还是独立运行的。

Hadoop 2.x配置完成之后在启动步骤中有一步：
hdfs zkfc -formatZK
这步干嘛的？这个时候Hadoop中的多个NameNode节点都已经配置好了，这个步骤就是把NameNode的信息注册到ZooKeeper当中，包括哪个是Active，有哪些Standby，都在ZooKeeper当中进行注册记录，并且有一个ZKFC进程负责观察NameNode的状态，如果有NameNode宕机了，就马上通知ZooKeeper进行相应的记录修改，也就是说，ZooKeeper当中实时存放着NameNode的节点列表以及哪个是Active。（这部分的实时记录和更新代码存在ZKFC当中，是Hadoop本身就已经实现的代码，不需要我们自己编写，配置好就行）。

Hadoop怎么知道ZooKeeper的存在？hdfs-site.xml中不是配置了dfs.nameservices这个属性吗，这个属性就是告诉Hadoop ZooKeeper的地址，Hadoop通过这个地址连接到ZooKeeper注册NameNode的命名信息，这个动作由hdfs zkfc -formatZK这个命令触发初始化执行。

到这步就已经知道了，Hadoop中的NameNode的信息ZooKeeper都知道了，那我们是怎么访问HDFS的呢？教程中很清楚说明了，我们访问的是hdfs-site.xml中配置的dfs.nameservices来访问HDFS的，这个地址刚才说了，就是ZooKeeper的地址，所以在访问HDFS的时候就是先访问了ZooKeeper得到当前的Active NameNode，然后再用得到的Active NameNode地址再去访问HDFS。

Hadoop的Active NameNode宕机呢？ZKFC时刻检测着NameNode，当NameNode宕机的时候，通知ZooKeeper，ZooKeeper保存了所有的NameNode的地址列表，他去通知所有的Standby NameNode进行抢锁竞选，谁抢到不重要，结果是会有一个Standby NameNode抢到锁并切换为Active NameNode，并通知ZooKeeper，这个时候ZooKeeper中的数据依然是实时最新的，很完美~
这不实现了高可用和主备自动切换吗~ 简单粗暴~

### 一些概念
#### 事务操作

在ZooKeeper中，能改变ZooKeeper服务器状态的操作称为事务操作。一般包括数据节点创建与删除、数据内容更新和客户端会话创建与失效等操作。对应每一个事务请求，ZooKeeper都会为其分配一个全局唯一的事务ID，用ZXID表示，通常是一个64位的数字。每一个ZXID对应一次更新操作，从这些ZXID中可以间接地识别出ZooKeeper处理这些事务操作请求的全局顺序。

#### Watcher

Watcher（事件监听器），是ZooKeeper中一个很重要的特性。ZooKeeper允许用户在指定节点上注册一些Watcher，并且在一些特定事件触发的时候，ZooKeeper服务端会将事件通知到感兴趣的客户端上去。该机制是ZooKeeper实现分布式协调服务的重要特性。

#### ACL

ZooKeeper采用ACL（Access Control Lists）策略来进行权限控制。ZooKeeper定义了如下5种权限。

CREATE: 创建子节点的权限。
READ: 获取节点数据和子节点列表的权限。
WRITE：更新节点数据的权限。
DELETE: 删除子节点的权限。
ADMIN: 设置节点ACL的权限。
注意：CREATE 和 DELETE 都是针对子节点的权限控制。

#### ZAB协议

##### ZAB协议概览

ZooKeeper是Chubby的开源实现，而Chubby是Paxos的工程实现，所以很多人以为ZooKeeper也是Paxos算法的工程实现。事实上，ZooKeeper并没有完全采用Paxos算法，而是使用了一种称为 **ZooKeeper Atomic Broadcast（ZAB，ZooKeeper原子广播协议）** 的协议作为其**数据一致性**的核心算法。

ZAB协议并不像Paxos算法和Raft协议一样，是通用的分布式一致性算法，它是一种特别为ZooKeeper设计的崩溃可恢复的原子广播算法。

接下来对ZAB协议做一个浅显的介绍，目的是让大家对ZAB协议有个直观的了解。读者不用太纠结于细节。至于更深入的细节，以后再专门分享。

基于ZAB协议，ZooKeeper实现了一种主备模式（Leader、Follower）的系统架构来保持集群中各副本之间数据的一致性。

具体的，ZooKeeper使用了一个单一的主进程（Leader）来接收并处理客户端的所有事务请求，并采用ZAB的原子广播协议，将服务器数据的状态变更以事务Proposal的形式广播到所有的副本进程上去（Follower）。ZAB协议的这个主备模型架构保证了同一时刻集群中只能有一个主进程来广播服务器的状态变更，因此能够很好地处理客户端大量的并发请求。另一方面，考虑到分布式环境中，顺序执行的一些状态变更其前后会存在一定的依赖关系，有些状态变更必须依赖于比它早生成的那些状态变更，例如变更C需要依赖变更A和变更B。这样的依赖关系也对ZAB协议提出了一个要求：ZAB协议必须能够保证一个全局的变更序列被顺序应用。也就是说，ZAB协议需要保证如果一个状态变更已经被处理了，那么所有依赖的状态变更都应该已经被提前处理掉了。最后，考虑到主进程在任何时候都有可能出现崩溃退出或重启现象，因此，ZAB协议还需要做到在当前主进程出现上述异常情况的时候，依然能够正常工作。

ZAB协议的核心是定义了对应那些会改变ZooKeeper服务器数据状态的事务请求的处理方式，即：

> 所有事务请求必须由一个全局唯一的服务器来协调处理，这样的服务器被称为Leader服务器，而剩下的其他服务器则成为Follower服务器。Leader服务器负责将一个客户端事务请求转换成一个事务Proposal（提案）并将该Proposal分发给集群中所有的Follower服务器。之后Leader服务器需要等待所有Follower服务器的反馈，一旦超过半数的Follower服务器进行了正确的反馈后，Leader就会再次向所有的Follower服务器分发Commit消息，要求对刚才的Proposal进行提交。



##### ZAB协议介绍
从上面的介绍中，我们已经了解了ZAB协议的核心，接下来更加详细地讲解下ZAB协议的具体内容。

ZAB协议包括两种基本的模式，分别是**崩溃恢复**和**消息广播**。在整个ZooKeeper集群启动过程中，或是当Leader服务器出现网络中断、崩溃退出与重启等异常情况时，ZAB协议就会进入恢复模式并选举产生新的Leader服务器。当选举产生了新的Leader服务器，同时集群中有过半的机器与该Leader服务器完成了状态同步之后，ZAB协议就会退出恢复模式。其中，状态同步是指数据同步，用来保证集群中存在过半的机器能够和Leader服务器的数据状态保持一致。

崩溃恢复模式包括两个阶段：Leader选举和数据同步。

当集群中有过半的Follower服务器完成了和Leader服务器的状态同步，那么整个集群就可以进入消息广播模式了。


### 参考文档

IBM教程：https://www.ibm.com/developerworks/cn/opensource/os-cn-zookeeper/
官网文档：https://zookeeper.apache.org/doc/r3.4.9/
参考博客：http://luchunli.blog.51cto.com/2368057/1681841
http://www.jianshu.com/p/84ad63127cd1
