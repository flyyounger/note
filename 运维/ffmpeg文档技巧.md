### FFmpeg使用技巧
```
#从视频中提取音频(声音)保存为mp3文件
ffmpeg -i math.mp4 -f mp3 -vn apple.mp3
#截取封面
ffprobe -v quiet -show_format -print_format json math.mp4
ffmpeg -i math.mp4 -ss 00:00:14.435 -vframes 1 out.png
#把视频的前30帧转换成一个Animated Gif
ffmpeg -i input_file -vframes 30 -y -f gif output.gif
#从视频截选指定长度的内容生成GIF图片
ffmpeg -ss 3 -t 5 -i input.mp4 -s 480*270 -f gif out.gif
ffmpeg -i in.mp4 -ss 00:01:10 -to 00:01:20 -vf “fps=10,scale=320:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse
#图片转换为视频
ffmpeg -f image2 -i out%4d.png -r 25 video.mp4
#切分视频并生成M3U8文件
ffmpeg -i input.mp4 -c:v libx264 -c:a aac -strict -2 -f hls -hls_time 20 -hls_list_size 0 -hls_wrap 0 output.m3u8
#内容反转（reverse）
// For video only
ffmpeg -i input-file.mp4 -vf reverse output.mp4
// For audio and video:
ffmpeg -i input-file.mp4 -vf reverse -af areverse output.mp4
压缩视频：
ffmpeg -threads 4 -i vid1559883307870mix.mp4 -vcodec libx264 -preset fast -crf 28 -y -vf scale=1280:720 -acodec libmp3lame -ab 128k thread.mp4
录屏：ffmpeg -video_size 1024x768 -framerate 25 -f x11grab -i :0.0+100,200 output.mp4

#命令
ffmpeg [global_options] {[input_file_options] -i input_url} ... {[output_file_options] output_url} ...

ffmpeg -i [输入文件名] [参数选项] -f [格式] [输出文件] 

参数选项： 
(1) -an: 去掉音频 
(2) -vn: 去掉视频 
(3) -acodec: 设定音频的编码器，未设定时则使用与输入流相同的编解码器。音频解复用在一般后面加copy表示拷贝 
(4) -vcodec: 设定视频的编码器，未设定时则使用与输入流相同的编解码器，视频解复用一般后面加copy表示拷贝 
(5) –f: 输出格式（视频转码）
(6) -bf: B帧数目控制 
(7) -g: 关键帧间隔控制(视频跳转需要关键帧)
(8) -s: 设定画面的宽和高，分辨率控制(352*278)
(9) -i:  设定输入流
(10) -ss: 指定开始时间（0:0:05）
(11) -t: 指定持续时间（0:05）
(12) -b: 设定视频流量，默认是200Kbit/s
(13) -aspect: 设定画面的比例
(14) -ar: 设定音频采样率
(15) -ac: 设定声音的Channel数
(16)  -r: 提取图像频率（用于视频截图）
(17) -c:v:  输出视频格式
(18) -c:a:  输出音频格式
(18) -y:  输出时覆盖输出目录已存在的同名文件
-vcoder 设定视频的编码器，未设定时则使用与输入流相同的编解码器
```

### 制作更好的 GIF: 另一种更好的办法
```
首先使用 ffmpeg 转换成一个个的 PNG 文件，保持纵横比，宽为320像素，帧率为20
mkdir -p png
ffmpeg -i clip.mp4 -vf scale=320:-1 -r 20 png/output%05d.png
接下来使用 ImageMagick 的 convert 工具来把 png 连接成 gif
convert -layers Optimize -delay 4/100 png/output*.png clip.gif
```

### MP4转m3u8
```
ffmpeg -i demo.mp4 -profile:v baseline -level 3.0 -start_number 0 -hls_time 60 -hls_list_size 0 -f hls demo.m3u8
ffmpeg -i foo.mp4 -codec copy -vbsf h264_mp4toannexb -map 0 -f segment -segment_list out.m3u8 -segment_time 10 out%03d.ts
ffmpeg -i https://xxx.com/index.m3u8 -acodec copy -vcodec copy -absf aac_adtstoasc xxx.mp4

```
-profile:v baseline 大概意思是档次转成基本画质，有四种画质级别,分别是baseline, extended, main, high，从低到高
-level 3.0 大概也是视频画质级别吧，基本上是从1到5,
-start_number 0 表示从0开始
-hls_time 10 标识每60秒切一个

为什么要加上参数-vbsf h264_mp4toannexb

官方文档说Convert an H.264 bitstream from length prefixed mode to start code prefixed mode (as defined in the Annex B of the ITU-T H.264 specification).

这个问题在用ffmpeg从mp4里提取码流的时候（命令：ffmpeg -i foo.mp4 -vcodec copy -an foo.h264）也会碰到，生成的264文件无法播放，码流走中并没有00 00 00 01来分隔每一帧，这是因为默认是使用length prefixed mode，而不是以00 00 00 01起始码作为prefixed，也就是说没使用start code prefixed mode