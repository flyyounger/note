进入菜单：系统管理 –> 脚本命令行

修改某一个job
```
Jenkins.instance.getItemByFullName("qmh_test3").updateNextBuildNumber(1034)
```

修改全部job
```
import jenkins.model.Jenkins
import hudson.model.Job
Jenkins.instance.allItems(Job).each { job ->
    job.updateNextBuildNumber(1001)
    job.getNextBuildNumber()
}
return;
```

提示：jenkins.*,hudson.* 为默认导入包，可以不用显式声明。