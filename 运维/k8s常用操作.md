﻿### 访问K8S API的三种方式
1.通过client

```java
Config config = new ConfigBuilder().withMasterUrl(http).build();
KubernetesClient client = new DefaultKubernetesClient(config);//使用默认的就足够了
return client;
```

如果是HTTPS，代码为：

```java
Config config = new ConfigBuilder().withMasterUrl("https://192.168.1.100:6443")
                .withTrustCerts(true)
                .withCaCertFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/product/ca.crt")
                .withClientCertFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/product/server.crt")
                .withClientKeyFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/product/server.key")
                .build();
client = new DefaultKubernetesClient(config);
```

2.通过 http kubectl proxy
Kubectl proxy --help，不安全
```bash
kubectl proxy --address='10.180.249.149' --port=8080 --accept-hosts='^localhost$,^127\.0\.0\.1$,^10.*$'
```

3.通过https rest Api
```bash
#获取token
TOKEN=$(kubectl get secrets -o jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']=='default')].data.token}"|base64 -d)
#设置rbac
kubectl create clusterrolebinding default-admin --clusterrole cluster-admin --serviceaccount=default:default
#然后
APISERVER=$(kubectl config view | grep server | cut -f 2- -d ":" | tr -d " ")
curl -X GET $APISERVER/api/v1/namespaces/default/pods?limit=500 --header "Authorization: Bearer $TOKEN" --insecure
```

结合fabric8

```java
public class KubernetesRestAPITest {
    
  @Test
  public void testk8s(){
    Config config = new ConfigBuilder().withMasterUrl("https://192.168.1.100:6443")
               .withTrustCerts(true)
               .withCaCertFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/staging/ca.crt")
               .withClientCertFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/staging/server.crt")
               .withClientKeyFile(Thread.currentThread().getContextClassLoader().getResource("").getPath()+"k8s/staging/server.key")
               .build();
    OkHttpClient k8sClient=HttpClientUtils.createHttpClient(config);
    Request k8sRequest = new Request.Builder().url("https://192.168.1.100:6443/apis/apps/v1beta1/namespaces/default/statefulsets/").build();
    Call call = k8sClient.newCall(k8sRequest); 
    try {
      Response k8sresponse = call.execute();
      System.out.println(k8sresponse.body().string());
    } catch (IOException e) {
        e.printStackTrace();
    }
  }
}

```



4.curl+https

```bash
curl -k --cert /etc/kubernetes/pki/apiserver-kubelet-client.crt --key /etc/kubernetes/pki/apiserver-kubelet-client.key -X GET https://10.180.249.149:6443/api/v1/nodes
```
参考  https://www.jianshu.com/p/af4f9349c3c2 



### etcd连接

配置

```yaml
etcd: #key必须是PKCS8格式
  url: https://10.180.249.149:2379
  ssl: true
  ca: |
    -----BEGIN CERTIFICATE-----
    MIICwjCCAaqgAwIBAgIBADANBgkqhkiG9w0BAQsFADASMRAwDgYDVQQDEwdldGNk
    LWNhMB4XDTIwMDQyMDEwMDAzMloXDTMwMDQxODEwMDAzMlowEjEQMA4GA1UEAxMH
    ZXRjZC1jYTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANR6iBfT4Sd8
    4ljEsw1fSWxFpLUO98Y11ZoYRp7pTLWu4Qyt7BuVt4N0/gT8P2xWCrU+sp0pMv3B
    wVz0zQe87yVOMCEiJZ8k98mHitcrqUTVwx37l5oWHCIOMYD6sWlMFUSF0AJuInXB
    n/iFBSFtCiaMvOcvPHGCBPbItwfcYGBGBBpwU2uHuIKc5mQK3fc/DpMtydOrteuk
    Do0nlYEvfXA2V/pUjZhRcTbBErjEiJDWIk8BMygWIri3jQ+MzmZr+Z/NY89Luovs
    gHWrtYPhWwi8FQNq2o1c3fVQt2IBYrBOluUP1lk74k7XCQdqmsy7kkyUSDWOfNMH
    USFjmzBd87ECAwEAAaMjMCEwDgYDVR0PAQH/BAQDAgKkMA8GA1UdEwEB/wQFMAMB
    Af8wDQYJKoZIhvcNAQELBQADggEBADXtX93dhA77gmuasepyc16T9TMd6+IBW3Av
    qBjCX+UOPc14S1/npvEG7jv8jGGpjzWjKHerGCwASAkdGg/zArmEmZKgn2+SJzNJ
    /0lOZ5845TQOvvwzMY8HoByXB8fKp/JlqHDy0nM2vzNtyu7Dv0cX0L8KEd7qbp3d
    QFNo40uLyEeCKN4cfiXeGq9WE0fCP9mIycWckRu8hAZRd8kmqnenu47D6p9d38i2
    Xv/LZrmvREdy3Gz95w7ZpsOQB3iMYsWIvoysmFBI6+5aDffeijJHJP2aCtqogIiX
    5c/TLPAluGZomVkC1fd1EGGgWl319ZJq63xnrrWztUcOkeW8wlI=
    -----END CERTIFICATE-----
  cert: |
    -----BEGIN CERTIFICATE-----
    MIIDGjCCAgKgAwIBAgIIbWo4DPahPIIwDQYJKoZIhvcNAQELBQAwEjEQMA4GA1UE
    AxMHZXRjZC1jYTAeFw0yMDA0MjAxMDAwMzJaFw0yMTA0MjAxMDAwMzNaMBUxEzAR
    BgNVBAMTCms4cy5tYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
    AQDOABbHk085oJz9L89N5fUtB/EJfVjxBeNzARhDcMrTSaJOLbsQKa8j11cKP80s
    KS03rbFdVSMAF9YP4bSVUeMo16dZ89HU43jr2nttVYucmg45TVXWyuaGBII5rUUf
    MC7NG8rSX2vsdclJHYH4fj/p4/k/wJrc2sl3viiZOmxspARzpMI5oTYEPT10e9Jy
    0TSmvG65V5cine0imENQDC0sNNeiQK4W3KNb5EX/cgn6cYXy59cYZjYKBohHYBtj
    PBYtNL+hzajQ0nHpynXWraDCbajUDVoPPSo1rO9eG4U/A4mLDPIJf3kxQ+JGdL2c
    CWff3d3GrZekFlkxP3KWlT+ZAgMBAAGjcTBvMA4GA1UdDwEB/wQEAwIFoDAdBgNV
    HSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwPgYDVR0RBDcwNYIKazhzLm1hc3Rl
    coIJbG9jYWxob3N0hwQKtPmVhwR/AAABhxAAAAAAAAAAAAAAAAAAAAABMA0GCSqG
    SIb3DQEBCwUAA4IBAQBH7ccuEmA7SCh5dquS7MtjsRTVfaHgtZJTtpQbsaPqwNjV
    9odx9oTjpK07Z8TPXrLf/klS0I8cbLoBNtFKqrMsO+TZuEAHcicAYh0Lf3FpY9mD
    tDkIIPQh5E8nOaXgwWChZ1b40c+gOqiXm6yiQz0VvHZbSxX4e9k4NoKLCr43nWFB
    wYs979ItVp40Wblmk6yM3q8ybUzGBZdXE1zeyc5cbQg1MT9wxY1yx8GJsZGDN1lS
    PVBq+BDgNY2atruxVN+7f6CHelieY6PN31NBa2TdqiTlwVSFzWBd/Dpr5iRcgF4s
    aMSSeRO2UUFhK2ocV7BvVMGwETYxJwkLD3kCktvt
    -----END CERTIFICATE-----
  key: |
    -----BEGIN PRIVATE KEY-----
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDOABbHk085oJz9
    L89N5fUtB/EJfVjxBeNzARhDcMrTSaJOLbsQKa8j11cKP80sKS03rbFdVSMAF9YP
    4bSVUeMo16dZ89HU43jr2nttVYucmg45TVXWyuaGBII5rUUfMC7NG8rSX2vsdclJ
    HYH4fj/p4/k/wJrc2sl3viiZOmxspARzpMI5oTYEPT10e9Jy0TSmvG65V5cine0i
    mENQDC0sNNeiQK4W3KNb5EX/cgn6cYXy59cYZjYKBohHYBtjPBYtNL+hzajQ0nHp
    ynXWraDCbajUDVoPPSo1rO9eG4U/A4mLDPIJf3kxQ+JGdL2cCWff3d3GrZekFlkx
    P3KWlT+ZAgMBAAECggEAcxmloLl0SNSvIuB1yia0yQZo7U52RaVuorF35ya8jTXV
    VC1FGhDQZQxK+9UORKB1PWj1cxrAxaqL1q/cAJqSfB2SyjXrhjHbPJ0E3eAwc724
    ONg3IB6ak0MtqCBiQLzhSYU/3C+o06Q0ZX/xQPD5sSvJ9o8deE1eyGMduA/myQQT
    O3+E1PO7OqQ9DzYHvN1rtz78nt+hqb+IkcpM5ZYsbzG2RHYymB4cy4PyqqcqzZvb
    ypWTv4s9Th5SiBZym2RlXTJqcI30VnqH8TJ6IjFx9AyZW7PAVgA9WJ0BRNC1x795
    OMy6qrBFE6YpV5zImECvmc5dXc3pXTyxMvZiXbpjAQKBgQDokGT9noYy1vJODpnZ
    6AUzXyn/L313680WP0vs6JUY1XfYWxKiP5HiuV8vQRQ+zqqUeHuHye/nErjSWcSa
    VtsJ7Eohvkh6q4hcU4OzXK44AkqB16ZFekD/ZFB7YvSFkB0sq/62+xyWzfdhww1/
    NXhc/dd3t/o1d3FxkWBO/slzMQKBgQDiwmnk351E7r13JBa/PKRua4BX+r4VeQ4X
    2pheLdZIuHV0POZS/oHSbDxdvbJHIbgj7EGs/kuZ1Mi8RfCqL2Is1b6W4Hu8bOoC
    fT7O+NGzSwvrA6M+2hS9EMvAjH9Vh9pRPHiRqn2tBLbxB5N7Yjyz88ExTSoKzjxp
    rODEAofo6QKBgFQdpel1pOROimVhMpR2LytdiDscWi09xHf/fN67YPISg75lcl/s
    zj9K/PqCd2ggJ+J7kXKTv4m8Y8zxWwLX13HHXSjHQj/cOv3p1wDQzNLFQV2lOiZ3
    CXtWNSXrrLWCYor9yqs0OCrzZD6f5gnUtSwtQ/mxOXaNNSL2ifcIgKSBAoGAHRhm
    7/gFPOgJJXVa+dS1IHnosOE6bJToywTAUi09dn08jNqXjwSa8b3zjO+fJWNdNjbF
    QoYqeyWDUC0FMUD9LtWKK9/H4Kh06jbZzUK93Wx+rfv8gT8INC4ohp9AY2AYEh5Z
    Ng+TKpUVSB35vXYgZdKb8lB9WX+W3tRQzrWobKkCgYEA6EL0t2wgC9ocXEPfsW6F
    5Zrm4wxR6K03OErtmf91r7I3ZBKLjKHJnpFE3EruP9S9PWFScB13rJH2M7LlJivi
    nRjc+96SKEb01RyczQ6pOjiQkA7Xg8lYeIR052MoevumILOrKWpCq3r3YblnI6uk
    MWGgDygag9Vn7BWMfJzr3Tw=
    -----END PRIVATE KEY-----
  authority: k8s.master
```

Java代码

```java
@Bean                                                                                     
public Client etcdClient() {                                                             
    if (!ssl) {                                                                           
        return Client.builder().endpoints(url).build();                                   
    }                                                                                      
    final InputStream rootCA = new ByteArrayInputStream(ca.getBytes());                   
    final InputStream certCA = new ByteArrayInputStream(cert.getBytes());                 
    final InputStream keyPem = new ByteArrayInputStream(key.getBytes());                 
    try {                                                                                 
        SslContext sslContext = GrpcSslContexts.forClient()                               
                .trustManager(rootCA)                                                     
                .ciphers(Http2SecurityUtil.CIPHERS, SupportedCipherSuiteFilter.INSTANCE) 
                .keyManager(certCA, keyPem)                                               
                .build();                                                                  
        ClientBuilder clientBuilder = Client.builder().endpoints(url).authority(authority).sslContext(sslContext);                          
        Client client = clientBuilder.build();                                           
        return client;                                                                   
    } catch (SSLException e) {                                                           
        log.error("etcd ssl连接服务端报错", e);                                             
    }                                                                                     
    return null;                                                                         
}                                                                                         
                                                                                         
```

### go mod操作
```bash
go build -mod=vendor #构建项目，使用vendor下的依赖
go mod init  [module]#初始化一个go mod项目
go mod download #download modules to local cache
go mod vendor  #make vendored copy of dependencies

SET CGO_ENABLED=0  // 禁用CGO
SET GOOS=linux  // 目标平台是linux
SET GOARCH=amd64  // 目标处理器架构是amd64
```
### k8s常用命令
```bash
kubectl get pods -l app=zoo1
kubectl api-resources --namespaced=false
kubectl create namespace <insert-namespace-name-here>
helm install memcached mi/memcached -n baicai --set nodeSelector.host=www
helm status kafka -o json
helm status kafka 
helm get manifest kafka
kubeadm config view > kubeadm-config.yaml
kubectl create -f https://k8s.io/docs/tasks/administer-cluster/memory-constraints.yaml --namespace=constraints-mem-example
kubectl get limitrange cpu-min-max-demo --namespace=constraints-mem-example 
etcdctl --endpoints=https://10.180.249.149:2379 --cert=/etc/kubernetes/pki/etcd/server.crt --cacert=/etc/kubernetes/pki/etcd/ca.crt --key=/etc/kubernetes/pki/etcd/server.key get /registry/management.cattle.io/users   --prefix
.\etcdctl.exe --endpoints=https://10.180.249.149:2379 --cert=G:\soft\etcd\etcd\server.crt --cacert=G:\soft\etcd\etcd\ca.crt --key=G:\soft\etcd\etcd\server.key get /registry/management.cattle.io/users   --prefix
```

### upport-node-selection-when-using-helm-install
```
kubectl get nodes  --show-labels
helm create test-chart && cd test-chart
helm install . --set nodeSelector.databases=mysql
helm install --name elasticsearch elastic/elasticsearch --set nodeSelector."beta\\.kubernetes\\.io/os"=linux
```

### Minio设置永久下载链接
```
mc  policy  set  download  minio/mybucket #这个命令的作用是将 server 端的 mybucket 桶设置为开放管理，可以直接通过 url 进行下载。

```