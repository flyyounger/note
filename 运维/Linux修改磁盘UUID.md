The device node name of a disk (/dev/sda, /dev/hda, /dev/vda, etc.)  may change in some situations. For example, after switching cables  around or upgrading certain packages, sda & sdc could swap places.  This causes problems when /etc/fstab references filesystems by the disk  names. It is not safe to use block device node names like /dev/sda1 and  /dev/vdb2 to refer to filesystems in /etc/fstab.

Instead, use filesystem UUIDs (universally unique identifiers) or  labels. Either of these allows for identifying a filesystem without  resorting to ephemeral block device names.

**Note** : UUIDs and labels are not required if a filesystem resides on an LVM logical volume, as in default RHEL/CentOS installations.

## 1.Check the current UUID of the filesystem

1. To find of the current UUID of the filesystem you can use either of the below commands.



```
# blkid /dev/sdc1
/dev/sdc1: UUID="94ddf54e-53f7-4a1a-bd2f-d0a01ee448d1" TYPE="ext4"
# dumpe2fs /dev/sdc1 | grep UUID
dumpe2fs 1.42.9 (28-Dec-2013)
Filesystem UUID:          94ddf54e-53f7-4a1a-bd2f-d0a01ee448d1
```

You can also view the UUID in the file /etc/fstab, if there is an entry done manually for the filesyste.

```
# grep data /etc/fstab
UUID="94ddf54e-53f7-4a1a-bd2f-d0a01ee448d1"       /data     ext4    defaults        0 2
```

## 2. Changing UUID using tune2fs

1. To be able to change the UUID of the filesystem, it must be umounted first.

```
# umount /data
```

2. The **tune2fs** command allows the UUID to be changed using the **-U** flag. The -U flag allows generation of a random UUID or time-based UUID.

```
# tune2fs -U random /dev/sdc1
tune2fs 1.42.9 (28-Dec-2013)
```

3. When modifying existing UUIDs, make sure to update any references  to the old labels in fstab. First check for the new UUID and then make  appropriate changes in the /etc/fstab file.

```
# blkid | grep sdc1
/dev/sdc1: UUID="d2c27808-f88f-44bc-bb1d-de3b03d133e4" TYPE="ext4"
# vi /etc/fstab
UUID="d2c27808-f88f-44bc-bb1d-de3b03d133e4"       /data     ext4    defaults        0 2
```

4. Mount the filesystem back again.

```
# mount /data
```