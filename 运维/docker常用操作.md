#### docker基本操作
```
#拉取镜像
sudo docker pull wiremind/omnidb
sudo docker images
#列出运行中和未运行的容器
sudo docker ps -a
#停止容器
sudo docker stop ee810
docker stop $(docker ps -aq)
#启动容器
sudo docker start ee810
#依次删除容器和镜像
sudo docker rm ee810
sudo docker rmi 4ce14c00ed3a
docker ps -a |awk '{print $1}'|xargs docker rm -f
```
### 高级操作
```
 # 设置内存限制
 sudo docker run -m 200M --memory-swap=300M --name myredis  -p 6379:6379  -v /develop/data/redis:/data -d --restart=always redis:latest redis-server --appendonly yes --requirepass "123456"
 #  进入容器
 docker exec -t -i mysql57 /bin/bash
 docker exec -it a126ec987cfe redis-cli -a '123456'
 # 查看日志
 sudo docker logs 352a
 # 导出容器
sudo docker save 414b -o debian.tar.gz
#合并后导出
sudo docker export 414b -o debian.tar.gz
#递归地输出指定镜像的历史镜像
docker history <image-id>
#导入镜像
docker import  my_ubuntu_v3.tar runoob/ubuntu:v4
#使用host网络
docker run -p 8002:8002 --name nginxds --network host
#查看docker空间统计
docker system df
#释放空间，慎用
docker system prune
docker cp 容器名：要拷贝的文件在容器里面的路径 要拷贝到宿主机的相应路径
```
### 常用操作
```
#运行容器
docker run --name slaveredis  -p 6380:6380  -v /develop/data/redis-slave:/data -v /develop/data/redis-slave.conf:/usr/local/etc/redis/redis.conf -d redis:latest redis-server /usr/local/etc/redis/redis.conf
docker run --name mysql -v /develop/data/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306  -d mysql:8.0.15.24
docker run -p 9000:9000 --name php72  -v /develop/data/www/:/www --privileged=true -d php:7.2-fpm

docker run -p 8001:8001 --name nginx -v /develop/data/www/:/www -v /develop/data/nginx/conf.d:/etc/nginx/conf.d -v /develop/data/nginx/logs:/wwwlogs --link php72:php --privileged=true -d nginx:latest

docker run -it --name composer -v /docker/www:/app --privileged=true composer <要执行的composer命令>

docker run -it --name composer -v /docker/www:/app --privileged=true composer composer create-project --prefer-dist laravel/laravel ./ 5.5.*
#HTTP API:
cat  /lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd  -H=tcp://127.0.0.1:2375 -H=fd://
curl http://127.0.0.1:2375/v1.39/containers/json?all=true
```
### docker私有仓库搭建
```
docker pull registry
docker run -d -p 5000:5000 --restart=always --name=registry-srv -v /develop/dockerrepo:/var/lib/registry registry
#然后下载web容器
docker pull hyper/docker-registry-web
docker run -it -p 8080:8080 --restart=always --name registry-web --link registry-srv -e REGISTRY_URL=http://registry-srv:5000/v2 -e REGISTRY_NAME=localhost:5000 hyper/docker-registry-web
#改下配置，重启docker
cat /etc/docker/daemon.json
{
 "registry-mirrors": ["https://registry.docker-cn.com"],
 "insecure-registries": ["192.168.94.192:5000"]
}
systemctl restart docker
#推送
docker tag redis 192.168.94.192:5000/chen/redis
docker push 192.168.94.192:5000/chen/redis
```
访问：http://192.168.94.192:8080/ 还需要让私有仓库镜像可以删除，需要编辑/etc/docker/registry/config.yml文件 增加下面配置
```
storage:
 delete:
   enabled: true
   ```
重启registry-srv容器。 查看镜像元信息
```
curl --header "Accept: application/vnd.docker.distribution.manifest.v2+json" -I -X  HEAD http://192.168.94.192:5000/v2/chen/redis/manifests/latest
#
HTTP/1.1 200 OK
Content-Length: 1572
Content-Type: application/vnd.docker.distribution.manifest.v2+json
Docker-Content-Digest: sha256:d7371d6c24ec0dd4b0834cfdf08789eee95c648198180be5b0eff64754b027ff
Docker-Distribution-Api-Version: registry/2.0
Etag: "sha256:d7371d6c24ec0dd4b0834cfdf08789eee95c648198180be5b0eff64754b027ff"
X-Content-Type-Options: nosniff
Date: Tue, 19 Mar 2019 02:56:38 GMT
```
删除
```
curl -v -X DELETE http://192.168.94.192:5000/v2/chen/redis/manifests/sha256:d7371d6c24ec0dd4b0834cfdf08789eee95c648198180be5b0eff64754b027ff
```
但这只是删除了元数据，镜像数据并没有删除，还需要进入容器删除
```
docker exec -it registry-srv /bin/sh
du -sh /var/lib/registry/
registry garbage-collect /etc/docker/registry/config.yml
```
参考文章：[https://segmentfault.com/a/1190000015629878](https://segmentfault.com/a/1190000015629878)


