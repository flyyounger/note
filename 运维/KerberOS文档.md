### Kerberos 命令

#### 登陆 kinit

    kinit -kt /etc/security/keytabs/hdfs.headless.keytab hdfs-cluster_101@BIGDATA

#### 查询登陆状态 klist

    [root@manager dataspace]# klist
    Ticket cache: FILE:/tmp/krb5cc_0
    Default principal: chen_001@BIGDATA
    
    Valid starting       Expires              Service principal
    10/28/2019 18:30:13  10/29/2019 18:30:13  krbtgt/BIGDATA@BIGDATA
    	renew until 11/04/2019 18:30:13
#### 登录后台  kadmin.local

    [root@manager dataspace]# kadmin.local
    Authenticating as principal chen_001/admin@BIGDATA with password.
    kadmin.local: 
#### 查看用户列表 listprincs

    kadmin.local:   listprincs

#### 创建用户 add_principal

    kadmin.local:  add_principal chen_001
    WARNING: no policy specified for chen_001@BIGDATA; defaulting to no policy
    Enter password for principal "chen_001@BIGDATA": 
    Re-enter password for principal "chen_001@BIGDATA": 
    Principal "chen_001@BIGDATA" created.

#### 删除用户 delete_principal

    kadmin.local:  delete_principal test1
#### 只导出用户keytab文件(并且不要修改密码)
```
 ktadd -k /etc/security/keytabs/chen_001.keytab -norandkey chen_001@BIGDATA
 #使用Keytab验证是否可以登录(无错误输出即可)
  kinit -kt /etc/security/keytabs/chen_001.keytab chen_001@BIGDATA
 ```
 #### 查看keytab文件中的帐号列表

      klist -ket /etc/security/keytabs/hdfs.headless.keytab

 #### 设置密码策略(policy)
 

    #kadmin.local:  
    addpol -maxlife "90 days" -minlife "75 days" -minlength 8 -minclasses 3 -maxfailure 10 -history 10 chen_pol
    #修改用户策略
    modprinc -policy chen_pol chen_001@BIGDATA
    #查看策略
    getpol chen_pol
    
    
    

