### rpm需要用到几个命令
```bash
yumdownloader --resolve --destdir=/var/www/html/InsightSPRMPP/hdp_utils/mpp_deps/ perl-Git #下载rpm依赖包
rpmrebuild -e -p --notest-install nginx_conf-1.0-0.noarch.rpm #rpm重新打包
rpm -qpR ambari-server-2.7.1.0-0.x86_64.rpm #查询RPM依赖
rpm -qpicdlR  ambari-server-2.7.1.0-0.x86_64.rpm #查询rpm基本信息和依赖
#以.rpm为扩展名的文件解压缩：
rpm2cpio file.rpm | cpio -div
```