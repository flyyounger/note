## zip文件格式

`.zip` 文件格式由**菲尔·卡茨(Phil Katz)**发明，属于当下主流的压缩格式之一

> `.zip` 的竞争者包括 `.rar` 格式和开放源码的 `.7z` 格式
>
> 从性能上比较，`.rar` 和 `.7z` 较 `.zip` 格式压缩率更高，而且 `.7z` 由于提供了免费的压缩工具而在更多领域得到应用

对于一个 `.zip` 文件来说，它由**3**部分组成，而每部分都有对应的**文件头标记**：

- 压缩源文件数据区（简称**数据区**），文件头为 `50 4B 03 04`
- 压缩源文件目录区（简称**目录区**），文件头为 `50 4B 01 02`
- 压缩源文件目录结束标志（简称**目录结束标志**），文件头为 `50 4B 05 06`

其中，所有文件头都有 `50 4B`，其对应的ASCII码为 `PK`，纪念 `.zip` 的发明人**Phil Katz**先生

------

我们首先来自制几个 `.zip` 文件

创建**文本文件** `1.txt`、`2.txt`、`3.txt`，里面分别只有一句话："First Text"、"Second Text"、"Third Text"（无回车符）

然后通过Bandizip或其它压缩软件，将3个 `.txt` 压缩成 `temp.zip`（无需密码）

我们用十六进制编辑器打开 `temp.zip`：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/header.png)

由于我们在 `temp.zip` 中放置了**3**个 `.txt` 文件，因此可以看到有**3**个表示**数据区**的文件头 `50 4B 03 04`

### 数据区

**数据区**又可以划分为**3**部分：

- **File Header** (文件头)
- **File Data** (文件数据)
- **Data Descriptor** (数据描述符)

**File Header**用于标识该文件的开始，它的结构如下：

| 偏移量(Offset) | 长度(Length) | 内容(Contents)               |
| -------------- | ------------ | ---------------------------- |
| 0              | **4** bytes  | 文件头(`50 4B 03 04`)        |
| 4              | 2 bytes      | 解压需要的**pkware**最低版本 |
| 6              | 2 bytes      | 通用位标记                   |
| 8              | 2 bytes      | **压缩方式**                 |
| 10             | 2 bytes      | 文件最后修改时间             |
| 12             | 2 bytes      | 文件最后修改日期             |
| 14             | **4** bytes  | **CRC-32校验码**             |
| 18             | **4** bytes  | 压缩后大小                   |
| 22             | **4** bytes  | 未压缩大小                   |
| 26             | 2 bytes      | 文件名长度（假设为**n**）    |
| 28             | 2 bytes      | 扩展区长度（假设为**m**）    |
| 30             | **n** bytes  | 文件名                       |
| 30+n           | **m** bytes  | 扩展区                       |

**File Data**实际存储着被压缩文件的数据

而**Data Descriptor**，它只在**文件被加密**的情况下才会出现，就未加密的情况而言，它为空

------

我们的 `temp.zip` 压缩着 `1.txt`、`2.txt`、`3.txt`，分别对应**3**个**数据区**，我们来详细看看第一个**数据区**：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/first_data_chunk.png)

依次分析为：

- 红色方框的 `50 4b 03 04` 代表**数据区**的文件头标识

- `14 00` 表示pkware的最低版本、`00 00` 表示通用位标识、`08 00` 表示压缩方式、`72 bd` 和 `4d 50` 分别表示最后的修改时间和修改日期

- `03 c3 94 58` 是**CRC-32校验码**

- 两个绿色方块分别表示压缩后、压缩前的文件体积大小 `0c 00 00 00` 和 `0a 00 00 00`

  值得一提的是，`.zip` 在存储文件大小的时候，使用的是**小端序**

  > **字节序**
  >
  > 计算机在存放**字节**数据的时候有**2**种顺序——**大端序**和**小端序**
  >
  > **大端序**指高位字节在前、低位字节在后；**小端序**相反，低位字节在前、高位字节在后
  >
  > 举例来说，十六进制数值 `0x1234567` 的**大端序**为 `01 23 45 67`、**小端序**为 `67 45 23 01`（注意计算机以**8** bits(1 byte)为基本单位，不足8 bits补齐）
  >
  > 我们人类习惯读写**大端序**，而在计算机电路中，优先处理**低位字节**效率会比较高，因而采用**小端序**存储
  >
  > 计算机在处理字节时，并不知道什么是**大端序**什么是**小端序**，它只会按顺序读取字节，先读第一个字节，再读第二个
  >
  > （事实上，`.zip` 存放大多数数据都采用**小端序**）

  所以，按**小端序**存储的 `0c 00 00 00` 实际上表示的数值是 `0xc`，对应十进制 `13`；同理，`0a 00 00 00` 对应的数值是 `0xa`，十进制为 `10`

  我们打开 `1.txt` 的详细信息，里面记载着 `1.txt` 的大小：**10字节**，恰好就是第二个绿色方框显示的数值（压缩前文件体积）

  > 为什么压缩后，文件大小反而由 `10` 增加到了 `13`？
  >
  > 这里涉及压缩的原理
  >
  > 假如文件中有大量重复的数据，如 `ABABABABABABCD`，如果我们把 `AB` 替换成 `X`，就成为了 `XXXXXXCD`，再在后面补充上 `X=AB`，这样会简洁很多
  >
  > 我们把"在后面补充上 `X=AB` "称之为**增加控制信息**
  >
  > **增加控制信息**对应压缩来说是必须的步骤，但是如果压缩前文件信息本身就很**紧凑**，根本不用压缩，那么增加的**冗余控制信息**反倒会使体积变大

回到这张图：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/first_data_chunk.png)

- `05 00` 表示文件名长度、`00 00` 表示扩展区长度

  很显然，`.zip` 对它们的存储也是通过**小端序**

- 蓝色方框表示**文件名**，它占用的字节数由前面的 `05 00` 决定，`31 2e 74 78 74` 对应的ASCII码就是 `1.txt`

由于扩展区长度为 `00 00`，所以截止至**文件名**，就是**数据区**的**File Header**部分了

黑色方框对应的是**File Data**部分，随后就进入下一个**数据区 `50 4B 03 04`** 了（因为该 `.zip` 未加密，所以**Data Descriptor**部分为空）

> 可以通过Python的**zlib**模块实现模拟 `.zip` 的压缩和解压
>
> 还记得我们的 `1.txt` 的数据吗？——"First Text"
>
> **zlib**对这段字符串进行压缩的Python 3代码为：
>
> ```
> 
> ```

```
import zlib
print(zlib.compress(b"First Text"))
	# b'x\x9cs\xcb,*.Q\x08I\xad(\x01\x00\x14g\x03\xce'
```

然后我们将 `.zip` 中的黑色方框部分的数据显示出来：

```
s = b"\x73\xcb\x2c\x2a\x2e\x51\x08\x49\xad\x28\x01\x00"
print(s)
	# b's\xcb,*.Q\x08I\xad(\x01\x00'
```

> 对比两次输出结果，我们发现，黑色方框中提取出来的数据只是"First Text"经过压缩的**一部分**，分别缺少了前面的 `b'x\x9c'` 和后面的 `b'\x14g\x03\xce'`
>
> > 个人理解是，
> >
> > `.zip` 中的 `73 cb 2c 2a 2e 51 08 49 ad 28 01 00` 才是 `1.txt` 真正的压缩数据，而首尾多出来的信息应该是属于**压缩过程中添加的控制信息**
> >
> > 用 `2.txt` 来测试下，发现**File Data**的显示信息为 `b'\x0bNM\xce\xcfKQ\x08I\xad(\x01\x00'`，而"Second Text"经zlib压缩得到 `b'x\x9c\x0bNM\xce\xcfKQ\x08I\xad(\x01\x00\x18^\x04"'`
> >
> > 可以看到，同样增加了前面的 `b'x\x9c'` 和后面的 `b'\x18^\x04"'`
> >
> > 我们用 `zlib.decompress()` 去直接解压 `.zip` 中的**File Data**，会发现报错：`zlib.error: Error -3 while decompressing data: incorrect header check`（错误的文件头检测）
> >
> > 因此猜测，额外添加的**控制信息**中还有确保解压正确的信息

------

理清楚了一个**数据区**中的部分数据含义后，我们再来看看之前被我们忽略的：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/ms_dos_time.png)

仍然是 `1.txt` 在 `temp.zip` 的**数据区**部分，这次我们讲点其它的

蓝色部分分别是**最后修改时间**和**最后修改日期**，两者都采用的是**MS-DOS格式**（因为 `.zip` 程序是在MS-DOS上发明的）

- **MS-DOS时间**

  将"时"、"分"、"秒"转换成**16 bits**格式（即2 bytes），转换过程为：

  | 位      | 内容                   |
  | ------- | ---------------------- |
  | 0 - 4   | 小时(24小时制，0 - 23) |
  | 5 - 10  | 分钟(0 - 59)           |
  | 11 - 15 | 秒除以2                |

  > 就拿上面的 `72 bd` 举例，由于采用**小端序**，因此实际存储的数值为 `0xbd72`
  >
  > 我们查看 `1.txt` 的最后修改时间（我是通过**exiftool**），得到 `2020:02:13 23:43:35`
  >
  > 如何由时间 `23:43:35` 得到 `bd72` 呢？
  >
  > 我们用Python将 `bd72` 转化成16位二进制字符串：
  >
  > ```
  > 
  > ```

```
n = "bd72"
n = bin(int(n,16))[2:].zfill(16)
	# n = '1011110101110010'
```

按照上面表格的截取，得到

```
n1 = n[0:5] # '10111'
n2 = n[5:11] # '101011'
n3 = n[11:] # '10010'
```

最后分别由二进制转成十进制：

```
print(int(n1,2)) # 23
print(int(n2,2)) # 43
print(int(n3,2)) # 18
```

`n1`、`n2`、`n3` 分别得到 `23 43 18`，对比时间 `23:43:35`，将**秒**乘以2

后，整个时间只有1

- > 秒的误差！

- **MS-DOS日期**

  **此表存疑**

  | 位     | 内容 |
  | ------ | ---- |
  | 0 - 4  | 年份 |
  | 5 - 8  | 月份 |
  | 9 - 15 | 日   |

  > 我没能在网上找到**MS-DOS日期和时间**转16 bits值的公式
  >
  > 但是依照上表进行解析，似乎发现了一点规律：
  >
  > - 我在 `2020/02/13` 新建了一个 `.txt`，压缩为 `.zip` 后查看日期为 `4d 50`，根据上表进行解析，得到 `10 0 77`
  > - 我在后一天 `2020/02/14` 进行了同样的操作，得到结果 `10 0 78`
  >
  > 感觉**MS-DOS日期**是以某年某天为参考系的...

回到这张图：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/ms_dos_time.png)

红色方框存储着**CRC-32校验码**

在学习 `.png` 格式的过程中，就已经了解过**CRC-32**是一种生成8 bytes(32 bits)固定长度字符串的哈希算法

`.zip` 的**CRC-32校验**只对**源文件的内容**进行运算，例如，`1.txt` 的文件内容为"First Text"，用Python 2快速计算下：

```
import zlib
temp = hex(zlib.crc32("First Text") & 0xffffffff)
print temp
	# 0x5894c303L
```

因为是**小端序**，所以可看到输出结果与图中的 `03 c3 94 58` 一致

------

#### CRC碰撞

> 在CTF，可能给你一个加密了的 `.zip` 文件，并且密码很长，不太可能通过暴力破解得到密码；但是里面只包含一个小小的 `.txt` 文件，记录着**长度不到30**的flag

我们留意到**CRC-32校验码**只对源数据进行运算，如果我们无法爆破 `.zip` 的密码，那么不妨直接爆破里面的flag

HBCTF中的一道题：

> 链接：https://pan.baidu.com/s/10fWciZ0OLhYxa_ZVBmp6tg
> 提取码：rfnb

 

**解答**

打开 `.zip`，看到

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/flag_six.png)

提示我们flag为6位数，并且根据原始大小 `6` 猜测文件 `flag6位数` 中只包含为flag的那6位数；又知道了这6位数的**CRC-32校验码**，写脚本对这6位数进行爆破：

```
# Python 2
import zlib
for i in range(100000, 1000000):
	if hex(zlib.crc32(str(i)) & 0xffffffff)[2:-1] == "9c4d9a5d":
		print i
```

最后得到的输出结果 `954288` 就是flag了

> 限于CPU能力，**CRC碰撞**只能用于压缩文件较小的情况

------

#### CRC碰撞脚本

**CRC碰撞**有时是极耗费时间的，所以不妨借助一些经过优化的工具来加速碰撞

- 文件字节数≤

5时（可以自己写脚本爆破）

我在网络上找到一个适用于字节数≤

5的Python 3的**CRC碰撞**脚本 `crcak.py`

> 链接：https://pan.baidu.com/s/1ucUlL8twOFFS1g6e0P1kjw
> 提取码：jjl6 

使用方法：`python crcak.py temp.zip`，脚本会自动判别字节数

（实测字节数等于6也可以，但是非常慢）

文件字节数等于6时，借助[Github](https://github.com/theonlypwner/crc32)上的脚本 `crc32.py`

将脚本clone到本地，可以使用 `-h` 参数查看用法：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/script_crc32.png)

单就**CRC碰撞**来讲，它的用法为：`python crc32.py reverse 0x8位校验码`

长度为6 bytes文本的**CRC校验码**已经有可能出现重复了：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/crc32_same.png)

该脚本会快速罗列出所有有可能的字符串

> 值得一提的是，打开 `crc32.py` 可以看到它的字符集：
>
> 
>
> 很显然它缺少一些特殊字符，有可能导致无法碰撞出正确结果；自行添加上去即可

文本长度≥

- 7的就不推荐使用**CRC碰撞**了

------

到这里，**数据区**的分析就告一段落

注意，**数据区**的名字虽然是"数据"，但它的定义是**记录**，**数据区**中每一个压缩的源文件/目录都是一条**记录**

真正存储数据的地方是后面的**目录区**

------

### 目录区

**目录区**在官方文档中的定义是**核心目录(Central Directory)**，**数据区**中的每一条记录都对应**目录区**中的一条数据

就结构而言，**目录区**和**数据区**还是很相似的

我们用**数据区**来对比，查看 `1.txt` 在 `temp.zip` 中的**目录区**数据：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/empty_data.png)

↑ 数据区截图

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/dir.png)

**数据区**的文件头标识为 `50 4B 03 04`，而**目录区**则是 `50 4B 01 02`，对应上图的黑色方框

在**数据区**，原本文件头标识后面就是2 bytes的pkware版本，而在**目录区**，它被扩展为4 bytes，前2 bytes仍然是新出现的**压缩所用的pkware版本**，后2 bytes则是原先的**解压所需的pkware最低版本**

后面的基本与**数据区**相同，`00 00` 表示通用位标记，`08 00` 表示压缩方法，`72 bd 4d 50` 分别表示最后修改时间和日期，`03 c3 94 58` 表示源数据的CRC-32校验码，`0c 00 00 00` 和 `0a 00 00 00` 分别表示压缩后文件大小、压缩前文件大小，`05 00` 表示文件名长度，`24 00` 表示扩展区长度

> 在**目录区**，扩展区开始不为空

图中，随后的**5**个蓝色方框都是相比较**数据区**，新出现的东西

> 然而这些都不在我们的主要讨论范围内，看一下就够了

首先的 `00 00` 表示"文件注释长度"，之后的 `00 00` 表示"文件开始位置的磁盘编号"，再之后的 `00 00` 表示"内部文件属性"

`20 00 00 00` 表示"外部文件属性"，`00 00 00 00` 表示"本地文件头的相对位移"

跳过这些后，到了表示**文件名**的 `31 2e 74 78 74`

**目录区**不像**数据区**由①File Header；②File Data；③Data Descriptor组成，所以文件名结束后，就到了**拓展区**和**文件注释内容**，它们分别由之前的长度确定

------

请看这个：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/flag.png)

它是我们一直没怎么在意的**通用位标记**，它同时存在于**数据区**和**目录区**，但只在**目录区**发挥效果

> 因为**数据区**只是**记录**

我们把绿色方框中的2 bytes数据称为**通用位标记(General Purpose Bit Flag)**，实际上它与 `.zip` 的**加密**有关，这2 bytes对应的16 bits也被称之为**加密位**

我们重复创建 `temp.zip` 的操作——将 `1.txt`、`2.txt`、`3.txt` 压缩成 `.zip` 文件，但是这次设置了密码 `abcd`，得到 `secret.zip`

在Bandizip中分别打开 `temp.zip` 和 `secret.zip`，可以看到：标注了 `*` 号为加密文件：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/jiami_or_not.png)

使用十六进制编辑器打开 `secret.zip`，查看**数据区**：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/jiami.png)

与原先未加密进行对比，变动的主要是上图各色方框的位置

> **关于 `.zip` 的加密算法**
>
> `.zip` 是允许多种无损压缩算法的，但最常用的算法为**Deflate**，以及Deflate的微型改进版——**Deflate64**
>
> **zip压缩软件**在得到用户设置的密码后，会根据密码动态修改自身的压缩算法（其实是改变了压缩算法中的个别参数）
>
> 正因为如此，通过密码而动态改变的压缩算法本质上起到了**加密算法**的作用
>
> 【参考】：https://bbs.csdn.net/topics/10315681

我们首先看加密后的红色方框和黑色方框部分，可以看到，经过ZIP加密后，加密后的 `1.txt` 文件体积增加到了 `0x18` bytes，而下方的**File Data**区域也经过了加密，变得不可读

**数据区**是**目录区**的记录，因此**目录区**也发生了相同的变化：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/secret_dir.png)

`1.txt` 的真实压缩数据位于**数据区**的**File Data**，而**目录区**没有，因此加密后**目录区**的变化少于**数据区**

> ......

 

重点在于绿色方框中的数据

我们参考pkware的[官方文档](https://pkware.cachefly.net/webdocs/APPNOTE/APPNOTE-6.2.0.txt)，在**general purpose bit flag**一栏中能够看到详细的解释：

```
general purpose bit flag: (2 bytes)

          Bit 0: If set, indicates that the file is encrypted.

          (For Method 6 - Imploding)
          Bit 1: If the compression method used was type 6,
                 Imploding, then this bit, if set, indicates
                 an 8K sliding dictionary was used.  If clear,
                 then a 4K sliding dictionary was used.
          Bit 2: If the compression method used was type 6,
                 Imploding, then this bit, if set, indicates
                 3 Shannon-Fano trees were used to encode the
                 sliding dictionary output.  If clear, then 2
                 Shannon-Fano trees were used.
......
```

将**加密位**的**2** bytes展开成16 bits，这16个二进制位，每一位都相当于一个开关；当某一位上的开关被打开(设置为 `1`，set)，那么就会启动相应的功能

我们看文档中的 `Bit 0`，它强调：**如果开启，则表明该文件已加密**

也就是说，16 bits中的最低位如果为 `1`，那么 `.zip` 会认为这是一个已经经过加密的文件；倘若人为地修改**未加密文件**的加密位的最低位为 `1`，那么哪怕没有设置密码，`.zip` 也会判定它是一个已加密文件

这也就引申出CTF中的一类题目——**zip伪加密**

------

#### zip伪加密

人为地修改**目录区**中，**加密位**的最低位为 `1`，ZIP压缩软件将判定 `.zip` 经过加密；解压时，会在没有设置密码的情况下，要求输入密码，否则解压失败

> **位运算**
>
> 在学习位运算的时候，就有过一道经典的题目：快速判断一个数是否是奇数
>
> 常规做法是，`if (n % 2 == 1)` 通过**取模**运算
>
> 而在位运算中，更快的做法是 `if (n & 1 == 1)` 
>
> 理由是，所有的数如果都转换成二进制表示，那么每个数都可以表示成：*a**n*×2*n*+*a**n*−1×2*n*−1+*a**n*−2×2*n*−2+...+*a*1×21+*a*0×20



（*a*0

到*a**n*

是各二进制比特位上的取值）

因此，一个数的奇偶性就取决于最后一位*a*0

> 的取值

ZIP压缩软件会将**加密位**最低位为 `1` 视为加密，也就是说，加密位上的数值被修改为**奇数**，那么都将实现**伪加密**

------

实验一下，十六进制编辑器打开 `temp.zip`，定位到 `1.txt` 的**目录区**的加密位上，手动把它更改为 `07 00`：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/change.png)

Bandizip打开 `temp.zip`，发现：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/just_one_encrypt.png)

可以看到，在没有设置密码的情况下，解压 `1.txt` 需要输入密码

> 这种情况一看就是**zip伪加密**，正常的压缩包加密都是把里面所有的文件都加密的，不可能出现单个文件加密、其余未加密的情况
>
> 所以通常将每个文件的**加密位**都做手脚，让**zip伪加密**更难被发现

------

由于**zip伪加密**仅仅只是修改了**加密位**的最后一位，使得ZIP压缩软件错误进行了"索要密码"的行为，除此之外，对 `.zip` 没有任何修改

这也意味着**zip伪加密**是脆弱的

- `binwalk -e` 能够无视**伪加密**，直接解压出里面的文件
- 在Mac OS以及部分Linux(如Kali)中，可以直接打开**伪加密**的 `.zip` 压缩包

当压缩文件较多，或者我们不想逐个字节的去找到**加密位**的时候，可以借助一些工具——检测**zip伪加密**的Java小工具 `ZipCenOp.jar`

> 链接：https://pan.baidu.com/s/1SokyYdAW8C8Pfs1kRTm5Dw
> 提取码：e48i 

使用方法为：打开CMD窗口，键入

```
java -jar ZipCenOp.jar r temp.zip # 解密
java -jar ZipCenOp.jar e temp.zip # 加密
```

解密时，会显示 `success x flag(s) found`，这时再次打开 `.zip`，会发现表示加密的 `*` 已经消失了，能够直接打开

> **注意**
>
> 亲测， `ZipCenOp.zip` 对正常加密的 `.zip` 压缩包会直接修改所有**加密位**为偶数，虽然会让ZIP压缩软件显示"未加密"，但往往 `.zip` 文件也损坏了，得到"CRC校验错误"的结果
>
> 所以如果拿到加密了的 `.zip` 文件，还是建议用十六进制编辑器打开，将**加密位**手动改回偶数；如果发现损坏了，则证明不是**伪加密**，再把**加密位**改回，尝试其它办法
>
> > 可以比对**数据区**和**目录区**的**加密位**，**伪加密**往往只会修改**目录区**的**加密位**（因为这样就能起到效果），而正常的加密会同时修改两者的**加密位**
> >
> > 如果出题人把两处的**加密位**都修改了，那只好一一尝试了

此外，我在之前认为，一个压缩包中的文件要么不加密、要么全加密，不可能出现**部分文件加密、部分文件不加密**的情况，这种情况的出现只能是**伪加密**导致的；然而经过测试，真的可以做到这样

以Bandizip压缩软件为例：

- 先将 `temp1.txt` 压缩为 `try1.zip`，然后用Bandizip打开 `try1.zip`，把 `temp2.txt` 拖入，加入压缩包中；这时可以选择为 `temp2.txt` 添加密码
- 或者先将 `temp1.txt` 加密压缩为 `try2.zip`，然后拖入 `temp2.txt` 时选择不加密

经过测试，`.zip` 和 `.rar` 都允许这样的操作

更有甚者，把 `temp1.txt` 加密压缩时选择一个密码，把 `temp2.txt` 拖入时选择另外一个密码，这样在解压时，需要输入两个密码Orz

所以这个不能再作为是否是**伪加密**的依据了

------

### 目录结束标志

我们已经大致浏览过了文件头为 `50 4B 03 04` 的**数据区**、文件头为 `50 4B 01 02` 的**目录区**，那么对于一个 `.zip` 文件，最后的部分就是文件头为 `50 4B 05 06` 的**目录结束标志**了

这部分在CTF中没什么应用，适当忽略，在这里仅作记录

我们查看 `temp.zip` 的**目录结束标志**部分：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/end_dir.png)

`50 4b 05 06` 是**文件头**，`00 00` 表示当前磁盘编号， `00 00` 表示目录区开始磁盘编号，`03 00` 表示本磁盘上记录总数，`03 00` 表示目录区记录总数，`05 01 00 00` 表示目录区尺寸大小，`8e 00 00 00` 表示目录区对第一张磁盘的偏移量，`00 00` 表示ZIP文件的注释长度

------

## Zip操作

### 压缩

我们可以借助Linux下的 `zip` 命令来创建 `.zip` 压缩文件

举例：把 `1.txt`、`2.txt`、`3.txt` 压缩为 `temp.zip`

直接压缩：

```

 zip temp.zip 1.txt 2.txt 3.txt
```

**命令 `zip` 要求把 `.zip` 文件名罗列在前，待压缩文件在后面**

如果把 `1.txt`、`2.txt`、`3.txt` 放入文件夹 `dir` 中，再把整个文件夹 `dir` 压缩为 `temp.zip`，需要指定参数 `-r` （递归执行）

```

 zip -r temp.zip dir
```

> 如果缺少参数 `-r`，那只会单独将一个空的 `dir` 文件夹压缩进 `temp.zip`

假如要向 `temp.zip` 中加入一个 `a.jpg`，添加参数 `-g`，就不会产生新的压缩文件：

```

 zip -g temp.zip a.jpg
```

如果 `1.txt` 有更改，需要把 `temp.zip` 中的 `1.txt` 也更新，添加 `-u` 参数：

```

 zip -u temp.zip 1.txt
```

删除 `temp.zip` 中的 `3.txt`，添加参数 `-d`：

```

 zip -d temp.zip 3.txt
```

压缩**3**个 `.txt` 文件时，设置密码：

```

 zip temp.zip 1.txt 2.txt 3.txt -e
```

然后会要求输入密码、验证密码

为每个压缩文档**单独**添加单行注释，使用参数 `-c`；为整个 `.zip` 文件添加多行注释，使用参数 `-z`（两者都是交互模式）

**参数罗列**

```
-r	递归执行
-g	向.zip中额外添加文件
-u	更新.zip中已存在的文件
-d	删除.zip中的文件
-e	加密
-c	每个文件单行注释
-z	.zip文件多行注释
```

> **中文密码**
>
> 到目前为止，还有挺多的压缩软件不支持密码为**非ASCII可打印字符**
>
> - Bandizip对 `.zip` 的压缩不支持中文密码
> - Linux的 `zip` 命令输入中文密码，解密时会出错

> **压缩方法**
>
> 注意，哪怕同为 `.zip` 压缩，实测Linux下的 `zip` 命令和Win10下的压缩软件Bandizip进行的操作是不同的
>
> > 还有，WinZip的私有压缩格式**zipx**对 `.jpeg` 压缩率比同类软件好，能减少20 ~ 30%——这也是对压缩方法不同的一个佐证
>
> 对于如何判断一个 `.zip` 是否采用**相同的压缩方法**，我查阅了很多资料，都没有发现...
>
> 注意压缩前后的**文件大小**是不能作为判断依据的，理由后面讲

------

### 解压

同样借助的是Linux下的 `unzip` 工具

直接解压：

```

 unzip temp.zip
```

将文件解压到指定的目录下，使用参数 `-d`：

```

 unzip -d /d/xxx temp.zip
```

当解压出来的文件与当前目录下的文件**重名**时，`unzip` 会出现选项：`[y]es, [n]o, [A]ll, [N]one, [r]ename`，可以添加参数 `-n`，它的效果跟选择`[n]o`  选项一样，不会将重名文件解压出来

不进行解压，先查看 `.zip` 中的文件信息，添加参数 `-l`：

```

 unzip -l temp.zip
```

参数 `-v` 查看文件列表，以及**压缩方法**和**压缩比率**

**参数罗列**

```
-d	指定解压文件夹
-n	重名文件不进行解压
-l	查看待解压文件信息，不进行解压
-v	查看文件列表、压缩方法和压缩比率，不进行解压
```

> 【参考】https://www.hangge.com/blog/cache/detail_1666.html

------

### 读取

像 `.zip` 的体积大小、CRC-32校验码之类的信息，我们都是通过手动打开 `.zip` 文件去查看的，那么能否通过脚本来获取这些信息呢？

在这里我们使用Python 3的**zipfile库**

> Python 3的**zipfile库**似乎是自带的

首先通过 `zipfile.ZipFile()` 创建**ZipFile**对象，该函数的API为：

```
class zipfile.ZipFile(path[, mode = r[, compression[, allowZip64]]])
```

- 参数 `path` 是即将要打开的 `.zip` 文件的路径
- `mode` 是打开方式，`r` 表示只读、`w` 表示创建或覆盖写入、`a` 表示附加写入
- `compression` 只有在写 `.zip` 时才会使用，它指定压缩方法，由于我们只读取，因此不用管该参数
- 当要操作的 `.zip` 文件大小超过**2G**，就需要将 `allowZip64` 参数设置为 `True`

创建**ZipFile**对象后，只需要掌握几个函数就可以了：

```
printdir()`、`namelist()`、`infolist()`、`gerinfo()
```

- `ZipFile.printdir()`

  打印压缩包中的基本信息：文件名、最近修改时间、文件体积

- `ZipFile.namelist()`

  返回一个列表，包含压缩包中的所有文件名

- `ZipFile.infolist()`

  返回一个对象列表，列表中的元素都是一个**ZipInfo**对象，并且会罗列出每个文件的一些基本信息，如 `filename` 文件名、`file_size` 文件大小...

- `getinfo(filename)`

  我们用这个函数来获取单个文件的信息，它接收文件名作为参数，然后返回对应的**ZipInfo**对象

  **ZipInfo**对象可以直接通过**属性**的方式获得相应的文件信息，如 `ZipInfo.filename`、`ZipInfo.file_size`

  注意，`ZipFile.infolist()` 中罗列出来的信息是不齐全的，比如还可以获取**CRC校验码**：`ZipInfo.CRC`；常用的**ZipInfo**属性有：

  `filename`、`data_time`、`compress_type`、`comment`、`create_version`、`extract_version`、`flag_bits`、`CRC`、`compress_size`、`file_size`

------

### 爆破

#### fcrackzip

**fcrackzip**是一款专门用于破解 `.zip` 文件密码的工具，适用于Linux

##### 安装

我们在Linux上安装**fcrackzip**，只需执行命令：

```

 sudo apt install fcrackzip
```

##### 使用

可以在终端输入 `fcrackzip -h` 来查看用法：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/fcrackzip_use.png)

下面记录几个常用的参数：

- `-c`

  指定字符集，分别有 `a`、`A`、`1`、`!`、`:`

  `a` 表示小写字码[a-z]、`A` 表示大写字母[A-Z]、`1` 表示数字[0-9]、`!` 表示特殊字符 `!:$%&/()=?{[]}+*~#`、`:` 表示包含之后的字符

  如：`-c "a1:&!"` 表示小写字母、数字、以及 `&` 和 `!`

- `-u`

  **fcrackzip**默认会将爆破过程罗列，添加该参数后只显示爆破成功的密码

- `-l`

  指定密码的长度，可以通过 `-` 来表示范围，如 `-l 3` 表示3位密码、`-l 3-7` 表示3到7位密码

- `-b`

  使用暴力破解算法

- `-D` 和 `-p`

  `-D` 指定使用字典爆破，而 `-p` 则指定要使用的字典，如 `fcrackzip -D -p dict.txt`

------

#### ARCHPR

**ARCHPR(Advanced Archive Password Recovery)**是一个高度优化的**口令恢复工具**，适用于 `.zip` 和 `.rar` 文件

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/archpr_show.png)

点击打开按钮，选择待爆破压缩文件，然后选择爆破方式，点击开始!

它支持**4**种密码爆破方式——**暴力破解**、**掩码**、**字典**、**明文攻击**

------

##### 暴力破解(Brute-Force)

作为最普通的密码攻击手段，**ARCHPR**允许在范围选项卡中，选择暴力破解的字符集：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/brute_force_choose.png)

其中"所有特殊符号"为：

```
!@#$%^&*()_+-=<>,./?[]{}~:;`'|"\
```

此外，可以自定义字符集：通过勾选用户定义□，然后点击右边的"自定义字符集"选项

> 如你所见，在给定的字符集中，只囊括了**ASCII可打印字符**，如果 `.zip` 的密码是中文的话，那就必须选择"自定义字符集"，并且勾选转换为OEM编码；否则密码会"Not Found"

在长度选项卡中，设置密码长度：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/length.png)

在范围选项卡的右边，可以看到：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/start_end.png)

这里就得知道，**ARCHPR**在暴力破解密码的时候，是采用特定的顺序的：

- 大小字母 `A` - `Z`
- 空格
- 小写字母 `a` - `z`
- 数字 `0` - `9`
- 特殊字符 `!@#$` ...

也就是说，暴力破解密码的顺序大致为：

```
"AAA" -> "AAB" -> "AAC" -> ... -> "AAZ" -> "AA " -> "AAa" -> "AAb" -> ... -> "AAz" -> "AA0" -> "AA1" -> ... -> "AA9" -> "AA!" -> "AA@"
```

上图的**"开始于"**有2个作用：

1. 当你知道密码长度为5，并且第一个字符为 `k`，你可以在**"开始于"**设置 `kAAAA`，那么**ARCHPR**将跳过 `AAAAA` 到 `kAAAA` 的前一个的暴力破解，节省时间
2. 如果破解密码的时间太长，**ARCHPR**会每隔5分钟自动保存密码（在自动保存选项卡中可以选择），如果某次暴力破解被迫中断，那么可以通过查看最后一次保存的密码，并设置**"开始于"**，直接从上次爆破的地方继续开始爆破

**"结束于"**的作用类似

------

在这里，引出**ARCHPR**的第一个坑：

###### 坑1：版本不支持

先去下载 `hour.zip`：

> 链接：https://pan.baidu.com/s/1Az6IyTvElnzPSjBZ3UWGMA
> 提取码：dgkv 

`hour.zip` 是某道CTF题中的最后一步，flag就在压缩包里面

我们尝试用**ARCHPR**暴力破解出 `hour.zip` 的密码，结果却显示：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/error_2.png)

> 我尝试在网上查找出现这个报错的原因，然而只能大概的说，压缩 `.zip` 的压缩算法是有版本的，**ARCHPR**不支持较高压缩版本的 `.zip`
>
> 后来使用**fcrackzip**爆破出了 `hour.zip` 的密码 `z1P`，得到里面的 `flag.txt` 后，我又用Bandizip将它压缩回 `flag.zip`，密码还是 `z1P`；然后我对比了 `hour.zip` 和 `flag.zip` 的内容，发现：
>
> ![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/diff.png)
>
> 左边是 `hour.zip`，右边是复现的 `flag.zip`
>
> 红色方框中的是**解压所需的pkware最低版本**，虽然不知道 `14 03` 如何转化为版本号，但两者的数值的确不同
>
> 将 `hour.zip` 的 `14 03` 手动更改为 `14 00` 后，发现使用**ARCHPR**就不会出现上面的报错信息！
>
> 然而由于是强行更改的，**ARCHPR**甚至爆破不出这个长度只有3的弱密码！
>
> 也就是说，对于较高压缩版本的 `.zip`，**ARCHPR**根本毫无用处

------

##### 掩码(Mask)

**掩码**攻击属于**局部暴力破解**，如果你已经知道部分的密码，那么可以直接输入它，再用 `?` 来代替那些不知道的部分；随后**ARCHPR**就会爆破 `?` 处的字符

注意**掩码**攻击是固定长度的

> 比如我知道密码的长度为7，后4位是 `love`，那么就可以在**掩码**处填入 `???love`

确定**掩码**后，爆破方式与**暴力破解**没什么不同

如果密码本身就可能包含 `?`，那可以在高级选项卡中，更改默认的掩码符号 `?`

------

##### 字典(Dictionary)

**字典**相比较**暴力破解**更"智能"，因为**字典**中包含了大部分可能使用的密码，而不是毫无头绪的穷尽遍历

一份好的字典是必须的，字典囊括的密码数决定了**字典攻击**的效果

> 比如Github上就有许多爆破字典：https://github.com/rootphantomer/Blasting_dictionary

**ARCHPR**为**字典攻击**提供了3个选项：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/three_choice.png)

- 智能变化

  "智能变化"是针对字典元素的：假如当**字典攻击**到 `password` 时，**ARCHPR**会自动将 `password` 进行变化：

  `PASSword`、`passWORD`、全小写 `password`、全大写 `PASSWORD`、首字母大写 `Password`、首字母小写 `pASSWORD`、元音字母大写 `pAsswOrd`、元音字母小写 `PaSSWoRD`、间隔变化1 `PaSsWoRd`、间隔变化2 `pAsSwOrD`

  对于字典中的每个元素，都会执行上面10种变化，增大破解几率，但破解时间会增大10倍

- 尝试所有可能的大/小写组合

  以字典元素 `password` 为例，**ARCHPR**会依次尝试每个字母的大小写组合：

  ```
  
  ```

```
password -> passworD -> passwoRd -> passwoRD -> ... -> PASSWORD
```

- 很显然，这种情况下破解时间会大幅增加

- 转换为OEM编码

  当字典元素不只包含**拉丁字母(Latin Letters)**时，这个选项才会生效

  （具体涉及ANSI代码页，暂时略过）

在字典选项卡中还有一个开始行号□，它用于规定从字典的第几行开始进行**字典攻击**；如果**字典攻击**中途停止了，最后一次攻击到的行数会写入开始行号□中，以便继续

------

##### 明文攻击(Plaintext)

**明文攻击**也是CTF中的一类比较少见但有用的题型

###### 原理

`.zip` **传统加密算法**本质上是伪随机数流和明文进行异或，产生这个伪随机流需要用到3个32 bits的**key**；找到这3个**key**，就能解开加密的文件

压缩软件用这3个**key**加密压缩包中的所有文件，当我们得到已加密压缩包中的任意一个文件，如果我们用**同样的压缩方法**进行无密码的压缩，得到的无密码 `.zip` 和有密码的 `.zip` 进行比较，分析两个文件的不同点，就能得到3个**key**了

所谓**明文**是指我们通过某些方法得到的已经加密的 `.zip` 文件中的部分文件

用**相同的压缩方法**将该**明文**压缩成 `.zip`在拥有**2**个 `.zip` 文件后，由于新的 `.zip` 和原本的 `.zip` 用**同样的压缩方法**放置入了至少1个**相同的文件**，那么就能够根据新的 `.zip` 爆破出压缩密码

> **明文**需大于12 bytes

###### 实战

我们以Bugku的杂项题目**神秘的文件**为例，[点击下载](https://ctf.bugku.com/files/d017f513e8f414cae61bfa3498ea34a8/5ee325f5-44c6-4a0b-b496-a0b11ef6dca1.rar)

解压 `.rar` 压缩包，得到 `flag.zip` 和 `logo.png`；我们发现 `flag.zip` 是加密的，但是里面也有一个名为 `logo.png` 的文件

怀疑 `logo.png` 就是**泄露文件**，首先需要检查两个 `logo.png` 是不是完全相同

使用**WinRar**将 `logo.png` 压缩为 `logo.zip`（无密码），浏览 `logo.zip`，发现原先的 `logo.png` 经过ZIP压缩，会得到与 `flag.zip` 中的 `logo.png` 相同的**CRC校验码**—— `3e62bf64`

基于此，我们可以断定两个 `logo.png` 是相同的，那么这就考察**明文攻击**了

打开**ARCHPR**，在明文选项卡中分别导入加密的 `flag.zip` 和明文 `logo.zip`，开始!

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/mingwen_1.png)

成功得到压缩密码 `q1w2e3r4`

> 剩下的求flag步骤与本文无关，略过

------

在这里，引出**ARCHPR**另外两个坑：

###### 坑2：不同压缩算法

就上面的题目而言，在压缩 `logo.png` 时不使用**WinRar**，而是使用Linux下的 `zip` 命令，得到 `logo.zip`

然后对 `flag.zip` 和 `logo.zip` 发动明文攻击，但**ARCHPR**却给出报错：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/error_1.png)

经测试和浏览网上的write-up，就这题而言，压缩 `logo.png` 时使用Bandizip、2345好压、7z、快压、360压缩、Linux `zip` 命令，都会出现上图的报错信息；只有使用**WinRar**压缩后才能正常进行明文攻击

缘由是不同软件对 `.zip` 的**压缩算法**是不同的

> 然而我浏览了许多关于**明文攻击**的文章，关于**zip不同压缩算法**都是模棱两可、一笔带过
>
> > "最后请教大佬得知，压缩算法的锅，emmm，有点想哭，修改压缩算法"
> >
> > "可能是文件的压缩方式与加密文件不同，尝试更换压缩方式"
>
> 找了许久，没能得到一个明确的答案，不知道如何确定所谓的"正确的压缩算法"；并且就上题而言，使用**WinRar**压缩后的 `logo.png` 虽然**CRC校验码**相同，但是**体积大小**却是不同的——压缩后的体积根本不能作为**相同压缩算法**的判别依据
>
> 或许做**明文攻击**题时，遇到上图的问题需要逐个尝试不同的压缩软件...☹
>
> **补充**
>
> 后面在一篇[文章](https://examine2.top/2021/01/12/CTF中的zip（下）/[https://veritas501.space/2017/06/23/给你压缩包却不给你密码的人到底在想什么/](https://veritas501.space/2017/06/23/给你压缩包却不给你密码的人到底在想什么/))中找到一句话：
>
> **构造明文压缩包时要选用与加密压缩包相同的压缩软件**，如果他用WinRar压的，你用7z构造出的压缩包来做明文压缩包，软件是会报错的

###### 坑3：口令未找到

当你进行长时间的**明文攻击**后，最终却显示：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/not_found.png)

你以为**明文攻击**失败了？并没有

> 虽然 `.zip` 文件的加密原理很难理解，但有一些需要记住
>
> `.zip`  的加密过程中会生成3个**key(加密密钥)**，如果我们能够得到**key**，那么哪怕没有**密码**，也能够通过**明文**和**key**复现出加密压缩包中的文件

**ARCHPR**的**明文攻击**分**3**个阶段：

1. 

   阶段一：**减少密钥的数量**

   这一步是为后面搜寻**密钥**做准备的

2. 

   阶段二：**搜索密钥**

3. 

   阶段三：**尝试找回口令**

"口令"就是加密压缩包的密码

在阶段二，就会根据**明文**去找到3个**key**；而在阶段三，会尝试找回压缩包的密码

> 我个人觉得阶段三是多余了，原因是它花费了大量的时间(相对阶段2)，还不保证成功
>
> 既然我们知道，拥有**key**就能够复现压缩包中的文件，那么就完全可以在阶段二结束后，停止**明文攻击**，跳过耗时久的阶段三

在阶段三直接点击停止，会出现：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/not_found.png)

点击保存可以将上述的"总计口令"、"总计时间"、"平均速度"之类的统计信息保存下来，但用处不大

点击确定，**ARCHPR**会让你保存一个 `$File_Name$_decrypted.zip` 的文件，这个文件就是**ARCHPR**根据**key**复现的压缩包文件

我们可以直接通过打开 `$File_Name$_decrypted.zip` 查看原先被加密的文件

> 因此，在进行**明文攻击**时，不妨在结束阶段二后就停止攻击，打开保存下来的 `.zip` 文件，就能得到flag

练习：

> 链接：https://pan.baidu.com/s/1f07VM3bGuPs5frMhbuFoOw
> 提取码：b74h 

------

## 练习

**练习一**：格式为flag{xxx}

> 链接：https://pan.baidu.com/s/1Ur7CTAqdw2s00fRCWK9RJg
> 提取码：ulv8

**练习二**：格式为flag{xxx}

> 链接：https://pan.baidu.com/s/1CXPXydOejb8PaUXbQwgaDg
> 提取码：z12i

 

**解答一**：

打开 `zippy.zip`，发现有54个以 `chunk` 命名的 `.zip`，并且附带一句提示：`the text files within each zip consist of only "printable" ASCII characters`

被告知"每个 `.zip` 只包含**可打印ASCII字符**"

大致浏览一下各个 `chunk.zip`，里面都有一个 `data.txt`，并且每个 `data.txt` 只有**4**字节大小，在**CRC爆破**的攻击范围内

由于 `.zip` 文件较多，写脚本将所有 `.zip` 的**CRC校验码**读取出来：

```
import zipfile
crc32 = []
for i in range(54):
    path = 'zippy\chunk' + str(i) + '.zip'
    file = zipfile.ZipFile(path, 'r')
    info = file.getinfo('data.txt')
    crc32.append(info.CRC)
```

得到结果如图：

![img](https://examine-typora-picbed.oss-cn-beijing.aliyuncs.com/img/many_crc32.png)

由于每个 `data.txt` 的内容为**4**字节的**可打印ASCII字符**，因此继续写脚本爆破内容：

```
import zlib

ASCII = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'

def solve(crc32):
    for a in ASCII:
        for b in ASCII:
            for c in ASCII:
                for d in ASCII:
                    if hex(zlib.crc32(bytes(a+b+c+d, encoding='utf-8')))[2:] == crc32:
                        return a+b+c+d
                    
flag = ""
crc32 = ['de65c01b', 'b156ff98', ...]
for i in crc32:
    flag += solve(i)
print(flag)
```

> **可打印ASCII字符**有95个，所以每个**CRC校验码**的爆破需要大约954=81450625

> 次，要爆破54个**CRC校验码**，所以耗费的时间是比较多的，我自己花费了1h03min才爆破出来
>
> 可以适当调整一下 `ASCII` 的顺序，让更有可能出现的大小写英文字母在前面，能相对提高速度

最终得到结果

```
UEsDBBQDAQAAAJFy1kgWujyNLwAAACMAAAAIAAAAZmxhZy50eHT/xhoeSnjMRLuArw2FXUAIWn8UQblChs4AF1dAnT4nB5hs2SkR4fTfZZRB56Bp/FBLAQI/AxQDAQAAAJFy1kgWujyNLwAAACMAAAAIAAAAAAAAAAAAIIC0gQAAAABmbGFnLnR4dFBLBQYAAAAAAQABADYAAABVAAAAAAA=
```

base64解码，得到乱码，然而乱码以 `PK` 开头

明显是 `.zip` 文件内容，将base64解码内容另存为 `temp.zip`，结果是一个未损坏的 `.zip` 压缩包，而且压缩包中包含一个加密的 `flag.txt`

检查过不是**伪加密**，于是采用**暴力破解**，使用**fcrackzip**爆破出压缩密码 `z1P`，得到flag（版本太高，**ARCHPR**不支持）

> **Mark**
>
> BUUCTF的题目 [zip](https://files.buuoj.cn/files/38ccc6412d706ee177c5f2880f4aeba2/b2ca8799-13d7-45df-a707-94373bf2800c.zip?token=eyJ1c2VyX2lkIjo3MDA0LCJ0ZWFtX2lkIjpudWxsLCJmaWxlX2lkIjoyMDJ9.XxWuhg.7qypYBumzGi0dLtkIXHAeprwVzU) 也是考察CRC爆破，还考察一点 `.rar` 的文件格式

------

**解答二**：

解压 `temp.rar`，得到 `Desktop.zip` 和 `readme.txt`

发现 `Desktop.zip` 中也有一个 `readme.txt`，尝试将外面的那个 `readme.txt` 压缩成 `readme.zip`，发现两个 `readme.txt` 的**CRC-32校验码**相同，确定为**明文攻击**

测试了几下，发现Bandizip、WinRar压缩的 `readme.zip` 都被**ARCHPR**判定为**不同压缩算法**；最后发现Linux下的 `zip` 命令压缩的 `readme.zip` 能够进行**明文攻击**

> ...

那就用Linux压缩的 `readme.zip` 和 `Desktop.zip` 进行**明文攻击**

**明文攻击**阶段二就停止，在导出的 `Desktop_decrypted.zip` 中打开 `key.txt`，得到flag

> 我在阶段三浪费了2个多小时，结果告诉我"口令未找到"，真的鸡肋...
>
> ```
> 
> ```

```
总计口令: n/a
总计时间: 2h 36m 45s 616ms 
平均速度(口令/秒): n/a
这个文件的口令 : 未找到
加密密钥: [ df96dc88 b432ddfd df4b9e93 ]
```

> 

------

## Zip炸弹