## PNG图片格式

- **图种**：`jpg` 图片有文件尾标识 `FF D9`，而图片查看器只读取 `FF D9` 之前的数据显示为图片，因此 `FF D9` 之后的空间能够存放隐藏数据；`png` 的末尾 `00 00 00 00 49 45 4e 44 ae 42 60 82` 充当文件尾标识，情况类似
- **LSB隐写**：将数据藏匿在最低有效位，因此只适合 **无损压缩 `png`** 或 **无压缩 `bmp`** 图片格式

很显然，想要更深入的了解**图片隐写**，就必须对常见的图片格式有所了解

本篇介绍 **`png`** 图片格式

------

### 数据块(Chunk)

**文件头标识** ——（8 bytes）`89 50 4E 47 0D 0A 1A 0A`，ASCII码为 `.PNG....`

`png` 文件由**数据块（Chunk）**组成，每个**Chunk**都必须包含：

- 长度**（Length）**：4 bytes

- 数据块类型**（Chunk Type）**：4 bytes

- 数据块数据**（Chunk Data）**：**Length** bytes

- **CRC**校验码：4 bytes，由Chunk Type和Chunk Data计算而来

  > **CRC**是**循环冗余校验(Cyclic Redundancy Check)**的缩写，它能根据数据产生简短的**固定位数校验码**，能够用来校验数据传输前后的完整性
  >
  > 数据中哪怕是 `1 bit` 的改动，也会导致完全不一样的CRC校验码
  >
  > 数据传输过程中难免会出现差错，那么可以在传输前对数据进行CRC计算、传输后再计算，如果两个CRC结果不一致，就应该要求重新传输数据

我们来重点分析 `png` 的第一个Chunk：**IHDR Chunk**

------

### **IHDR Chunk**

#### 介绍

IHDR Chunk又称为**图片文件头数据块（image header chunk）**，是 `png` 图片的第一个chunk，一张 `png` 图片**有且仅有**一个IHDR Chunk

IHDR Chunk包含了 `png` 图片的基本信息：

- 宽、高
- 颜色深度
- 颜色类型
- 压缩方法
- ......

我们假设用十六进制查看器打开一张 `png` 图片，数据如下：

```
0000000: 8950 4e47 0d0a 1a0a 0000 000d 4948 4452 .PNG........IHDR
0000010: 0000 0010 0000 0010 0800 0000 003a 98a0 .............:..
0000020: bd00 0000 0774 494d 4507 da0a 0a07 150b .....tIME....... 
......
```

前**8**个固定字节属于文件头标识：`89 50 4e 47 0d 0a 1a 0a`

紧接着的**4**个字节是 `00 00 00 0d`，它属于chunk的Length域，也就是说这个chunk的Chunk Data域的长度应该为 `0x0d = 13` 个字节

> 从Length开始已经进入了 `png` 文件的第一个chunk——IHDR Chunk了

再往后取**4**个字节，`49 48 44 52`，对应chunk的Chunk Type，其对应的ASCII字符为 `IHDR`

接下来应该是Chunk Data域了，它的长度由Length确定，所以有 **`0x0d = 13`** 个字节

就如之前所说的，IHDR Chunk包含 `png` 文件的许多信息，这些信息就包含在IHDR Chunk的Chunk Data里面。这 `13` 个字节分别代表：

- 宽度**（Width）**：**4** bytes

- 高度**（Height）**：**4** bytes

- 颜色深度**（Bit Depth）**：**1** byte

  > **注意**
  >
  > 颜色深度（Bit Depth）应该与另一个概念**位深度**一起理解
  >
  > - **位深度**表示RGBA通道中，每个通道的位数
  > - **颜色深度**表示RGBA四个通道总共的位数，有 `4 * 位深度 = 颜色深度`
  >
  > 不要被英文翻译弄晕了
  >
  > 不同的位图软件会以不同的角度去解释图像的**位深度**，有可能同一张图片，在十六进制查看器中显示颜色深度为8 bytes；而查看图片属性却显示位深度为32 bytes

- 颜色类型**（Color Type）**：**1** byte

- 压缩方法**（Compression Method）**：**1** byte

- 过滤方法**（Filter Method）**：**1** byte

- 扫描方法**（Interlace Method）**：**1** byte

跳过Chunk Data的13个字节，IHDR Chunk的最后**4**个字节 `3a 98 a0 bd` 是由Chunk Type和Chunk Data计算得到的CRC校验码

------

#### CRC计算方法（可跳过）

任意一个**二进制数**都可以与一个**系数仅为 `0` 或 `1` 的多项式**一一对应

> 例如，`1010111` 对应 *x*6+*x*4+*x*2+*x*+1

，而*x*5+*x*3+*x*2+*x*+1

> 对应 `101111`

在计算CRC码之前，都要选定一个**参数模型**，**参数模型**就是上面的**多项式**

我们可以将*x*6+*x*4+*x*2+*x*+1

称之为**CRC-6**；将*x*5+*x*3+*x*2+*x*+1

称之为**CRC-5**；但是光**CRC-6**就有许多种——只要最高项指数为6、系数为1的，都是

科学家对CRC的研究延伸出**生成多项式**，专门用于研究如何选定一个多项式为**CRC-6**，使得CRC-6的性能提升（CRC类似哈希算法，理应避免哈希碰撞）

就目前而言，广泛使用的**CRC-5**是*x*5+*x*2+1

、而不是*x*5+*x*3+*x*2+*x*+1；广泛使用的**CRC-6**是*x*6+*x*+1，而不是*x*6+*x*4+*x*2+*x*+1



> 选定CRC的多项式是有数学依据的，但这篇文章不讨论这个

> 我们用自定义的**CRC-3**来快速演示一下CRC的计算过程：
>
> 假设选定多项式为：*x*3+*x*+1



假设原始数据为 `1010`，求 `1010` 经过我们自定义的CRC-3编码后，得到的CRC码：

1. 多项式*x*3+*x*+1

对应的二进制数是 `1011`

我们把最高项指数称为**R**，在这里，*R*=3

> 1. 
>
>    （**R**是CRC的命名依据，并且也是生成CRC码的**二进制**位数）
>
>    把原始数据 `1010` 左移**R**位，得到移位数据 `1010000`
>
> 2. 移位数据与**多项式对应二进制数**进行**模2除法**
>
>    > **模2除法**
>    >
>    > 普通除法过程中的**加减法**全部改成**异或运算**
>
>    得到 `1011011`
>
> 3. 取**异或运算**结果的**后R位**，也就是 `011`，这个就是生成的CRC码
>
> 具体可以参见文章https://blog.csdn.net/Kj1501120706/article/details/73330526

------

回到我们 `png` 的IHDR Chunk的CRC码 `3a 98 a0 bd`，它是由Chunk Type和Chunk Data计算得到的CRC校验码

Chunk Type和Chunk Data的数据为 `4948 4452 0000 0010 0000 0010 0800 0000 00`，这就是CRC计算中的原始数据

对于 `png`，默认使用**CRC-32**：

*x*32+*x*26+*x*23+*x*22+*x*16+*x*12+*x*11+*x*10+*x*8+*x*7+*x*5+*x*4+*x*2+*x*+1



#### Python 2计算CRC-32

使用Python 2是借助**zlib库**可以快速计算CRC-32的，以上面的原始数据为例：

```
import zlib
s = "\x49\x48\x44\x52\x00\x00\x00\x10\x00\x00\x00\x10\x08\x00\x00\x00\x00"
c = zlib.crc32(s) & 0xffffffff
print hex(c)
	# 0x3a98a0bd
```

可以看到，得到的结果的确是我们想要的 `3a 98 a0 bd`

> 原始数据必须转换成 `\x` 形式，它表示单字节编码；而 `0x` 是不行的
>
> `\x` 表示一个不能直接显示的单字节字符的编码；`0x` 便是一个标准十六进制的字符串

> **补充**
>
> Python 2中 `crc32()` 的值域为 [−231,231−1]

之间的整数，而更为广泛使用的**CRC码**（如Python 3、C语言）的取值范围为 [0,232−1]

> 为了修正Python 2中的CRC码，必须与 `0xffffffff` 进行**与**运算

#### Python 3计算CRC-32

Python 3同样有**zlib库**：

```
import zlib
s = b"\x49\x48\x44\x52\x00\x00\x00\x10\x00\x00\x00\x10\x08\x00\x00\x00\x00"
c = zlib.crc32(s)
print(hex(c))
	# 0x3a98a0bd
```

注意Python 3的 `zlib.crc32()` 的参数必须是 **`bytes`**类型，而Python 3强制区分**bytes**和**string**的使用，为此在字符串 `s` 前面多加一个 `b`

------

在CTF中，有一类题目就是人为减小了IHDR Chunk中Chunk Data的**Width**或**Height**的值，这就导致图片查看器在解析 `png` 图片数据的时候，按照修改后的**Width**或**Height**会缺少图片部分区域的显示，这部分区域往往就隐藏着flag

为此，我们需要在十六进制编辑器中将**Width**或**Height**修改回来，而正确的**Width**或**Height**就隐藏在**CRC码**中

> **注意**
>
> 胡乱修改IHDR Chunk中的**Width**或**Height**，在某些操作系统中会导致**CRC校验报错**，从而无法打开 `png` 图片
>
> 比如**Linux**下的图片查看器就不会忽略**错误的CRC校验码**，因此用Linux打开修改过**Width**或**Height**的 `png` 图片，会出现打不开的情况
>
> 下图为Kali中打开CRC校验报错的 `png` 图片：
>
> 
>
> 但是**Windows**是会忽略的，它会按照修改后的**Width**和**Heigth**来显示图片

BugKu的杂项题目[隐写](https://ctf.bugku.com/challenges)就是这一类题目   [点击下载图片](https://ctf.bugku.com/files/f8da9b5979e89e91d083c7accdea4427/2.rar)

> 下载得到 `2.png`，用十六进制编辑器打开，查看IHDR Chunk的数据：
>
> 
>
> `png` 文件头标识 `89 50 4E 47 0D 0A 1A 0A` 没有问题
>
> 图片宽度为 `00 00 01 f4`，即 `500` 像素；高度为 `00 00 01 a4`，即 `420` 像素；CRC码为 `cb d6 df 8a`
>
> 我们用Chunk Type和Chunk Data的数值来计算一下IHDR Chunk的CRC码：(Python 3)
>
> 
>
> 发现计算出来的CRC校验码与文件数据中的CRC校验码不同，猜测为更改了图片的宽度或高度
>
> 由于图片的宽度和高度的取值都在一个比较小的范围内，因此可以根据CRC码，爆破出正确的宽度或高度

#### `png` CRC爆破

CRC码可以利用Python的 `zlib.crc32()` 函数求得，反过来，也可以用该函数进行爆破

由于图片宽度为 `500`像素、高度为 `420` 像素，所以假设高度被改小了，那么就基于**CRC码**和**IHDR Chunk中其它数据**，爆破出正确的高度值

```
import struct
import zlib

# 十六进制字符串转bytes：两两分割字符串，对子串使用struct.pack()转换成bytes类型，拼接在一起
def hexStr2bytes(s):
    b = b""
    for i in range(0,len(s),2):
        temp = s[i:i+2]
        b += struct.pack("B", int(temp, 16))
    return b

# str1和str2分别是待爆破的Height前面的数据、后面的数据，呈现为16进制字符串
str1 = "49484452000001f4"
str2 = "0806000000"
bytes1 = hexStr2bytes(str1)
bytes2 = hexStr2bytes(str2)

crc32 = "0xcbd6df8a"

# 遍历高度h从0到1000
for h in range(1000):
    str = hex(h)[2:].rjust(8, '0') # 将十进制int转化为8位十六进制数，rjust()用于补齐
    bytes_temp = hexStr2bytes(str)

    if hex(zlib.crc32(bytes1+bytes_temp+bytes2)) == crc32:
        print(h)
        
# 脚本是自己写的，跟网上常见的binascii有所不同
```

上面的**Python 3**代码主要应用了**`zlib.crc32()`**和**`struct.pack()`**函数来爆破**Height**

执行代码的运行结果为： `500`，所以对应CRC码的正确高度值应该为 `500` 像素

`500` 对应十六进制 `1f4`，那么在十六进制编辑器中将 `png` 的高度修改回 `1f4`：

![img](https://examine2.top/images/image_format/2_png_change.png)

再次打开 `2.png`，发现将高度修正后，图片查看器将显示出被遮掩的部分，flag出现

> 补充一篇网上的 `png` CRC爆破代码，修改过适用于本题：
>
> ```
> 
> ```

```
import binascii
import struct

misc = open("2.png","rb").read()

for h in range(1024):
 data = misc[12:20] + struct.pack('>i',h)+ misc[24:29]
 crc32 = binascii.crc32(data) & 0xffffffff
 if crc32 == 0xcbd6df8a:
     print h
```

**对比**

我原先的代码之所以写得这么长，主要有两个原因：

- 没熟悉 `struct.pack()` 的使用方法

  **struct库**只要用于处理二进制数据，`pack()` 的API为 `pack(fmt, v1, v2)`

  第一个参数 **`fmt`** 表示 `pack()` 对数据处理的格式，`B` 是**unsigned char**的缩写，占**1** bytes；而 `i` 是**integer**的缩写，占**4** bytes

  我傻傻地将高度 `h` 转换成8位十六进制数，两两拆分后，把每个单独的2为十六进制数再转换成**int**，最后使用 `B` 参数指定 `pack()` 生成**1**个bytes，再把**4**个的**1** bytes拼接起来

  而使用 `i` 参数就不用这么麻烦，该参数直接指定生成**4** bytes

  > 留意到 `pack()` 接收的都是十进制数

  此外，`pack()` 提供 `>` `<` 来改变生成字节的顺序，默认为 `<`，因此需要指定为 `>`

  > 详细部分可以参考文章：https://www.cnblogs.com/gala/archive/2011/09/22/2184801.html

- Python 2不像Python 3一样，对**bytes**和**string**有严格区分

  留意到上面代码中，`struct.pack()` 返回的**bytes**与 `misc` 的切片**string**相加，这在Python 3中只会给你一个报错信息：`TypeError: can only concatenate str (not "bytes") to str`，而这在Python 2中是允许的，节省了不少功夫

- **`0xffffffff`**

  首先得指出，**binascii库**和**zlib库**的 `crc32()` 函数都是**一样**的  ←存疑

  在Python 2中，`crc32()` 计算处理的**CRC**值域为[−231,231]

之间的**有符号整数**，为了与一般的CRC结果作对比，需要将其转换为**无符号整数**，所以与 `0xffffffff` 进行**&**运算来进行转换

Python 3的计算结果是[0,232−1]

> - 间的**无符号整数**，无需额外的转换
>
>   因此这个 `0xffffffff` 的出现是因为Python的版本问题

#### 直接修改

Thanks to Windows对错误的CRC校验码的忽略，我们可以直接将高度修改为**较大的值**，可以很快解决掉这种类型的CTF题目

相比于**CRC爆破**，这种方法是最快的

------

### All Chunks

`png` 文件主要由**4**大块（Chunk）组成：

- 文件头数据块**IHDR**（header chunk）
- 调色板数据块**PLTE**（palette chunk）
- 图像数据块**IDAT**（data chunk）
- 图像结束数据块**IEND**（trailer chunk）

上面**4**个数据块是 `png` 最主要的数据块

> 还有其它算不上重要的数据块，比如：
>
> - 在**PLTE**和**IDAT**之前的：cHRM 基色和白色点数据块、gAMA 图像Y数据块、sBIT 样本有效位数据块
> - 介于**PLTE**和**IDAT**之间的：bKGD 背景颜色数据块、hIST 图像直方图数据块、tRNS 图像透明数据块
> - 无位置限制的：tIME 图像最后修改时间数据块、tEXt 文本信息数据块 ...

每个数据块都是由**Length**、**Chunk Type**、**Chunk Data**、**CRC码**构成

------

### IEND Chunk

每个正常的 `png` 图片文件的**IEND Chunk**都是**固定**的：

```
00 00 00 00 49 45 4e 44 ae 42 60 82
```

按照chunk的结构进行分析：

- **Length**占**4** bytes，因此 `00 00 00 00` 就是IEND Chunk的长度

  又因为**Chunk Data**的大小就取决于**Length**，因此可以看到：**IEND Chunk**的**Chunk Data**是空的，没有分配哪怕**1** bytes的空间给它

- **Chunk Type**也占**4** bytes，而 `49 45 4e 44` 对应的ASCII字符恰好就是 `IEND`

- 最后的**4** bytes属于**CRC**，我们可以计算一下：

  ```
  
  ```

```
import zlib
s = b"\x49\x45\x4e\x44"
print(hex(zlib.crc32(s)))
	# 0xae426082
```

- 

由于**IEND Chunk**的长度为0，导致**Chunk Data**为空；又由于**Chunk Type**是固定的 `IEND`，导致计算出来的**CRC码**也是固定的

综上，每个 `png` 文件的**IEND Chunk**都是 `00 00 00 00 49 45 4e 44 ae 42 60 82`，占**12**字节，它作为 `png` 文件的**文件尾标识**

> 我们知道，**图种**的原理就是利用**文件尾标识**之后的数据不被图片查看器解析成图片，因此 `png` 与 `jpg` 一样可作为**图种**的载体
>
> > **习题**：格式为flag{xxx}
> >
> > 链接：https://pan.baidu.com/s/1Ze6bct3jA8qJt-zh0sNBKg
> > 提取码：8vsh 

------

### IDAT Chunk

#### 介绍

作为 `png` 图片中存储实际数据的数据块**Data Chunk**，它在 `png` 中的存在并不像**IHDR Chunk**那样是唯一的。相反，它可以有很多个，但所有的**IDAT Chunk**必须**连续存在**

总结一下，它有如下特点：

- 一个 `png` 文件可能有多个**连续**的**IDAT Chunk**，而**IHDR**、**PLTE**、**IEND**都是唯一存在的
- **IDAT Chunk**的数据都是经过**LZ77派生算法**压缩过的，数据不可读；可以用**zlib**解压缩
- **IDAT Chunk**只有上一个块充满，才会开启一个新的块

由于**IDAT Chunk**是多个存在的，我们就可以将隐写信息以**IDAT Chunk**的形式加入图片中，将信息隐藏起来

检索隐藏在**IDAT Chunk**中的信息比较麻烦，因此我们借用工具：**pngcheck**

------

#### **pngcheck**简介

**pngcheck**能够检查文件的结构以验证完整性（通过检查内部的32位CRC校验码、并解压缩图像数据），以人类可读的方式转储图像中的块级信息

据[pngcheck官网](http://www.libpng.org/pub/png/apps/pngcheck.html)显示，**pngcheck**支持所有的 `png` 块、`jng` 块、`mng` 块

------

#### **pngcheck**安装

**pngcheck**貌似在kali上是自带的，在Linux系统上安装只需执行命令：

```

 sudo apt install pngcheck
```

安装成功后输入 `$ pngcheck`，可以看到使用方法：

![img](https://examine2.top/images/image_format/png_check.png)

**pngcheck**在Windows上也可以安装，方法自寻

------

#### **pngcheck**使用

我们以**sctf**的一道题来演示这类题目的解题过程

> 链接：https://pan.baidu.com/s/1D6ORthAZH5TVsoeyBNZyfA
> 提取码：rrb8 

建议自己按常规套路对图片进行一番信息检索

 

**解答**

使用**pngcheck**对 `sctf.png` 进行检索，注意添加 `-v` 参数，使其罗列详细数据：

![img](https://examine2.top/images/image_format/pngcheck_sctf.png)

**pngcheck**首先检查了**IHDR Chunk**，并将里面的文件信息显示了出来：宽1000像素、高562像素、位深度32，无扫描方法(interlace method)

随后**pngcheck**依次检查了sRGB Chunk、gAMA Chunk、pHYs Chunk，并对每个检索到的chunk都显示**名称**、**偏移量offset**、长度**length**

> 这里有两点关于**pngcheck**显示的数据的要点
>
> 1. **pngcheck**检索每个块的**length**只是这个块的**Chunk Data**占用的字节数
>
>    我们可以根据上图计算一下：
>
>    > 比如**IHDR**和**sRGB**，两者的偏移量相减：`0x00025 - 0x0000c = 0x00019 = 25`，也就是说**IHDR**总共占用25个字节，而**pngcheck**显示 `length 13`，很明显它不将**IHDR**中，**Length**的**4**字节、**Chunk Type**的**4**字节、**CRC**的**4**字节——总共**12**字节统计在内，满足 `13 + 12 = 25`
>
> 2. **pngcheck**的**offset**指到一个chunk的**Chunk Type**，而不是**Length**
>
>    > 我们直接看**IHDR Chunk**，抛却 `png` 最开始的**8**字节的文件头标识，**IHDR Chunk**的开始应该是 `0x00008`（从 `0x00000` 到 `0x00007` 是文件头标识），而上图却显示为 `0x0000c`，两者相差的**4**个字节就是**IHDR Chunk**的**Length**了

然后到了连续的多个**IDAT Chunk**以及最后的**IEND Chunk**：

![img](https://examine2.top/images/image_format/pngcheck_sctf2.png)

最后的**IEND Chunk**的**Chunk Data**是空的，所以显示的 `length` 为0

但是在最后一个**IDAT Chunk**处出现了异常——我们知道，**IDAT Chunk**只有上一个块充满，才会开启一个新的块。按照前面的**IDAT Chunk**可知，正常的**IDAT Chunk** `length` 为**65524**时才满

我们发现倒数第二个**IDAT Chunk**的 `length` 只有45027，在它未满的情况下后面出现了 `length` 为138的新**IDAT Chunk**，我们可以断定，这个 `length` 为138的**IDAT Chunk**是人为添加进去的

> **关于IDAT Chunk的"满"**
>
> `png` 图片中，**满**不一定是**65524** bytes，这取决于图片本身
>
> 例如，我们打开一张正常的非隐写 `png` 图片，可以看到：
>
> 
>
> 这里的**满**是**32768** bytes，最后一个**IDAT Chunk** `length` 为22583，未满
>
> 如何判断**IDAT Chunk**是否已**满**，根据大部分**IDAT Chunk**的 `length` 就可以得知

既然发现了异常数据，那么可以用十六进制编辑器打开 `png`，定位到偏移量 `0x15aff7`：

![img](https://examine2.top/images/image_format/png_hex.png)

图中紫蓝色的十六进制数据部分就是定位到的异常chunk的数据

区域的开始位置为**pngcheck**检索到的偏移量 `0x0015aff7` + `4` = `0x0015affb`（要跳过**Chunk Type**），结束位置为**IEND Chunk**的偏移量 `0x0015b08d` - `8` = `0x0015b085（跳过**RCR**以及**IEND Chunk**的**Length**）

之前有提到，**IDAT Chunk**中的数据都是经过**LZ77派生算法**压缩过的，不可直接读，但可以使用**zlib**解压缩。解压缩的Python 2代码为：

```
import zlib
IDAT = "789c5d...897667".decode('hex')
result = zlib.decompress(IDAT)
```

上面代码首先用 `decode()` 将十六进制数解码为bytes（注意Python 2不区分**bytes**和**string**），然后直接调用 `zlib.decompress()` 解压缩

得到的 `result` 为：

![img](https://examine2.top/images/image_format/zlib_result.png)

事实上到这里已经可以结束了，这道题结合的综合知识比较多，剩余部分超出了这篇文章的讲解范畴

以上就是**pngcheck**的介绍

------

## 总结

```
png` 文件头标识(8 bytes) `89 50 4E 47 0D 0A 1A 0A`，ASCII码为 `.PNG....
```

依次为**4**个关键数据块：**IHDR**、**PLTE**、**IDAT**、**IEND**，每个数据块包含**Length(4 bytes)**、**Chunk Type(4 bytes)**、**Chunk Data(Length bytes)**、**CRC(4 bytes)**

- **IHDR Chunk**，Length = 13 bytes，Chunk Data中**Width(4 bytes)**、**Height(4 bytes)**

  通过计算CRC码查看有没有修改宽高，计算代码：(Python 2，**cmd**快速使用版)

  ```
  
  ```

```
str = "直接复制过来的十六进制字符串，形如 '49 28 0d a7  ' ，可以保留两端的空格"

import zlib
str = str.strip().split(" ")
temp = reduce(lambda x,y: x+y, map(lambda x: chr(int(x, 16)), str))
print hex(zlib.crc32(temp) & 0xffffffff)
```

CRC码不匹配，宽高被修改，Windows下直接修正为较大的值；Linux下爆破出正确的宽度或高度

**IDAT Chunk**，执行命令 `$ pngcheck -v temp.png`，查看有无异常的IDAT Chunk

IDAT Chunk中异常数据的解压缩代码：

```
import zlib
IDAT = "789c5d...897667".decode('hex')
result = zlib.decompress(IDAT)
```



**IEND Chunk**

```
00 00 00 00 49 45 4e 44 ae 42 60 82
```

- 文件尾不是上面的数值，可能存在图种

------

## 随笔

理清了 `png` 的文件结构，顺便还拾起了堆在角落的Python知识

**总结**那里，计算CRC的代码中的那句：

```
temp = reduce(lambda x,y: x+y, map(lambda x: chr(int(x, 16)), str))
```

事实上是

```
temp = ""
for i in str:
	temp += chr(int(i, 16))
```

缩写成一行而已

之所以把三行简单的语句弄成这么复杂的一句，是因为这是**cmd快速使用版**，只要从十六进制编辑器中复制异常数据出来，作为字符串赋值给 `str`，然后把 `str` 下面的4行代码复制到cmd中，就可以出结果了

之前写成三行代码的时候，发现 `for` 语句下面的那个**缩进**复制过去的时候会被cmd自动删去，得手动添加上一个缩进，这不符合所谓的"快速使用版"，因此想办法将这个缩进干掉，借用lambda、map、reduce写成了一行代码

我还有一个 `.py` 文件是直接调用出结果的，但在cmd中切换目录和找到这篇文章复制代码，这两者之间孰快孰慢，不知道...

新工具：

- pngcheck

新函数：

- `zlib.crc32()` —— 计算CRC码
- `struct.pack()` —— 字符串转换成Unicode码
- `zlib.decompress()` —— 解压缩

------

end