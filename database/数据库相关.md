#### InnoDB存储引挚通过.ibd恢复数据
mysql ibdata1误删除后，整个数据库无法访问 利用数据库目录下面的ibd文件进行恢复操作
1. 创建一个新的数据库，重新安装wordpress 这样表及表结构全部安装上
2. 丢弃表空间，如下操作，操作完之后，数据目录的ibd文件将被清除

```sql
ALTER TABLE test.wp_comments DISCARD TABLESPACE;
```

3. 复之前的备份目录下的.ibd文件至新的数据目录下，导入表空间

```sql
ALTER TABLE test.wp_comments IMPORT TABLESPACE;
```

打开网站数据回来了，但是日志仍然报错如下：
InnoDB: Error: Table "mysql"."innodb_table_stats" not found.
删除mysql数据库目录下的innodb_打头的文件：
rm -f innodb_*
进入mysql数据下执行：

```sql
CREATE TABLE IF NOT EXISTS innodb_index_stats ( database_name VARCHAR(64) NOT NULL, table_name VARCHAR(64) NOT NULL, index_name VARCHAR(64) NOT NULL, last_update TIMESTAMP NOT NULL NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, /* there are at least: stat_name='size' stat_name='n_leaf_pages' stat_name='n_diff_pfx%' */ stat_name VARCHAR(64) NOT NULL, stat_value BIGINT UNSIGNED NOT NULL, sample_size BIGINT UNSIGNED, stat_description VARCHAR(1024) NOT NULL, PRIMARY KEY (database_name, table_name, index_name, stat_name) ) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0;
CREATE TABLE IF NOT EXISTS innodb_table_stats ( database_name VARCHAR(64) NOT NULL, table_name VARCHAR(64) NOT NULL, last_update TIMESTAMP NOT NULL NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, n_rows BIGINT UNSIGNED NOT NULL, clustered_index_size BIGINT UNSIGNED NOT NULL, sum_of_other_index_sizes BIGINT UNSIGNED NOT NULL, PRIMARY KEY (database_name, table_name) ) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0
```
#### mysql 导入导出csv

```
-- 导出：
select RESOURCE_ID_INT,RESOURCE_ID_CHAR,RESOURCE_TITLE from t_resource_info_wb
into outfile '/usr/local/test.csv'
fields terminated by ',' optionally enclosed by '"' escaped by '"'
lines terminated by '\r\n';
-- 导入：
load data infile '/usr/local/test.csv'
into table t_resource_info
fields terminated by ','  optionally enclosed by '"' escaped by '"'
lines terminated by '\r\n' (RESOURCE_ID_INT,RESOURCE_ID_CHAR,RESOURCE_TITLE);
```


#### SQL SERVER 查看执行慢的语句

```
select * from sys.sysprocesses
where spid>50
and lastwaittype<>'MISCELLANEOUS'
and status<>'sleeping'
and spid<>@@SPID
```

批量插入的情况下, 最理想的速度经测试, 应该是JDBC + executeBatch + JDBC url 加上参数 : useServerPrepStmts=false&rewriteBatchedStatements=true&useCompression=true

#### STRAIGHT_JOIN
要尽可能的保证排序字段在驱动表中，所以必须以A为驱动表，于是乎必须借助「STRAIGHT_JOIN」强制连接顺序。

#### Mysql计算两GPS坐标的距离

```SQL
drop function getDistance;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getDistance`(
     lon1 float(10,7)
    ,lat1 float(10,7)
    ,lon2 float(10,7)
    ,lat2 float(10,7)
) RETURNS double
begin
    declare d double;
    declare radius int;
    set radius = 6378140; #假设地球为正球形，直径为6378140米
    set d = (2*ATAN2(SQRT(SIN((lat1-lat2)*PI()/180/2)
        *SIN((lat1-lat2)*PI()/180/2)+
        COS(lat2*PI()/180)*COS(lat1*PI()/180)
        *SIN((lon1-lon2)*PI()/180/2)
        *SIN((lon1-lon2)*PI()/180/2)),
        SQRT(1-SIN((lat1-lat2)*PI()/180/2)
        *SIN((lat1-lat2)*PI()/180/2)
        +COS(lat2*PI()/180)*COS(lat1*PI()/180)
        *SIN((lon1-lon2)*PI()/180/2)
        *SIN((lon1-lon2)*PI()/180/2))))*radius;
    return d;
end
$$
DELIMITER ;
select getDistance(116.3899,39.91578,116.3904,39.91576); #调用函数
```
#### 在InnoDB下，使用表锁要注意以下两点
（1）使用LOCK TABLES虽然可以给InnoDB加表级锁，但必须说明的是，表锁不是由InnoDB存储引擎层管理的，而是由其上一层──MySQL Server负责的，仅当autocommit=0、innodb_table_locks=1（默认设置）时，InnoDB层才能知道MySQL加的表锁，MySQL Server也才能感知InnoDB加的行锁，这种情况下，InnoDB才能自动识别涉及表级锁的死锁；否则，InnoDB将无法自动检测并处理这种死锁。有关死锁，下一小节还会继续讨论。

（2）在用LOCK TABLES对InnoDB表加锁时要注意，要将AUTOCOMMIT设为0，否则MySQL不会给表加锁；事务结束前，不要用UNLOCK TABLES释放表锁，因为UNLOCK TABLES会隐含地提交事务；COMMIT或ROLLBACK并不能释放用LOCK TABLES加的表级锁，必须用UNLOCK TABLES释放表锁。正确的方式见如下语句：

例如，如果需要写表t1并从表t读，可以按如下做：
``` SQL
SET AUTOCOMMIT=0;

LOCK TABLES t1 WRITE, t2 READ, ...;

[do something with tables t1 and t2 here];

COMMIT;

UNLOCK TABLES;
```
### 计算MySQL数据表适合的字段
```
SELECT * FROM `user` PROCEDURE ANALYSE(1, 10); 
```

