### zincSearch使用

下载地址：https://github.com/zinclabs/zinc/

启动：
```bah
ZINC_FIRST_ADMIN_USER=admin ZINC_FIRST_ADMIN_PASSWORD=admin@123 ZINC_PLUGIN_GSE_ENABLE=true ./zinc
```
添加index,配置
```json
 {
		"analysis": {
			"analyzer": {
				"default": {
					"type": "gse_search"
				}
			}
		}
 }
```
增加mapping
```json
{
  "properties": {
    "@timestamp": {
      "type": "date",
      "index": true,
      "store": false,
      "sortable": true,
      "aggregatable": true,
      "highlightable": false
    },
    "_id": {
      "type": "keyword",
      "index": true,
      "store": false,
      "sortable": true,
      "aggregatable": true,
      "highlightable": false
    },
    "content": {
      "type": "text",
      "index": true,
      "store": true,
      "sortable": false,
      "aggregatable": true,
      "highlightable": true,
      "analyzer": "gse_search",
      "search_analyzer": "gse_standard"
    },
    "create_time": {
      "type": "date",
      "format": "2006-01-02 15:04:05",
      "index": true,
      "store": false,
      "sortable": true,
      "aggregatable": true,
      "highlightable": false
    },
    "title": {
      "type": "text",
      "index": true,
      "store": true,
      "sortable": false,
      "aggregatable": false,
      "highlightable": true,
      "analyzer": "gse_search",
       "search_analyzer": "gse_standard"
    }
  }
}
```

使用：
```bash
curl -X PUT http://localhost:4080/api/web/_doc/1 -i -u admin:admin@123 -H 'Content-Encoding: UTF-8' -d '{ "title": "俄军攻入北顿涅茨克市中心","create_time": "2022-05-31 12:29:55","content": "北顿涅茨克之战打了好几天，乌军承认现在处于劣势。据美国有线电视新闻网报道，乌方的卢甘斯克地区负责人30日表示，俄罗斯军队正在向北顿涅茨克市中心推进。此前俄军已经巩固了对该市东北部和东南部郊区的控制，并试图包围北顿涅茨克和利西昌斯克。“目前战斗非常激烈，”乌方的这名负责人表示，当地情况非常困难，除了通过星链进行权限有限的连接外，该市已经没有互联网连接。"}'
``` 
 
 
 


