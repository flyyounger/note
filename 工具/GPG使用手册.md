#### 创建：
 gpg --full-generate-key

#### 列出：
 gpg -k

#### 生成撤销证书：
 gpg --gen-revoke bairadish@gmail.com


#### 导出ASCII的公钥和私钥：
gpg --armor --output public-key2.txt --export bairadish@gmail.com
gpg --armor --output private-key2.txt --export-secret-keys bairadish@gmail.com

#### 加密和解密：
gpg --recipient 13E09ED9D87D6C31  --output demo.en.txt --encrypt docker
gpg --decrypt demo.en.txt >de.txt

#### 签名和验签：
gpg --verify docker.asc docker
gpg --armor --detach-sign docker
gpg --verify docker.asc docker
