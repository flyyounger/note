## 1.数字证书常见标准

什么是CA？

CA就相当于一个认证机构，只要经过这个机构签名的证书我们就可以当做是可信任的。我们的浏览器中，已经被写入了默认的CA根证书。

什么是证书？

证书就是将我们的公钥和相关信息写入一个文件，CA用它们的私钥对我们的公钥和相关信息进行签名后，将签名信息也写入这个文件后生成的一个文件。

- 符合PKI ITU-T X509标准，传统标准（.DER .PEM .CER .CRT）

- 符合PKCS#7 加密消息语法标准(.P7B .P7C .SPC .P7R)

- 符合PKCS#10 证书请求标准(.p10)

- 符合PKCS#12 个人信息交换标准（.pfx *.p12） 

X509是数字证书的基本规范，而P7和P12则是两个实现规范，P7用于数字信封，P12则是带有私钥的证书实现规范。

### x509

基本的证书格式，只包含公钥。 x509证书由用户公共密钥和用户标识符组成。此外还包括版本号、证书序列号、CA标识符、签名算法标识、签发者名称、证书有效期等信息。X.509 定义了数字证书格式的标准，详情请参见维基百科词条 ，通过 Openssl 命令行 openssl x509 -in certificate.crt -text -noout 工具可以查看证书格式



### PKCS#7

Public Key Cryptography Standards #7。 PKCS#7一般把证书分成两个文件，一个公钥、一个私钥，有PEM和DER两种编码方式。PEM比较多见，是纯文本的，一般用于分发公钥，看到的是一串可见的字符串，通常以.crt，.cer，.key为文件后缀。DER是二进制编码。 PKCS#7一般主要用来做数字信封。

### PKCS#10

证书请求语法。

### PKCS#12

Public Key Cryptography Standards #12。 一种文件打包格式，为存储和发布用户和服务器私钥、公钥和证书指定了一个可移植的格式，是一种二进制格式，通常以.pfx或.p12为文件后缀名。 使用OpenSSL的pkcs12命令可以创建、解析和读取这些文件。 P12是把证书压成一个文件，xxx.pfx 。主要是考虑分发证书，私钥是要绝对保密的，不能随便以文本方式散播。所以P7格式不适合分发。.pfx中可以加密码保护，所以相对安全些。

### PKCS系列标准

实际上PKCS#7、PKCS#10、PKCS#12都是PKCS系列标准的一部分。相互之间并不是替代的关系，而是对不同使用场景的定义。

## 2.编码方式

.pem 后缀的证书都是base64编码

.der  后缀的证书都是二进制格式

##  3.SSL证书常见后缀

根据不同的服务器以及服务器的版本，我们需要用到不同的证书格式，就市面上主流的服务器来说，大概有以下格式：

- .DER .CER，文件是二进制格式，只保存证书，不保存私钥。
- .PEM，一般是文本格式，可保存证书，可保存私钥。
- .CRT，可以是二进制格式，可以是文本格式，与 .DER 格式相同，不保存私钥。
- .PFX .P12，二进制格式，同时包含证书和私钥，一般有密码保护。
- .JKS，二进制格式，同时包含证书和私钥，一般有密码保护。

### DER

该格式是二进制文件内容，Java 和 Windows 服务器偏向于使用这种编码格式。

OpenSSL 查看

```
openssl x509 -in certificate.der -inform der -text -noout
```

转换为 PEM：

```
openssl x509 -in cert.crt -inform der -outform pem -out cert.pem
```

### PEM

Privacy Enhanced Mail，一般为文本格式，以 `-----BEGIN...` 开头，以 `-----END...` 结尾。中间的内容是 BASE64 编码。这种格式可以保存证书和私钥，有时我们也把PEM 格式的私钥的后缀改为 .key 以区别证书与私钥。具体你可以看文件的内容。

这种格式常用于 Apache 和 Nginx 服务器。

OpenSSL 查看：

```
openssl x509 -in certificate.pem -text -noout
```

PEM转换为 DER：

```
openssl x509 -in cert.crt -outform der -out cert.der
```

 DER到PEM

```
openssl x509 -in cert.crt -inform der -outform pem -out cert.pem 
```



### CRT

Certificate 的简称，有可能是 PEM 编码格式，也有可能是 DER 编码格式。如何查看请参考前两种格式。

#### CER扩展名

CER = .crt的替代形式（Microsoft Convention）您可以在微软系统环境下将.crt转换为.cer（.both DER编码的.cer，或base64 [PEM]编码的.cer）。

可参考：https://support.comodo.com/index.php?/Knowledgebase/Article/View/361/17/how-do-i-convert-crt-file-into-the-microsoft-cer-format

.cer文件扩展名也被IE识别为 一个运行MS cryptoAPI命令的命令（特别是rundll32.exe cryptext.dll，CryptExtOpenCER），该命令显示用于导入和/或查看证书内容的对话框。 

### CSR

证书请求文件(Certificate Signing Request)。 这个并不是证书，而是向权威证书颁发机构获得签名证书的申请，其核心内容是一个公钥(当然还附带了一些别的个人信息)。 查看的办法：openssl req -noout -text -in my.csr，DER格式的话加上-inform der。

### PFX

Predecessor of PKCS#12，这种格式是二进制格式，且证书和私钥存在一个 PFX 文件中。一般用于 Windows 上的 IIS 服务器，springboot也偏好这种证书。改格式的文件一般会有一个密码用于保证私钥的安全。**一份 .p12 文件 = X.509 证书+私钥**

OpenSSL 查看：

```
openssl pkcs12 -in server.p12 -cacerts
openssl pkcs12 -in server.p12 -clcerts
openssl pkcs12 -in server.p12 -nocerts -nodes
```

转换为 PEM：

```
openssl pkcs12 -in for-iis.pfx -out for-iis.pem -nodes
```

 生成和查看：

```bash
keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore inspur2.p12 -validity 3650
keytool -list -v -storetype pkcs12 -keystore .\inspur2.p12
```

使用OpenSSL工具，可以将PFX文件转化为密钥文件KEY和公钥文件CRT。

将PFX文件放到OpenSSL目录下，打开OpenSSL执行以下命令：

```bash
openssl pkcs12 -in server.pfx -nodes -out server.pem
openssl rsa -in server.pem -out server.key
openssl x509 -in server.pem -out server.crt

openssl pkcs12 -in certificate.pfx  -nocerts -out private.pem
openssl pkcs12 -in certificate.pfx -clcerts -nokeys -out public.pem
```

将 PEM/KEY/CRT 格式证书转换为 PFX 格式

```bash
 openssl pkcs12 -export -out server.pfx -inkey server.key -in server.crt
```

### JKS/Truststore/Keystore

Java Key Storage，很容易知道这是 JAVA 的专属格式，利用 JAVA 的一个叫 `keytool` 的工具可以进行格式转换。一般用于 Tomcat 服务器。

将 JKS 格式证书转换成 PFX 格式

```bash
keytool -importkeystore -srckeystore D:\server.jks -destkeystore D:\server.pfx -srcstoretype JKS -deststoretype PKCS12 
```

将 PFX 格式证书转换为 JKS 格式

```bash
keytool -importkeystore -srckeystore D:\server.pfx -destkeystore D:\server.jks -srcstoretype PKCS12 -deststoretype JKS 
```



### KEY

可以被编码为二进制DER或ASCII PEM， **它并非 X.509 证书格式**，。

通常用来存放一个公钥或者私钥。 查看KEY的办法：openssl rsa -in mykey.key -text -noout 如果是DER格式的话，同理应该这样了：openssl rsa -in mykey.key -text -noout  -inform der 这是使用RSA算法生成的key这么查看，DSA算法生成的使用dsa参数。



知识点：

1、使用公钥操作数据属于加密

2、使用私钥操作数据属于签名

3、公钥和私钥可以互相加解密

4、不同格式的证书之间可以互相转换

5、公钥可以对外公开，但是私钥千万不要泄露，要妥善保存

注意：在我们备份证书信息的时候，最好使用.jks或者.pfx文件进行保存，这样备份的证书文件可以被完整的导出。

我们在使用证书的时候，要根据不同平台，不同应用的要求，转换成不同的格式进行使用。

## 4.使用

### 如何使用公钥加密私钥解密

```bash
echo Hello > file.txt
openssl rsautl -encrypt -inkey public.pem -pubin -in file.txt -out file.encrypted.txt
openssl rsautl -decrypt -inkey private.pem -in file.encrypted.txt -out file.decrypted.txt
```

### 如何使用私钥签名公钥验签

考虑一个问题，为什么没有用私钥加密公钥解密的场景？因为公钥是公开发布的，如果要用一个公开发布的秘钥来解密，这个场景就不是加解密，而是数字签名。

数字签名过程

- A 首先对消息使用 HASH 算法得到消息摘要
- A 使用私钥对消息摘要进行加密（为什么不直接对消息或文件进行加密而是对其摘要进行加密？因为非对称加密效率不高，非常耗时）。
- A 将消息和加密后的摘要（签名）发给接收方 B
- 接收方用 B 使用 A 的公钥对签名进行解密得到一个 摘要，然后用同样的 HASH 算法得到消息的摘要，对这两个摘要比对，如果完全相同，则说明消息是 A 发送的，没有被篡改。

### 生成X509格式的CA自签名证书

```
# openssl req -new -x509 -keyout ca.key -out ca.crt 
```

### 生成服务端的私钥(key文件)及csr文件

```
# openssl genrsa -des3 -out server.key 1024 
# openssl req -new -key server.key -out server.csr 
```

### 用生成的CA的证书为刚才生成的server.csr,client.csr文件签名

```
# openssl ca -in server.csr -out server.crt -cert ca.crt -keyfile ca.key 
# openssl ca -in client.csr -out client.crt -cert ca.crt -keyfile ca.key 
```

### 生成p12格式证书

```
# openssl pkcs12 -export -inkey client.key -in client.crt -out client.pfx 
# openssl pkcs12 -export -inkey server.key -in server.crt -out server.pfx 
```
生成证书请求(.csr)及私钥(.key) -> 将请求文件(.csr)提交给证书颁发机构 -> 机构使用其根证书私钥签名,生成了证书公钥文件 -> 返回证书(.cer或.crt)给用户 -> 用户根据使用需求转换证书格式(.pem .p12 .jks等等)

### 生成证书请求和私钥

```
openssl req -new -newkey rsa:2048 -nodes -out req_file_name.csr -keyout key_file_name.key 
#执行命令后需要根据提示输入相关信息，大部分内容可以跳过;
#通常我们会输入域名信息进行绑定，这样如果网站使用的域名与证书内的域名信息不匹配时，浏览器会报出安全警告:
#不建议绑定IP，这样不便于管理证书;
#可以使用-subj选项来提前输入一些描述信息。
```

## 5.SSL和TLS

TLS 1.0 = SSL 3.1
TLS 1.1 = SSL 3.2
TLS 1.2 = SSL 3.3

禁用废弃协议 SSL和TLS 1,TLS 1.1都不安全

```bash
SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1 #Apache
ssl_protocols TLSv1.2; #Nginx
sslEnabledProtocols = "TLSv1.2" #tomcat server.xml
```

## 6.根据IP获取域名
```bash
curl https://39.156.69.79 -sv --output /dev/null 2>&1 | grep CN=
openssl s_client -connect 39.156.69.79:443
```

## 7.来验证一下，以 GitHub 使用的根证书 DigiCert High Assurance EV Root CA 为例
```bash
# 首先，打开 Keychain Access，找到上述证书，拖拽出来
# 重命名为 root.cer

# 转换 DER 到 PEM 格式
$ openssl x509 -inform der -in root.cer -out root.pem

# 查看证书的签名，可以看到签名所使用的的 hash 算法是 sha1
$ openssl x509 -in root.pem -text -noout -certopt ca_default -certopt no_validity -certopt no_serial -certopt no_subject -certopt no_extensions -certopt no_signame
    Signature Algorithm: sha1WithRSAEncryption
         1c:1a:06:97:dc:d7:9c:9f:3c:88:66:06:08:57:21:db:21:47:
         f8:2a:67:aa:bf:18:32:76:40:10:57:c1:8a:f3:7a:d9:11:65:
         8e:35:fa:9e:fc:45:b5:9e:d9:4c:31:4b:b8:91:e8:43:2c:8e:
         b3:78:ce:db:e3:53:79:71:d6:e5:21:94:01:da:55:87:9a:24:
         64:f6:8a:66:cc:de:9c:37:cd:a8:34:b1:69:9b:23:c8:9e:78:
         22:2b:70:43:e3:55:47:31:61:19:ef:58:c5:85:2f:4e:30:f6:
         a0:31:16:23:c8:e7:e2:65:16:33:cb:bf:1a:1b:a0:3d:f8:ca:
         5e:8b:31:8b:60:08:89:2d:0c:06:5c:52:b7:c4:f9:0a:98:d1:
         15:5f:9f:12:be:7c:36:63:38:bd:44:a4:7f:e4:26:2b:0a:c4:
         97:69:0d:e9:8c:e2:c0:10:57:b8:c8:76:12:91:55:f2:48:69:
         d8:bc:2a:02:5b:0f:44:d4:20:31:db:f4:ba:70:26:5d:90:60:
         9e:bc:4b:17:09:2f:b4:cb:1e:43:68:c9:07:27:c1:d2:5c:f7:
         ea:21:b9:68:12:9c:3c:9c:bf:9e:fc:80:5c:9b:63:cd:ec:47:
         aa:25:27:67:a0:37:f3:00:82:7d:54:d7:a9:f8:e9:2e:13:a3:
         77:e8:1f:4a

# 提取签名内容到文件中
$ openssl x509 -in root.pem -text -noout -certopt ca_default -certopt no_validity -certopt no_serial -certopt no_subject -certopt no_extensions -certopt no_signame | grep -v 'Signature Algorithm' | tr -d '[:space:]:' | xxd -r -p > root-signature.bin

# 提取根证书中含有的公钥
$ openssl x509 -in root.pem -noout -pubkey > root-pub.pem

# 使用公钥解密签名
$ openssl rsautl -verify -inkey root-pub.pem -in root-signature.bin -pubin > root-signature-decrypted.bin

# 查看解密后的内容
# 可以看到，签名中存储的 hash 值为 E35E...13A8
$ openssl asn1parse -inform DER -in root-signature-decrypted.bin
    0:d=0  hl=2 l=  33 cons: SEQUENCE
    2:d=1  hl=2 l=   9 cons: SEQUENCE
    4:d=2  hl=2 l=   5 prim: OBJECT            :sha1
   11:d=2  hl=2 l=   0 prim: NULL
   13:d=1  hl=2 l=  20 prim: OCTET STRING      [HEX DUMP]:E35EF08D884F0A0ADE2F75E96301CE6230F213A8

# 接下来我们计算证书的 hash 值

# 首先提取证书的 body
# 因为证书中含有签名，签名是不包含在 hash 值计算中的
# 所以不能简单地对整个证书文件进行 hash 运算
$ openssl asn1parse -in root.pem -strparse 4 -out root-body.bin &> /dev/null

# 计算 sha1 哈希值
$ openssl dgst -sha1 root-body.bin
SHA1(root-body.bin)= e35ef08d884f0a0ade2f75e96301ce6230f213a8

```

hash 值匹配，这也就说明根证书确实是自签名的，用自己的私钥给自己签名。

## 8.纸上得来终觉浅

理论知识我们已经全部具备了，接下来我们来完整走一遍流程，以 github.com 为例，校验一下它的证书是否有效。

```bash
# 新建一个文件夹 github 保存所有的文件
$ mkdir github && cd github

# 首先，我们下载 github.com 发送的证书
$ openssl s_client -connect github.com:443 -showcerts 2>/dev/null </dev/null | sed -n '/-----BEGIN/,/-----END/p' > github.com.crt

# github.com.crt 是 PEM 格式的文本文件
# 打开可以发现里面有两段 -----BEGIN CERTIFICATE----
# 这说明有两个证书，也就是 github.com 把中间证书也一并发过来了

# 接下来我们把两个证书提取出来
$ awk '/BEGIN/,/END/{ if(/BEGIN/){a++}; out="cert"a".tmpcrt"; print >out}' < github.com.crt && for cert in *.tmpcrt; do newname=$(openssl x509 -noout -subject -in $cert | sed -n 's/^.*CN=\(.*\)$/\1/; s/[ ,.*]/_/g; s/__/_/g; s/^_//g;p').pem; mv $cert $newname; done

# 我们得到了两个证书文件
# github_com.pem 和 DigiCert_SHA2_High_Assurance_Server_CA.pem

# 首先，验证 github_com.pem 证书确实
# 是由 DigiCert_SHA2_High_Assurance_Server_CA.pem 签发的

# 提取 DigiCert_SHA2_High_Assurance_Server_CA 的公钥
# 命名为 issuer-pub.pem
$ openssl x509 -in DigiCert_SHA2_High_Assurance_Server_CA.pem -noout -pubkey > issuer-pub.pem

# 查看 github_com.pem 的签名
$ openssl x509 -in github_com.pem -text -noout -certopt ca_default -certopt no_validity -certopt no_serial -certopt no_subject -certopt no_extensions -certopt no_signame
    Signature Algorithm: sha256WithRSAEncryption
         86:32:8f:9c:15:b8:af:e8:d1:de:08:3a:44:0e:71:20:24:d6:
         fc:0e:58:31:cc:aa:b4:ad:1c:d5:0c:c5:af:c4:bb:fe:5f:ac:
         90:6a:42:c8:21:eb:25:f1:6b:2c:37:b2:2a:a8:1a:6e:f2:d1:
         4f:a6:2f:bc:cf:3a:d8:c1:9f:30:c0:ec:93:eb:0a:5a:dc:cb:
         6c:32:1c:60:6e:ec:6e:f8:86:a5:4f:a0:b4:6d:6a:07:4a:21:
         58:d0:29:7d:65:8a:c8:da:6a:ba:ab:f0:75:21:33:00:40:6f:
         85:c5:13:e6:27:73:6c:ae:ea:e3:96:d0:53:db:c1:21:68:10:
         cf:e3:d8:50:b0:14:ec:a9:98:cf:b8:ce:61:5d:3d:a3:6d:93:
         34:c4:13:fa:11:66:a3:dd:be:10:19:70:49:e2:04:4d:81:2c:
         1f:2e:59:c6:2c:53:45:3b:ee:f6:13:f4:d0:2c:84:6e:28:6d:
         e4:e4:ca:e4:48:89:1b:ab:ec:22:1f:ee:12:d4:6c:75:e9:cc:
         0b:15:74:e9:6d:9f:db:40:1f:e2:24:85:a3:4b:a4:e9:cd:6b:
         c8:77:9f:87:4f:05:73:00:38:a5:23:54:68:fc:a2:3d:bf:18:
         19:0e:a8:fd:b9:5e:8c:5c:e8:fc:e4:a2:52:70:ee:79:a7:d2:
         27:4a:7a:49

# 提取签名到文件中
$ openssl x509 -in github_com.pem -text -noout -certopt ca_default -certopt no_validity -certopt no_serial -certopt no_subject -certopt no_extensions -certopt no_signame | grep -v 'Signature Algorithm' | tr -d '[:space:]:' | xxd -r -p > github_com-signature.bin

# 使用上级证书的公钥解密签名
$ openssl rsautl -verify -inkey issuer-pub.pem -in github_com-signature.bin -pubin > github_com-signature-decrypted.bin

# 查看解密后的信息
$ openssl asn1parse -inform DER -in github_com-signature-decrypted.bin
    0:d=0  hl=2 l=  49 cons: SEQUENCE
    2:d=1  hl=2 l=  13 cons: SEQUENCE
    4:d=2  hl=2 l=   9 prim: OBJECT            :sha256
   15:d=2  hl=2 l=   0 prim: NULL
   17:d=1  hl=2 l=  32 prim: OCTET STRING      [HEX DUMP]:A8AA3F746FE780B1E2E5451CE4383A9633C4399E89AA3637252F38F324DFFD5F

# 可以发现，hash 值是 A8AA...FD5F

# 接下来计算 github_com.pem 的 hash 值

# 提取证书的 body 部分
$ openssl asn1parse -in github_com.pem -strparse 4 -out github_com-body.bin &> /dev/null

# 计算 hash 值
$ openssl dgst -sha256 github_com-body.bin
SHA256(github_com-body.bin)= a8aa3f746fe780b1e2e5451ce4383a9633c4399e89aa3637252f38f324dffd5f
```

hash 值匹配，我们成功校验了 github.pem 这个证书确实是由 DigiCert_SHA2_High_Assurance_Server_CA.pem 这个证书来签发的。

上面的流程比较繁琐，其实也可以直接让 openssl 来帮我们验证。

```bash
$ openssl dgst -sha256 -verify issuer-pub.pem -signature github_com-signature.bin  github_com-body.bin
Verified OK
```



接下来的过程是上面一样，首先，我们获取上级证书的信息。

```bash
# 获取上级证书的名字
$ openssl x509 -in DigiCert_SHA2_High_Assurance_Server_CA.pem -text -noout | grep Issuer:
        Issuer: C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert High Assurance EV Root CA
```

然后就是校验 DigiCert_SHA2_High_Assurance_Server_CA 是由 DigiCert High Assurance EV Root CA 来签发的，同时这个证书存在于我们系统中。

这一步我就不再重复了，留给大家作为练习~



## 9.双向验证

```bash
# 生成服务端密钥文件localhost.jks
keytool -genkey -alias localhost -keyalg RSA -keysize 2048 -sigalg SHA256withRSA -keystore localhost.jks -dname CN=localhost,OU=Test,O=pkslow,L=Guangzhou,C=CN -validity 731 -storepass changeit -keypass changeit

# 导出服务端的cert文件
keytool -export -alias localhost -file localhost.cer -keystore localhost.jks

# 生成客户端的密钥文件client.jks
keytool -genkey -alias client -keyalg RSA -keysize 2048 -sigalg SHA256withRSA -keystore client.jks -dname CN=client,OU=Test,O=pkslow,L=Guangzhou,C=CN -validity 731 -storepass changeit -keypass changeit

# 导出客户端的cert文件
keytool -export -alias client -file client.cer -keystore client.jks

# 把客户端的cert导入到服务端
keytool -import -alias client -file client.cer -keystore localhost.jks

# 把服务端的cert导入到客户端
keytool -import -alias localhost -file localhost.cer -keystore client.jks

# 检验服务端是否具有自己的private key和客户端的cert
keytool -list -keystore localhost.jks
```

成功执行完上述步骤后，会生成4个文件：

- 服务端密钥文件： localhost.jks
- 服务端cert文件：localhost.cer
- 客户端密钥文件：client.jks
- 客户端cert文件：client.cer

实际上`cert`文件可以不要了，因为已经导入对方的**Trust Store**里面去了。也就是在文件`localhost.jks`里，包含了服务端的私钥、公钥还有客户端的公钥。`client.jks`同理。

springboot配置

```bash
server.port=443

server.ssl.enabled=true
server.ssl.key-store-type=JKS
server.ssl.key-store=classpath:localhost.jks
server.ssl.key-store-password=changeit
server.ssl.key-alias=localhost

server.ssl.trust-store=classpath:localhost.jks
server.ssl.trust-store-password=changeit
server.ssl.trust-store-provider=SUN
server.ssl.trust-store-type=JKS
server.ssl.client-auth=need
```

需要分别配置`Key Store`和`Trust Store`的文件、密码等信息，即使是同一个文件。

需要注意的是，`server.ssl.client-auth`有三个可配置的值：`none`、`want`和`need`。双向验证应该配置为`need`；`none`表示不验证客户端；`want`表示会验证，但不强制验证，即验证失败也可以成功建立连接。

为了建立连接，应该要把客户端的密钥文件给`Postman`使用。因为`JKS`是Java的密钥文件格式，我们转换成通用的`PKCS12`格式如下：

```bash
# 转换JKS格式为P12
keytool -importkeystore -srckeystore client.jks -destkeystore client.p12 -srcstoretype JKS 
```

或者我们可以把密钥文件拆成`private key`和`cert`，命令如下：

```bash
# 导出客户端的cert文件
openssl pkcs12 -nokeys -in client.p12 -out client.pem

# 导出客户端的key文件
openssl pkcs12 -nocerts -nodes -in client.p12 -out client.key
```
refer: https://segmentfault.com/a/1190000022546935
