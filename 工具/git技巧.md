#### 用仓库的文件强制覆盖本地的修改

```
git log -3 CreditPieceMapper.xml
git reset d0b37d49d1dd5e936964b00ee75a01f09833fc6d CreditPieceMapper.xml
git checkout -- CreditPieceMapper.xml
```
#### 减小git目录占用的空间

```
git gc
```
#### 从Working Tree和Index移除文件

```
git rm
git rm -f #不做update检查
git rm –cached #只从Index中移除，保留Working Tree中的文件状态
```
#### 显示日志

```
git log --graph --oneline --decorate
--graph：图形化显示分支提交历史
--oneline：一个提交显示一行
--decorate：显示分支引用

git log --pretty=format:"%h %t %p %an %s" --graph
%h：Commit对象的简短哈希串
%t：Tree对象的简短哈希串
%p：父Commit对象的简短哈希串
%an：作者名字
%ae：作者邮件
%ad：修订日期
%s：Commit Message

--grep：搜索提交说明
--author：匹配作者
--committer：匹配提交者
--after：时间起点
--before：时间终点
--：特定路径

美化日志
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
 git log -3 --oneline --pretty=format:"%cn committed %h on %cd->%s" --date=format:'%Y-%m-%d %H:%M:%S'
```
#### 修改提交日志

```
git commit --amend -m "amend commit msg"
```
#### 用Index覆盖Working Tree

```
git checkout -- read.txt
```
#### 显示Git对象信息

```
git show --oneline -s 4f6c14f
git show --oneline -s HEAD
```
#### 更新引用

```
git update-ref refs/heads/master HEAD~
git update-ref refs/heads/master 85746a6
```
#### 回到远程仓库的状态

抛弃本地所有的修改，回到远程仓库的状态。
```bash
git fetch --all && git reset --hard origin/master
```
#### 重设第一个 commit

也就是把所有的改动都重新放回工作区，并**清空所有的 commit**，这样就可以重新提交第一个 commit 了
```
git update-ref -d HEAD
```
#### 本地仓库的操作历史

```
git reflog
```
#### 修改head的引用

```
 git reset --hard dbe82e8  #--hard直接覆盖未提交的修改
```
#### 彻底从历史记录中的删除Git提交的文件

``` shell
git filter-branch --index-filter 'git rm --cached --ignore-unmatch *PaymentApiTest.*' --prune-empty --tag-name-filter cat -- --all
git push origin master --force
rm -rf .git/refs/original/
git reflog expire --expire=now --all
git gc --prune=now
git gc --aggressive --prune=now
```

#### 代码量统计
```shell
#统计某人的代码提交量，包括增加，删除：
git log --author="$(git config --get user.name)" --pretty=tformat: --numstat | gawk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END { printf "added lines: %s removed lines : %s total lines: %s\n",add,subs,loc }' -
# 带上时间限制
git log --author="chenwen" --pretty=tformat: --numstat  --since="2 months"| gawk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END { printf "added lines: %s removed lines : %s total lines: %s\n",add,subs,loc }' -
#提交者排名（找出最懒程序员）
git log --pretty='%aN' | sort | uniq -c | sort -k1 -n -r
#另一种写法
git shortlog -s -n
#提交数统计：
git log --oneline | wc -l
```

### 删除远程服务器上错误的提交
```
#找到并恢复到错误提交上一次的id
git reset --hard 8136dd88
#可以用新的数据覆盖（看需求，中间的add和commit看情况），强制推送
git push --force
```

### 处理不用反复输入密码问题
```
git config --global credential.helper store
```

### 一个简单的自动部署脚本
```
#!/bin/bash
echo -e  "\e[31;47;1m =============开始自动部署front项目=============\e[0m"

cd /home/soft/front/
echo "拉取最新代码\n"
git pull
echo "开始maven编译，请耐心等待...."
CURTIME=`date +"%Y-%m-%d %H:%M:%S"`
mvn clean install
FINISHTIME=`date +"%Y-%m-%d %H:%M:%S"`
ctime=`date -d "$CURTIME" +%s`
ftime=`date -d "$FINISHTIME" +%s`
interval=`echo $ftime-$ctime|bc`
echo "编译完成，耗时 $interval 秒"
if [ ! -f "/home/soft/front/siyi-webapp/target/siyi-webapp-0.0.1-SNAPSHOT.war" ]; then
  echo "maven编译失败未找到war文件，请检查，退出自动部署"
  exit
fi
pid=$(ps -ef|grep tomcat-8080-front|grep -v grep|awk '{print $2}')
# 注意，这里不要写成grep front这样，这样会把自己给杀了。要么重命名下这个脚本
echo "杀死该项目对应的tomcat进程 $pid "
kill -9 ${pid}
echo "拷贝war包\n"
cd /home/tomcat/tomcat-8080-front/webapps/
rm -rf ROOT*
mv /home/soft/front/siyi-webapp/target/siyi-webapp-0.0.1-SNAPSHOT.war /home/tomcat/tomcat-8080-front/webapps/ROOT.war
echo "重启tomcat\n"
cd /home/tomcat/tomcat-8080-front/bin
./startup.sh
echo "重启完毕==============自动部署完毕\n"
httpcode=$(curl -I -m 30 10.180.210.101:9999/swagger-ui.html -o /dev/null -s -w %{http_code})
echo "检查业务状态码 status= ${httpcode}"
```

### 分支
```
从本地推送分支，使用
git push origin branch-name，
如果推送失败，先用git pull抓取远程的新提交；
git pull origin master --allow-unrelated-histories
在本地创建和远程分支对应的分支，使用
git checkout -b branch-name origin/branch-name，本地和远程分支的名称最好一致；

建立本地分支和远程分支的关联，使用
git branch --set-upstream branch-name origin/branch-name
```
新建并切换到新分支上，同时这个分支没有任何 commit

相当于保存修改，但是重写 commit 历史
```
git checkout --orphan <branch-name>
```
### 其他
```
git rev-list --count HEAD: 查看从初始版本到当前版本的提交个数
## 展示所有忽略的文件
git ls-files --others -i --exclude-standard
## commit 历史中显示 Branch1 有的，但是 Branch2 没有 commit
git log Branch1 ^Branch2
## clone 下来指定的单一分支
git clone -b <branch-name> --single-branch https://github.com/user/repo.git
## 以最后提交的顺序列出所有 Git 分支,最新的放在最上面
git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/
git remote show origin
```

### git 浅克隆
```
git clone --depth 1 https://github.com/torvalds/linux.git
#浅表克隆转换为完整克隆
git fetch --unshallow
```

### 代理
```
export ALL_PROXY=socks5://127.0.0.1:1080
或者
git config --global http.proxy 'socks5://127.0.0.1:1080'
git config --global https.proxy 'socks5://127.0.0.1:1080'
```
### fork后如何与原仓库同步
```
1. 查看你的远程仓库的路径
git remote -v
2. 配置原仓库的路径
 git remote add upstream https://gitee.com/y_project/RuoYi.git
3. 抓取原仓库的修改文件
 git fetch upstream
4. 切换到master分支
 git checkout master 
5. 合并远程的master分支
 git merge upstream/master
 然后推送到远程即可。
```
### 如何将一个已存在的目录转换为一个 GIT 项目并托管到 GITHUB 仓库
```
git init
git add .
git status
git commit -m "Initial commit"
git remote add origin https://github.com/iminto/doopie.git
git remote -v
git push origin master
```
### powersheel乱码
```
git config --global core.quotepath false
git config --global gui.encoding utf-8
git config --global i18n.commit.encoding utf-8
git config --global i18n.logoutputencoding utf-8
$env:LESSCHARSET='utf-8'
```
### 删除远程分支
```bash
git branch -dr  origin/gfb
git push origin --delete gfb
```