### p12证书
 keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore inspur2.p12 -validity 3650
 keytool -list -v -storetype pkcs12 -keystore .\inspur2.p12
 
 Springboot使用：
 ```yaml
 server:
  port: ${PORT:9000}
  ssl:
    key-store: ${SSLCERT:classpath:keystore.p12}
    key-store-password: inspur
    key-store-type: PKCS12
    keyAlias: tomcat
    enabled: true
 ```
 注意：keyalias必须配对