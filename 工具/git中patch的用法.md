git中重用提交的方式有两种, 一种是cherry-pick, 另一种便是patch.
```bash
#生成距离HEAD最近的n个patch
git format-patch -2
#生成A-B之间的patch
git format-patch <old-commit-sha>...<new-commit-sha> -o <patch-file-dir>
#检查patch文件
git apply --stat ~/temp_patch/0001-add-content-to-bb.c.patch
#查看是否能应用成功
git apply --check ~/temp_patch/0001-add-content-to-bb.c.patch
#应用patch文件
git am -s ./0001-add-content-to-bb.c.patch
```
apply patch成功后会自动commit，并且保留原来commit的comments，submittor等信息。
但是commit id会新生成(目前我也不知道有什么方法能保留commit id)