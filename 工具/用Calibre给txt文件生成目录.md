problem
1、众所周知，电子书有多种格式，十分混乱，如txt，epub，mobi，azw3等等
2、txt格式是不支持目录的，有很多看书软件有根据标题生成目录的功能，但只能用于阅读模式，转成其它格式后就没有目录了。
3、所以，可以用calibre给来生成目录，转成其他格式

官网下载地址：https://calibre-ebook.com/download
github源代码地址：https://github.com/kovidgoyal/calibre

将电子书txt拖入calibre，找到转换书籍
在结构检测的xpath表达式中粘贴代码，点击转换即可。
//*[re:test(., "^\s*[第卷][0123456789一二三四五六七八九十零〇百千两]*[章回部节集卷].*", "i")]