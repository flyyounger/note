一堆商品的单价是XXX,YYY,ZZZ。。然后总价是AAA，求商品组合。

```
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static int getRemain(int bag,List<Integer> items){
        for(int idx=items.size()-1;idx>=0;idx--){
                if(bag<items.get(idx)){
                   continue;
                }else{
                    int remain=bag%items.get(idx);
                    double cnt=Math.ceil(bag/items.get(idx));
                    System.out.println("包裹是"+items.get(idx)+"，装了"+cnt+"个--剩余"+remain);
                    if(remain!=0){
                        return getRemain(remain,items);
                    }else{
                        return remain;
                    }
                }
        }
        return 0;
    }

    public static void main(String[] args) throws Exception {
        List<Integer> items=new ArrayList<>();
        //price=[58,98,128,168,188,288,479,558,568,928,999,1098,1799,2098
        items.add(58);
        items.add(98);
        items.add(128);
        items.add(168);
        items.add(188);
        items.add(288);
        items.add(479);
        items.add(558);
        items.add(568);
        items.add(928);
        items.add(999);
        items.add(1098);
        items.add(1799);
        items.add(2098);//先排序
        Integer bags=1403322;
        getRemain(bags, items);

    }
}

```
