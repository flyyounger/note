###  spring Framework连接池报这个错误：
org.apache.commons.dbcp.SQLNestedException: Cannot load JDBC driver class 'com.mysql.jdbc.Driver '
Caused by: java.lang.ClassNotFoundException: com.mysql.jdbc.Driver
可以：

```
将
<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource">
改为
<bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
即可。
```
###  非spring中获取bean

```
ApplicationContext ctx = new ClassPathXmlApplicationContext("\\spring\\applicationContext.xml");
JmsTemplate jmsTemplate=(JmsTemplate)ctx.getBean("jmsTemplate");
//如果是junit种可以如下：
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml" })
public class TestUtil {

	@Autowired
	SmsChannelServiceImpl smsChannelServiceimpl;
```
### 管理spring4中的定时器

```
package com.baicai.core;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.config.Task;
import org.springframework.stereotype.Component;
@Component("scheduledManage")
public class ScheduledManage {

    private ScheduledTaskRegistrar scheduledTaskRegistrar;
    public List<Task> getScheduledTasks() {
        List<Task> result = new ArrayList<Task>();
        result.addAll(this.scheduledTaskRegistrar.getTriggerTaskList());
        result.addAll(this.scheduledTaskRegistrar.getCronTaskList());
        result.addAll(this.scheduledTaskRegistrar.getFixedRateTaskList());
        result.addAll(this.scheduledTaskRegistrar.getFixedDelayTaskList());
        return result;
    }
    public List<CronTask> getScheduledCronTasks() {
        List<CronTask> cronTaskList = this.scheduledTaskRegistrar.getCronTaskList();
        for (CronTask cronTask : cronTaskList) {
            System.out.println(cronTask.getExpression());
        }
        return cronTaskList;
    }
}
//refer:https://issues.apache.org/jira/browse/HADOOP-6148
```
### spring-mvc 非 controller 层获取HttpServletRequest
在web.xml配置

```
<listener>
  <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
 </listener>
```
在service或者dao中获取HttpServletRequest 的代码如下

```
HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
 .getRequestAttributes()).getRequest();
```
### SpringMVC——redirect重定向跳转传值
#### 不带参数

```
return "redirect:/toList";
```
#### 带参数的重定向
第二种情况，列表页面有查询条件，跳转后我的查询条件不能丢掉，这样就需要带参数的了，带参数可以拼接url
方式一：自己手动拼接url

```
new ModelAndView("redirect:/toList？param1="+value1+"&param2="+value2);
```
这样有个弊端，就是传中文可能会有乱码问题。
方式二：用RedirectAttributes，这个是发现的一个比较好用的一个类,这里用它的addAttribute方法，这个实际上重定向过去以后你看url，是它自动给你拼了你的url。
使用方法：

```
public String test(RedirectAttributes attributes)
{
attributes.addAttribute("test", "hello");
return "redirect:/test/test2";
}
```
这样在toController这个方法中就可以通过获得参数的方式获得这个参数，再传递到页面。过去的url还是和方式一一样的。如果你细心的看重定向之后的url地址的话，你就会发现其实和上面的地址是一样的，这样也会出现上面那个方法出现的问题。

介绍一个不会出现中文乱码，而且不会在你的Url上出现你所要传递的数据的，这样就可以保证你在传递数据的安全

```
public String red(RedirectAttributes attributes)
{
attributes.addFlashAttribute("test", "hello");
return "redirect:/test/test2";
}
```
咱们用上面的方法进行数据传递你就会发现不会再Url上出现你要传递的数据，那么数据放到哪里去了，我们就来看看这是Spring 3.0新出现的特性，attributes.addFlashAttribute("test", "hello")实际存储的属性在flashmap，那么flashmap又是什么呢？

Flash 属性 和 RedirectAttribute：通过FlashMap存储一个请求的输出，当进入另一个请求时作为该请求的输入，典型场景如重定向（POST-REDIRECT-GET模式，1、POST时将下一次需要的数据放在FlashMap；2、重定向；3、通过GET访问重定向的地址，此时FlashMap会把1放到FlashMap的数据取出放到请求中，并从FlashMap中删除；从而支持在两次请求之间保存数据并防止了重复表单提交）。

Spring Web MVC提供FlashMapManager用于管理FlashMap，默认使用SessionFlashMapManager，即数据默认存储在session中

既然知道了怎么回事，那么我们就可以把它提取出来，怎么提取呢，很多人会说，既然存在session中，那就从session中获取，结果发现没有，那怎么办？

```
 public String test2(HttpServletRequest request)
{
Map<String,?> map = RequestContextUtils.getInputFlashMap(request);
System.out.println(map.get("test").toString());
return "/test/hello";
}
```
### Spring中实现文件上传
用户必须能够上传图片，因此需要文件上传的功能。比较常见的文件上传组件有Commons FileUpload[（http://jakarta.apache.org/commons/fileupload/）]和 [COS FileUpload](http://www.servlets.com/cos)，Spring已经完全集成了这两种组件，这里我们选择Commons FileUpload。

由于Post一个包含文件上传的Form会以multipart/form-data请求发送给服务器，必须明确告诉DispatcherServlet如何处理MultipartRequest。首先在dispatcher-servlet.xml中声明一个MultipartResolver：

```
<bean id="multipartResolver"
    class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
    <!-- 设置上传文件的最大尺寸为1MB -->
    <property name="maxUploadSize">
        <value>1048576</value>
    </property>
</bean>
```
这样一旦某个Request是一个MultipartRequest，它就会首先被MultipartResolver处理，然后再转发相应的Controller。
在UploadImageController中，将HttpServletRequest转型为MultipartHttpServletRequest，就能非常方便地得到文件名和文件内容：
```
public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        // 转型为MultipartHttpRequest：
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        // 获得文件：
        MultipartFile file = multipartRequest.getFile(" file ");
        // 获得文件名：
        String filename = file.getOriginalFilename();
        // 获得输入流：
        InputStream input = file.getInputStream();
        // 写入文件

        // 或者：
        File source = new File(localfileName.toString());
        multipartFile.transferTo(source);
    }
```
### 生成缩略图 (目录)
当用户上传了图片后，必须生成缩略图以便用户能快速浏览。我们不需借助第三方软件，JDK标准库就包含了图像处理的API。我们把一张图片按比例缩放到120X120大小，以下是关键代码：
```
public static void createPreviewImage(String srcFile, String destFile) {
        try {
            File fi = new File(srcFile); // src
            File fo = new File(destFile); // dest
            BufferedImage bis = ImageIO.read(fi);

            int w = bis.getWidth();
            int h = bis.getHeight();
            double scale = (double) w / h;
            int nw = IMAGE_SIZE; // final int IMAGE_SIZE = 120;
            int nh = (nw * h) / w;
            if (nh > IMAGE_SIZE) {
                nh = IMAGE_SIZE;
                nw = (nh * w) / h;
            }
            double sx = (double) nw / w;
            double sy = (double) nh / h;

            transform.setToScale(sx, sy);
            AffineTransformOp ato = new AffineTransformOp(transform, null);
            BufferedImage bid = new BufferedImage(nw, nh,
                    BufferedImage.TYPE_3BYTE_BGR);
            ato.filter(bis, bid);
            ImageIO.write(bid, " jpeg ", fo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    " Failed in create preview image. Error:  "
                            + e.getMessage());
        }
    }
```
### 解决Spring JdbcTemplate调用queryForObject()方法结果集为空时报异常
#### 方法1，捕获异常：

```
public String queryValue(String nid){
		try {
			String SQL=" SELECT `value` FROM dw_system WHERE nid=?";
			return dao.queryForString(SQL, new Object[]{nid});
		} catch (Exception e) {
			return "";
		}

	}
```
#### 方法2，变通法

```
/**
	 * 查询并获取一个字符串，解决Spring JdbcTemplate调用queryForObject()方法结果集为空时报异常
	 * @param sql
	 * @param o
	 * @return
	 */
	public String queryForString(String sql, Object[] o) {
		List<String> strLst = getJdbcTemplate().query(sql,o,
				new RowMapper<String>() {
					@Override
					public String mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getString(1);
					}
				});
		if (strLst.isEmpty()) {
			return "";
		} else {
			return strLst.get(0);
		}
	}
```
#### 方法3

```
public String queryForString(String sql, Object[] o) {
		return getJdbcTemplate().query(sql,o, new ResultSetExtractor<String>() {
	        @Override
	        public String extractData(ResultSet rs) throws SQLException,DataAccessException {
	            return rs.next() ? rs.getString(1) : null;
	        }
	    });
	}
```
### NamedParameterJdbcTemplate处理in查询

```
package com.test;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import com.java.common.util.BeanFactory;
import com.java.ssmb.entity.domain.UserInfo;
/**
 * 使用NamedParameterJdbcTemplate查询条件sql中in查询和javabean查询
 * 一般spring项目中都存在JdbcTemplate，
 * 例子使用JdbcTemplate创建NamedParameterJdbcTemplate
 *
 * @author yeshun
 *
 */
public class NamedParameterJdbcTemplateTest {
  private NamedParameterJdbcTemplate namedJdbcTemplate;

  public NamedParameterJdbcTemplateTest(){
    JdbcTemplate jdbcTemplate = BeanFactory.getBean("jdbcTemplate",JdbcTemplate.class);
    this.namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
  }

  /*
   * 使用map查询
   */
  public void queryListByMap(List<String> userids){
    String sql = "select * from user_table u where u.user_id in (:userids )";

    Map<String,Object> parameters = new HashMap<String,Object>();
    parameters.put("userids", userids);

    List<Map<String,Object>> list = namedJdbcTemplate.queryForList(sql, parameters);

    for (Map<String, Object> map : list) {
      System.out.println("-------------------");
      System.out.println(map.get("user_id"));
      System.out.println(map.get("user_name"));
    }
  }

  /*
   * 使用MapSqlParameterSource查询
   */
  public void queryListByMapSqlParameterSource(List<String> userids){
    String sql = "select * from user_table u where u.user_id in (:userids )";

    MapSqlParameterSource parameters = new MapSqlParameterSource();
    parameters.addValue("userids", userids);

    List<Map<String,Object>> list = namedJdbcTemplate.queryForList(sql, parameters);

    for (Map<String, Object> map : list) {
      System.out.println("-------------------");
      System.out.println(map.get("user_id"));
      System.out.println(map.get("user_name"));
    }
  }

  /*
   * 使用BeanPropertySqlParameterSource查询，javabean查询
   */
  public void querySingleByBeanPropertySqlParameterSource(){
    //:userId -- 对应UserInfo.userId
    String sql = "select * from user_table u where u.user_id = :userId ";

    UserInfo user = new UserInfo();
    user.setUserId("user0");
    BeanPropertySqlParameterSource parameters = new BeanPropertySqlParameterSource(user);

    Map<String,Object> map = namedJdbcTemplate.queryForMap(sql, parameters);

    System.out.println(map.get("user_id"));
    System.out.println(map.get("user_name"));

  }

  /*
   * 查询单个返回值
   */
  public void querySingle(List<String> userids){
    String sql = "select count(*) from user_table u where u.user_id in (:userids )";
    MapSqlParameterSource parameters = new MapSqlParameterSource();
    parameters.addValue("userids", userids);

    Integer count = namedJdbcTemplate.queryForObject(sql, parameters, Integer.class);
    // queryForInt
//		Integer count = namedJdbcTemplate.queryForInt(sql, parameters);
    System.out.println(count);
  }

  /*
   * 使用MapSqlParameterSource查询,使用RowMapper返回
   */
  public void queryUsersByMapSqlParameterSource(List<String> userids){
    String sql = "select * from user_table u where u.user_id in (:userids )";

    MapSqlParameterSource parameters = new MapSqlParameterSource();
    parameters.addValue("userids", userids);

    List<UserInfo> userList = namedJdbcTemplate.query(sql, parameters,new RowMapper<UserInfo>(){
      @Override
      public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        // TODO Auto-generated method stub
        UserInfo user = new UserInfo();
        user.setUserId(rs.getString("user_id"));
        user.setUserName(rs.getString("user_name"));
        user.setPassword(rs.getString("password"));
        return user;
      }
    });

    for (UserInfo userInfo : userList) {
      System.out.println(userInfo.getUserId()+"--"+userInfo.getUserName());
    }
  }

  public static void main(String[] args) {
    NamedParameterJdbcTemplateTest test = new NamedParameterJdbcTemplateTest();
    List<String> userids= new ArrayList<String>();
    userids.add("user0");
    userids.add("user1");
    userids.add("user2");
    userids.add("user3");

//		test.queryListByMap(userids);
//		test.queryListByMapSqlParameterSource(userids);
//		test.querySingle(userids);
//		test.querySingleByBeanPropertySqlParameterSource();
    test.queryUsersByMapSqlParameterSource(userids);
  }
}
```
JSON配置

```
<mvc:message-converters register-defaults="true">
            <bean
                class="org.springframework.http.converter.ByteArrayHttpMessageConverter" />
            <!-- 将StringHttpMessageConverter的默认编码设为UTF-8 -->
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg value="UTF-8" />
            </bean>

            <!-- 将Jackson2HttpMessageConverter的默认格式化输出设为true -->
            <bean
                class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            </bean>

        </mvc:message-converters>
        <mvc:argument-resolvers>
            <bean
                class="org.springframework.data.web.PageableHandlerMethodArgumentResolver">
            </bean>
             <bean
                class="org.springframework.data.web.SortHandlerMethodArgumentResolver">
            </bean>
        </mvc:argument-resolvers>
    </mvc:annotation-driven>

    <bean class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
        <property name="contentNegotiationManager">
            <bean class="org.springframework.web.accept.ContentNegotiationManagerFactoryBean">
                <property name="ignoreAcceptHeader" value="true"/>
                <property name="defaultContentType" value="application/json"/>
                <property name="mediaTypes">
                    <map>
                        <entry key="html" value="text/html"/>
                        <entry key="json" value="application/json"/>
                    </map>
                </property>
            </bean>
        </property>
        <property name="defaultViews">
            <list>
                <bean class="org.springframework.web.servlet.view.json.MappingJackson2JsonView"/>
            </list>
        </property>
    </bean>
<mvc:annotation-driven>
		<mvc:message-converters register-defaults="true">
			<bean id="fastJsonHttpMessageConverter"
				class="com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter">
				<property name="charset" value="UTF-8" />
				<property name="supportedMediaTypes">
					<list>
						<!-- 这里顺序不能反，一定先写text/html,不然ie下出现下载提示 -->
						<value>text/html;charset=UTF-8</value>
						<value>text/json;charset=UTF-8</value>
						<value>application/json;charset=UTF-8</value>
					</list>
				</property>
			</bean>
		</mvc:message-converters>
	</mvc:annotation-driven>
```
### 在servlet中用spring @Autowire 注入

```
public class MyServlet extends HttpServlet {
  @Autowired
  private MyService myService;
  public void init(ServletConfig config) {
    super.init(config);
    SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
      config.getServletContext());
  }
}
// 或者
public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext servletContext = this.getServletContext();
        WebApplicationContext wac = null;
        wac = WebApplicationContextUtils
                .getRequiredWebApplicationContext(servletContext);
        this.setUserServiceService((UserServiceService) wac
                .getBean("userServiceService"));// Spring 配置 中的 bean id
    }
```
SpringMVC异常配置

```
<!-- 全局异常配置 start -->
	<bean id="exceptionResolver" class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
		<property name="exceptionMappings">
			<props>
				<prop key="java.lang.Exception">errors/error</prop>
				<prop key="java.lang.Throwable">errors/err</prop>
			</props>
		</property>
		<property name="statusCodes">
			<props>
				<prop key="errors/error">500</prop>
				<prop key="errors/404">404</prop>
			</props>
		</property>
		<!-- 设置日志输出名字，不定义则默认不输出警告等错误日志信息。实际上WARN级别是写死的 -->
		<property name="warnLogCategory" value="WARN"></property>
		<!-- 默认错误页面，当找不到上面mappings中指定的异常对应视图时，使用本默认配置 -->
		<property name="defaultErrorView" value="errors/error"></property>
		<!-- 默认HTTP状态码 -->
		<property name="defaultStatusCode" value="500"></property>
	</bean>
	<!-- 全局异常配置 end -->
```
### spring mvc rest中如何处理点号

```
@Controller
@RequestMapping("/site")
public class SiteController {

	@RequestMapping(value = "/{domain:.+}", method = RequestMethod.GET)
	public String printWelcome(@PathVariable("domain") String domain,
		ModelMap model) {

		model.addAttribute("domain", domain);
		return "domain";

	}
```
### controller 返回的 view 是不固定
如果在 controller 返回的 view 是不固定的，如："redirect:form.html?entityId=" + entityId，由于 entityId 的值会存在 N 个，那么会导致产生 N 个 ViewName 被缓存起来。
由于内存大小是有限的，所以当 N 大到一定数量时，会产生内存溢出。为了解决这个问题，spring 提供了其他的方案：http://static.springsource.org/spring-framework/docs/3.2.0.BUILD-SNAPSHOT/reference/htmlsingle/#mvc-ann-redirect-attributes

```
@RequestMapping(method = RequestMethod.POST)
public String onPost(RedirectAttributes redirectAttrs) {
    // ...
    redirectAttrs.addAttribute("entityId", entityId)
    return "redirect:form.html?entityId={entityId}";
}
```
如上，使用占位符后， viewName 的数量被限制为了1个。

### 如何让spring mvc web应用启动时就执行特定处理

Spring-MVC的应用中，要实现类似的功能，主要是通过实现下面这些接口（任选一，至少一个即可）

#### 一、ApplicationContextAware接口

```
package org.springframework.context;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.Aware;
import org.springframework.context.ApplicationContext;

public interface ApplicationContextAware extends Aware {
    void setApplicationContext(ApplicationContext var1) throws BeansException;
}
```

#### 二、ServletContextAware 接口

```
package org.springframework.web.context;

import javax.servlet.ServletContext;
import org.springframework.beans.factory.Aware;

public interface ServletContextAware extends Aware {
    void setServletContext(ServletContext var1);
}
```

#### 三、InitializingBean 接口

```
package org.springframework.beans.factory;

public interface InitializingBean {
    void afterPropertiesSet() throws Exception;
}
```

#### 四、ApplicationListener<ApplicationEvent> 接口

```
package org.springframework.context;

import java.util.EventListener;
import org.springframework.context.ApplicationEvent;

public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {
    void onApplicationEvent(E var1);
}
```

####  示例程序：

```
package test.web.listener;

import org.apache.logging.log4j.*;
import org.springframework.beans.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.*;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;
import javax.servlet.ServletContext;

@Component
public class StartupListener implements ApplicationContextAware, ServletContextAware,
        InitializingBean, ApplicationListener<ContextRefreshedEvent> {

    protected Logger logger = LogManager.getLogger();

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        logger.info("1 => StartupListener.setApplicationContext");
    }

    @Override
    public void setServletContext(ServletContext context) {
        logger.info("2 => StartupListener.setServletContext");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("3 => StartupListener.afterPropertiesSet");
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent evt) {
        logger.info("4.1 => MyApplicationListener.onApplicationEvent");
        if (evt.getApplicationContext().getParent() == null) {
            logger.info("4.2 => MyApplicationListener.onApplicationEvent");
        }
    }

}
```

运行时，输出的顺序如下：

1 => StartupListener.setApplicationContext

2 => StartupListener.setServletContext

3 => StartupListener.afterPropertiesSet

4.1 => MyApplicationListener.onApplicationEvent

4.2 => MyApplicationListener.onApplicationEvent

4.1 => MyApplicationListener.onApplicationEvent

注意：onApplicationEvent方法会触发多次，初始化这种事情，越早越好，建议在setApplicationContext方法中处理。

### 非spring在获取bean

```java
ApplicationContext ctx = new ClassPathXmlApplicationContext("\\spring\\applicationContext.xml");
JmsTemplate jmsTemplate=(JmsTemplate)ctx.getBean("jmsTemplate");
//如果是junit种可以如下：
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml" })
public class TestUtil {

	@Autowired
	SmsChannelServiceImpl smsChannelServiceimpl;
```

#### 