


#### prepareStatement
就算你用的是5.7，就算用最新的mysql-connector-j，用Connection.prepareStatement执行sql时，默认还是会把字段值和sql拼成一条完整的字符串发给MySQL Server的……除非显式设置了useServerPrepStmts为true。

#### Java高效复制文件的两种方法

```
private static void copyFileUsingFileChannels(File source, File dest)
        throws IOException {
    FileChannel inputChannel = null;
    FileChannel outputChannel = null;
    try {
        inputChannel = new FileInputStream(source).getChannel();
        outputChannel = new FileOutputStream(dest).getChannel();
        outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
    } finally {
        inputChannel.close();
        outputChannel.close();
    }
}
private static void copyFileUsingJava7Files(File source, File dest)
        throws IOException {
    Files.copy(source.toPath(), dest.toPath());
}
```
#### Java 取得文件的MIME type

```
String pathname = "/tmp/1";
File file = new File(pathname);
InputStream is = new BufferedInputStream(new FileInputStream(file));
String mimeType = URLConnection.guessContentTypeFromStream(is);
System.out.println(mimeType);
```
#### 解析单个节点的XML

```
String ymret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><response><error>0</error><message></message></response>";
ymret = ymret.trim();
SAXReader sb = new SAXReader();
Document doc = sb.read(new StringReader(ymret));
Element root = doc.getRootElement();
System.out.println(root.selectSingleNode("//response//error").getText());
```

#### Java获取系统信息wmic
有时候实际应用是要获取系统的相关信息，比如内存大小、cpu使用率等等，java在这方面不太擅长，虽然有开源的sigar使用。之前有个项目要用到获取cpu序列号、硬盘序列号。开始是使用网上那种调用vbs文件，不过很多电脑上都获取不到数据。最后是直接调用dll，后来发现window原来有个强大的功能:wmic命令(Windows Management Instrumentation Command-line)。可以获取到系统的n多信息，简直强大到无法想象。

```
 /**
   * 获取cpu序列号
   *
   * @return
   */
  public static String getCpuSerial(){
    return exec("wmic", "cpu", "get", "ProcessorId");
  }
  /**
   * 获取bios序列号
   *
   * @return
   */
  public static String getBiosSerial(){
    return exec("wmic", "bios", "get", "SerialNumber");
  }
  /**
   * 获取主板序列号
   *
   * @return
   */
  public static String getMotherboardSerial(){
    return exec("wmic", "baseboard", "get", "SerialNumber");
  }
  /**
   * 取硬盘物理序列号
   *
   * @return
   */
  public static String getIdeSerial(){
    return exec("wmic", "diskdrive", "get", "SerialNumber");
  }
  private static String exec(String... cmd){
    String result = null;
    Scanner sc = null;
    try{
      Process process = Runtime.getRuntime().exec(cmd);
      IOUtils.closeQuietly(process.getOutputStream());
      sc = new Scanner(process.getInputStream());
      sc.next();
      result = sc.next();
    }catch(IOException e){
      System.err.println("执行wmic错误!" + e.getMessage());
    }finally{
      IOUtils.closeQuietly(sc);
    }
    return result;
  }
```
#### 判断中文
```
java
public static boolean isChinese(char c) {
    Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
    if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
            || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
            || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
            || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
            || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
            || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
        return true;
    }
    return false;
}
```
#### 清除行号，减少编译后体积
```
<plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.5.1</version>
          <configuration>
            <source>1.8</source>
            <target>1.8</target>
            <debug>true</debug>
            <debuglevel>none</debuglevel>
          </configuration>
        </plugin>
#javac -g:none HelloWorld.java
```