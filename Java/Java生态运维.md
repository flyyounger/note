# Java生态运维
### Kafka基本操作
```bash
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic testTopic
bin/kafka-topics.sh --list --zookeeper localhost:2181
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic testTopic
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testTopic --from-beginning
```
### RocketMQ的几个常用方法
```bash
mqadmin printMsg -n 192.168.20.31:9876 -t T-BID-STATUS-NOTIFY
mqadmin topicStatus -n 192.168.20.31:9876 -t T-BID-STATUS-NOTIFY
```
### filebeat 关键配置
```yaml
#multiline.pattern: '^([0-9]{4}|[0-9]{2})-[0-9]{2}'
processors:
  - drop_fields: 
      fields: ["apache","apache2","auditd","certificate","client","cloud","container","destination","event","input","input_type", "offset","haproxy","iis","iptables","mysql","netflow","mongodb","nginx","observer","organization","os","osquery","postgresql","process","redis","related","santa","server","service","source","stream","suricata","syslog","system","tags","traefik","url","user","user_agent","zeek"]
setup.template.name: "ngerr"
setup.template.pattern: "ngerr-*"
output.elasticsearch.index: "ngerr-%{+yyyy.MM.dd}"
setup.ilm.enabled: false      
```
#### 生成HTTPS本地证书
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout https.key -out https.crt 
```

#### 启用远程JMX

```
-Dcom.sun.management.jmxremote.authenticate=false
-Dcom.sun.management.jmxremote.ssl=false
-Dcom.sun.management.jmxremote.port=7777
; 如果是Linux，下面这一行必须添加，以指定目标JVM的地址
-Djava.rmi.server.hostname=192.168.0.232
-Dcom.sun.management.jmxremote
-XX:+UnlockCommercialFeatures
-XX:+FlightRecorder
```

注意：JMX 不光开启了7777 端口，还开启了另外两个端口，而在获取监控数据时会使用到其中一个，但不幸的是这两个端口是随机监听的，这就导致我们暂时没法通过固定的防火墙规则来开放该端口。这个时候我们需要使用另外一个替代方式来使该监听端口固定下来——使用 Tomcat提供的额外组件catalina-jmx-remote.jar。

对rmi的工作方式进行了了解，发现jmx如果采用rmi作为传输协议的话，客户端需要进行两个连接，如：JMXConnectorServer的JMXServiceURL为如下形式：service:jmx:rmi://localhost:5000/jndi/rmi://localhost:6000/jmxrmi
则首先客户端连接到rmiregistry上得到真实服务器的stub（如rmi://localhost:6000/rmxrmi），然后客户端再根据该stub连接到真实的服务器上（如rmi://localhost:5000）。如果jmx服务端省略了蓝色部分的标注，默认的通信端口是随机产生的。针对目前现网的情况，如果现网只开放了一个注册端口，如果要通过防火墙进行通信还需要开放一个通信端口，另外jmx服务端也需要固定通信端口。

通过在防火墙上开放两个端口一般能够解决穿越防火墙的问题，但针对防火墙做了内外网IP映射情况，还需要增加额外的配置，使服务端带回的stub为外网IP而非内网IP ，System.setProperty("java.rmi.server.hostname","外网IP");此解决方案没有验证，具体可参见http://blog.csdn.net/ktyl2000/article/details/4485896
为避开rmi穿越防火墙问题，通常采用jmx消息协议(jmxmp)来代替rmi，使用jmxmp协议较为简单，并且服务端不需要进行端口注册过程。


一个用于参考的tomcat配置：

```
@echo off
echo *******************************************************************************
echo Portable Tomcat 7 by Alex Wang
echo.
pushd "%~dp0"
for /f "tokens=1-3 delims=/ " %%a in ('date /t') do (set md=%%a-%%b-%%c)
for /f "tokens=1-3 delims=/:." %%a in ("%TIME%") do (set mt=%%a%%b%%c)
set CUR_TIME=%md%-%mt%

set "JAVA_HOME="
set "CATALINA_HOME=%CD%"
set "JRE_HOME=%CATALINA_HOME%\jre"
set "LOG_DIR=%CATALINA_HOME%\logs"
rem 请根据服务器硬件配置、系统的特性填写下面一行
rem 响应时间优先配置示例：-XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:ParallelGCThreads=8 -XX:MaxGCPauseMillis=500
rem 吞吐量优先的配置示例：-XX:+UseParallelGC -XX:+UseParallelOldGC -XX:ParallelGCThreads=8 -Xmn1280M
set JAVA_OPTS=-server -Xms2G -Xmx2G -Xss128K -XX:MaxPermSize=256M
rem 启用错误记录
set JAVA_OPTS=%JAVA_OPTS% -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=%LOG_DIR%\ -XX:ErrorFile=%LOG_DIR%\hs_err_pid%%p.log
rem 启用JMX远程管理和黑匣子
set JAVA_OPTS=%JAVA_OPTS% -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.port=7777 -Dcom.sun.management.jmxremote -XX:+UnlockCommercialFeatures -XX:+FlightRecorder
rem 启用黑匣子默认记录行为
set JAVA_OPTS=%JAVA_OPTS% -XX:FlightRecorderOptions=defaultrecording=true,disk=true,dumponexit=true,dumponexitpath=%LOG_DIR%\jfr\dump-on-exit-%CUR_TIME%.jfr,maxage=3600s,settings=%JRE_HOME%\lib\jfr\profile.jfc
rem 记录GC详细日志
set JAVA_OPTS=%JAVA_OPTS% -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:%LOG_DIR%\gc.log
rem 启用远程调试
set JAVA_OPTS=%JAVA_OPTS% -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8000

echo Setting CATALINA_HOME %CATALINA_HOME%
echo Setting JRE_HOME %JRE_HOME%
echo Setting JAVA_OPTS %JAVA_OPTS%
echo.
echo Launching Tomcat 7 startup.bat...
echo *******************************************************************************
echo.
call bin\startup.bat
popd
```

修改server.xml，找到connector 8080这一段xml配置，追加一个属性address="127.0.0.1"，重启tomcat即可不让8080端口对外开放，以便做proxy