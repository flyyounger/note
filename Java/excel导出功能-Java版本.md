
```java
package com.weidai.bwcrm.util;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @Description: 使用XML导出excel，不完全支持多sheet
 * @author 陈文 waitfox@qq.com
 * @date 2016年12月7日 下午2:33:46
 * @version V1.0
 */
public class XMLExcel {
	private String sheetName = "sheet1";
	public StringBuilder generatorHead() {
		StringBuilder head = new StringBuilder();
		head.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
		head.append("<?mso-application progid=\"Excel.Sheet\"?>");
		head.append(
				"<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"   xmlns:o=\"urn:schemas-microsoft-com:office:office\"  xmlns:x=\"urn:schemas-microsoft-com:office:excel\"  xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"  xmlns:html=\"http://www.w3.org/TR/REC-html40\"> ");
		head.append("<DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
		head.append("<Created>").append(new Date()).append("</Created>");
		head.append("</DocumentProperties>\r\n");
		return head;
	}
	public StringBuilder generatorStyle(String styleId, String FontName, String FontColor) {
		StringBuilder style = new StringBuilder();
		style.append("<Style ss:ID=\"").append(styleId).append("\">");
		style.append("<Font ss:FontName=\"").append(FontName).append("\" x:CharSet=\"134\" ss:Size=\"12\" ss:Color=\"")
				.append(FontColor).append("\"  ss:Bold=\"1\"/>");
		style.append("</Style>");
		return style;
	}
	public StringBuilder addWorkSheet(String[] title, List<List<Object>> data) {
		StringBuilder sheet = new StringBuilder();
		sheet.append(" <Worksheet ss:Name=\"").append(sheetName).append("\">\r\n ");
		sheet.append("<Table ss:ExpandedRowCount=\"").append(data.size()+1).append("\">\r\n");
		sheet.append("<Row>\r\n");
		for (int i = 0; i < title.length; i++) {// 添加标题
			sheet.append("<Cell><Data ss:Type=\"String\">").append(title[i]).append("</Data></Cell>\r\n ");
		}
		sheet.append("</Row>\r\n");
		if (data != null) {
			for (List<Object> o : data) {// 添加
				sheet.append("<Row>\r\n");
				for (int i = 0; i < o.size(); i++) {
					sheet.append("<Cell><Data ss:Type=\"").append(getType(o.get(i))).append("\">")
							.append(addValue(o.get(i))).append("</Data></Cell>\r\n ");
				}
				sheet.append("</Row>\r\n");
			}
		}
		sheet.append("</Table>\r\n");
		sheet.append("</Worksheet>\r\n");
		return sheet;
	}
	public StringBuilder generatorFooter() {
		StringBuilder foot = new StringBuilder();
		foot.append("</Workbook>");
		return foot;
	}
	public String buildExcel(String[] title, List<List<Object>> data) {
		StringBuilder excel = new StringBuilder(512);
		excel.append(generatorHead());
		excel.append(addWorkSheet(title, data));
		excel.append(generatorFooter());
		return excel.toString();
	}
	/**
	 * 处理数据类型，暂时只处理整数，其他均视作字符串
	 *
	 * @param o
	 * @return
	 */
	public String getType(Object o) {
		if (Integer.class.isInstance(o)) {
			return "Number";
		} else {
			return "String";
		}
	}
	public String addValue(Object o) {
		if (String.class.isInstance(o) || o instanceof String) {
			if (o == null || o.equals("")) {
				return "";
			} else {
				return o.toString();
			}
		} else {
			return o==null?"":o.toString();
		}
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public static void main(String[] args) {
		String[] title = { "ID", "姓名", "年龄" };
		List<List<Object>> data = new ArrayList<>();
		List<Object> row1 = new ArrayList<>();
		row1.add(1);
		row1.add("张三");
		row1.add("24");
		data.add(row1);
		List<Object> row2 = new ArrayList<>();
		row2.add(2);
		row2.add("张三2");
		row2.add("25");
		data.add(row2);
		XMLExcel e = new XMLExcel();
		e.setSheetName("进件报表");
		String excel = e.buildExcel(title, data);
		System.out.println(excel);
	}
}

```
