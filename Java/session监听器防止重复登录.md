先在登录时加上：

```
session.setAttribute("loginUser", user);

// 把用户名放入在线列表
List onlineUserList = (List) request.getServletContext().getAttribute("onlineUserList");
if (onlineUserList == null) {
onlineUserList = new ArrayList();
request.getServletContext().setAttribute("onlineUserList", onlineUserList);
		    }

System.out.println("session中加入某个用户+"+user.getUserName());
System.out.println("session中是否已存在此户："+onlineUserList.contains(user.getUserName()));
onlineUserList.add(user.getUserName());
```
然后加个listener

```
package filter;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import com.xxd.model.user.User;
/**
 * @Description: Session监听器
 * @author 猪肉有毒 waitfox@qq.com
 * @date 2015-2-4 下午1:39:05
 * @version V1.0 我只为你回眸一笑，即使不够倾国倾城，我只为你付出此生，换来     生再次相守
 */
public class SessionManage implements HttpSessionListener {
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		System.out.println(session);
	}
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		System.out.println(session + "的元素：");
		Enumeration<String> s = session.getAttributeNames();
		while (s.hasMoreElements()) {
			System.out.println(session.getAttribute(s.nextElement()));
			// 这里的s.nextElement()就对应了每一个键名 通过他取值就可以了
		}
		ServletContext application = session.getServletContext();
		// 取得登录的用户名
		User user = (User) session.getAttribute("loginUser");
		String userName = user.getUserName();
		// 从在线列表中删除用户名
		List onlineUserList = (List) application.getAttribute("onlineUserList");
		onlineUserList.remove(userName);
		System.out.println(userName + "已经退出！");
	}
}
```
web.xml加上：

```
<listener>
<listener-class>filter.SessionManage</listener-class>
</listener>
```
