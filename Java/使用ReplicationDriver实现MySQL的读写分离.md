JavaEE应用中采用MySQL的Master-Slave模式(关于配置可见之前博文)还是很常见的，以此实现数据库读写分离，降低数据库压力。

常见实现数据库读写分离的方案是在程序中手动指定多个数据源，比较麻烦。或者使用中间件(这个没用过)。这里介绍一种更好的方法。

针对Master/Slave，MySQL自带了一个ReplicationDriver的jdbc driver。详见文档[http://dev.mysql.com/doc/connector-j/en/connector-j-master-slave-replication-connection.html](http://dev.mysql.com/doc/connector-j/en/connector-j-master-slave-replication-connection.html)里面写了它的用法及sample。它是通过Connection事务的readonly属性实现读和写操作的分离。


不过需要注意的是文档中提到：
> ReplicationDriver does not currently work with java.sql.DriverManager-based connection creation unless it is the only MySQL JDBC driver registered with the DriverManager 。

也就是说，DriverManager是一个单例模式，一个DriverManager只能注册一个ReplicationDriver驱动，即ReplicationDriver和Driver两个类不能同时使用。

实际使用中需注意ReplicationDriver的url协议头：jdbc:mysql:replication://master,slave1,slave2。这个是mysql自己扩展的jdbc url协议。另外必须指定Connection的readonly属性。当然如果用spring就很方便了，配置一个事务aop即可。


```
<tx:advice id="txAdvice" transaction-manager="txManager">
   <tx:attributes>
      <tx:method name="save*" propagation="REQUIRED" />
      <tx:method name="insert*" propagation="REQUIRED" />
      <tx:method name="update*" propagation="REQUIRED" />
      <tx:method name="merge*" propagation="REQUIRED" />
      <tx:method name="remove*" propagation="REQUIRED" />

      <tx:method name="query*" read-only="true" />
      <tx:method name="load*" read-only="true" />
      <tx:method name="count*" read-only="true" />
      <tx:method name="list*" read-only="true" />

      <tx:method name="*" propagation="REQUIRED" />
    </tx:attributes>
</tx:advice>
```
如果使用数据库连接池，配置以下代码：简单而又强大，无代码入侵。当然如果使用其它数据库就没办法使用了，不过可以参照ReplicationDriver源码自己实现

```
<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">
    <property name="driverClassName" value="com.mysql.jdbc.ReplicationDriver" />
    <property name="url" value="jdbc:mysql:replication://master,slave1,slave2" />
    <property name="username" value="root" />
    <property name="password" value="12345" />
</bean>
```
