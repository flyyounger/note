查看类的一些信息，如字节码的版本号、常量池等

```
javap -verbose classname
```
查看进程的gc情况

```
jstat -gcutil [pid] (显示总体情况)
jstat -gc [pid] 1000 10（每隔1秒刷新一次 一共10次）
```
查看jvm内存使用状况

```
jmap -heap [pid]
```
查看jvm内存存活的对象：

```
jcmd [pid] GC.class_histogram
jmap -histo:live [pid]
```
把heap里所有对象都dump下来，无论对象是死是活

```
jmap -dump:format=b,file=xxx.hprof [pid]
```
先做一次full GC，再dump，只包含仍然存活的对象信息：

```
jcmd [PID] GC.heap_dump [FILENAME]
jmap -dump:format=b,live,file=xxx.hprof [pid]
```
线程dump

```
jstack [pid] #-m参数可以打印出native栈的信息
jcmd Thread.print
kill -3 [pid]
```
查看目前jvm启动的参数

```
jinfo -flags [pid] #有效参数
jcmd [pid] VM.flags #所有参数
```
查看对应参数的值

```
jinfo -flag [flagName] [pid]
```
启用/禁止某个参数

```
jinfo -flag [+/-][flagName] [pid]
```
设置某个参数

```
jinfo -flag [flagName=value] [pid]
```
查看所有可以设置的参数以及其默认值
```
java -XX:+PrintFlagsInitial
```

查看JVM启动和已运行时间
```
ps -eo pid,lstart,etime,cmd | grep java
```

JVM配置示例

```
-server #64位机器下默认
-Xms6000M #最小堆大小
-Xmx6000M #最大堆大小
-Xmn500M #新生代大小
-Xss256K #栈大小
-XX:PermSize=500M (JDK7)
-XX:MaxPermSize=500M (JDK7)
-XX:MetaspaceSize=128m  （JDK8）
-XX:MaxMetaspaceSize=512m（JDK8）
-XX:SurvivorRatio=65536
-XX:MaxTenuringThreshold=0 #晋升到老年代需要的存活次数,设置为0时，survivor区失去作用，一次minor gc，eden中存活的对象就会进入老年代，默认是15，使用CMS时默认是4
-Xnoclassgc #不做类的gc
#-XX:+PrintCompilation #输出jit编译情况，慎用
-XX:+TieredCompilation #启用多层编译，jd8默认开启
-XX:CICompilerCount=4 #编译器数目增加
-XX:-UseBiasedLocking #取消偏向锁
-XX:AutoBoxCacheMax=20000 #自动装箱的缓存数量，如int默认缓存为-128~127
-Djava.security.egd=file:/dev/./urandom #替代默认的/dev/random阻塞生成因子
-XX:+AlwaysPreTouch #启动时访问并置零内存页面，大堆时效果比较好
-XX:-UseCounterDecay #禁止JIT调用计数器衰减。默认情况下，每次GC时会对调用计数器进行砍半的操作，导致有些方法一直是个温热，可能永远都达不到C2编译的1万次的阀值。
-XX:ParallelRefProcEnabled=true # 默认为false，并行的处理Reference对象，如WeakReference
-XX:+DisableExplicitGC #此参数会影响使用堆外内存，会造成oom，如果使用NIO,请慎重开启
#-XX:+UseParNewGC #此参数其实在设置了cms后默认会启用，可以不用设置
-XX:+UseConcMarkSweepGC #使用cms垃圾回收器
#-XX:+UseCMSCompactAtFullCollection #是否在fullgc是做一次压缩以整理碎片，默认启用
-XX:CMSFullGCsBeforeCompaction=0 #full gc触发压缩的次数
#-XX:+CMSClassUnloadingEnabled #如果类加载不频繁，也没有大量使用String.intern方法，不建议打开此参数，况且jdk7后string pool已经移动到了堆中。开启此项的话，即使设置了Xnoclassgc也会进行class的gc, 但是某种情况下会造成bug：https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwjR16Wf6MHQAhWLrVQKHfLdCe4QFgg8MAQ&url=https%3A%2F%2Fblogs.oracle.com%2Fpoonam%2Fentry%2Fjvm_hang_with_cms_collector&usg=AFQjCNFNtkw6jHM-uyz-Wjri3LtAVXWJ8g&sig2=BFxSfHc-AIek18fEhY07mg。
#-XX:+CMSParallelRemarkEnabled #并行标记, 默认开启, 可以不用设置
#-XX:+CMSScavengeBeforeRemark #强制remark之前开始一次minor gc，减少remark的暂停时间，但是在remark之后也将立即开始又一次minor gc
-XX:CMSInitiatingOccupancyFraction=90 #触发full gc的内存使用百分比
-XX:+PrintClassHistogram #打印类统计信息
-XX:+PrintHeapAtGC #打印gc前后的heap信息
-XX:+PrintGCDetails #以下都是为了记录gc日志
-XX:+PrintGCDateStamps
-XX:+PrintGCApplicationStoppedTime #打印清晰的GC停顿时间外，还可以打印其他的停顿时间，比如取消偏向锁，class 被agent redefine，code deoptimization等等
-XX:+PrintTenuringDistribution #打印晋升到老年代的年龄自动调整的情况(并行垃圾回收器启用UseAdaptiveSizePolicy参数的情况下以及其他垃圾回收期也会动态调整，从最开始的MaxTenuringThreshold变成占用当前堆50%的age)
#-XX:+UseAdaptiveSizePolicy # 此参数在并行回收器时是默认开启的会根据应用运行状况做自我调整，如MaxTenuringThreshold、survivor区大小等，其他情况下最好不要开启
#-XX:StringTableSize #字符串常量池表大小(hashtable的buckets的数目)，java 6u30之前无法修改固定为1009，后面的版本默认为60013，可以通过此参数设置
-XX:GCTimeLimit=98 #gc占用时间超过多少抛出OutOfMemoryError
-XX:GCHeapFreeLimit=2 #gc回收后小于百分之多少抛出OutOfMemoryError
-Xloggc:/home/logs/gc.log
```
#### 查看JVM在忙什么
1.查找jvm进程ID: jps -lv 或者 ps aux | grep java

2.根据pid，查找占用cpu较高的线程：ps -mp pid -o THREAD,tid,time 如图所示：找到占用cpu最高的tid (可以使用sort命令排序：sort -k 3 -r -n)

3.将tid转换为16进制的数字：printf “%x\n” tid

4.使用jstack命令，查询线程信息，从而定位到具体线程和代码：jstack pid | grep 7ccd -A 30
脚本如下

```
#!/bin/bash
#
# 当JVM占用CPU特别高时，查看CPU正在做什么
# 可输入两个参数：1、pid Java进程ID，必须参数  2、打印线程ID上下文行数，可选参数，默认打印10行
#

pid=$1

if test -z $pid
then
 echo "pid can not be null!"
 exit
else
 echo "checking pid($pid)"
fi

if test -z "$(jps -l | cut -d '' -f 1 | grep $pid)"
then
 echo "process of $pid is not exists"
 exit
fi

lineNum=$2
if test -z $lineNum
then
    $lineNum=10
fi

jstack $pid >> "$pid".bak

ps -mp $pid -o THREAD,tid,time | sort -k2r | awk '{if ($1 !="USER" && $2 != "0.0" && $8 !="-") print $8;}' | xargs printf "%x\n" >> "$pid".tmp

tidArray="$( cat $pid.tmp)"

for tid in $tidArray
do
    echo "******************************************************************* ThreadId=$tid **************************************************************************"
    cat "$pid".bak | grep $tid -A $lineNum
done

rm -rf $pid.bak
rm -rf $pid.tmp
```
#### jvm运行期打印汇编信息

```
java -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly YourMainClass
```
debug与fastdebug build就不用带。
参考[http://rednaxelafx.iteye.com/blog/729214：](http://rednaxelafx.iteye.com/blog/729214)

#### JVM用到的优化技术
![JVM](_v_images/20200214114554224_4134.png)