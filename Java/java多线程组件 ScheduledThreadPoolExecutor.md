在实际的情况中可能会需要有的线程是定期执行的,有的线程是只执行一次,如果通过Timer去触发,会涉及线程的切换以及线程安全问题,这对这种情况从jdk1.5开始添加了ScheduledThreadPoolExecutor 组件,这种组件主要就是用来解决前面出现的问题,通过这个组件即可以定期的去执行一个任务,也可以只执行一次,也可以把这两种情况一起使用。

Timer和TimerTask存在一些缺陷：

1：Timer只创建了一个线程。当你的任务执行的时间超过设置的延时时间将会产生一些问题。

2：Timer创建的线程没有处理异常，因此一旦抛出非受检异常，该线程会立即终止。

JDK 5.0以后推荐使用ScheduledThreadPoolExecutor。该类属于Executor Framework，它除了能处理异常外，还可以创建多个线程解决上面的问题。
```
public class TaskTest {
   static ScheduledThreadPoolExecutor stpe = null;
   static int index;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //构造一个ScheduledThreadPoolExecutor对象，并且设置它的容量为5个
        stpe = new ScheduledThreadPoolExecutor(5);
        MyTask task = new MyTask();
//隔2秒后开始执行任务，并且在上一次任务开始后隔一秒再执行一次；
//        stpe.scheduleWithFixedDelay(task, 2, 1, TimeUnit.SECONDS);
        //隔6秒后执行一次，但只会执行一次。
        stpe.schedule(task, 6, TimeUnit.SECONDS);
    }
    private static String getTimes() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E");
        Date date = new Date();
        date.setTime(System.currentTimeMillis());
        return format.format(date);
    }
    private static class MyTask implements Runnable {
        @Override
        public void run() {
            index++;
            System.out.println("2= " + getTimes()+" "  +index);
            if(index >=10){
                stpe.shutdown();
                if(stpe.isShutdown()){
                    System.out.println("停止了？？？？");
                }
            }
        }
    }
}
```
另外一个例子：

```
public class TaskTest {
   static ScheduledThreadPoolExecutor stpe = null;
   static int index;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //构造一个ScheduledThreadPoolExecutor对象，并且设置它的容量为5个
        stpe = new ScheduledThreadPoolExecutor(5);
        MyTask task = new MyTask();
//隔2秒后开始执行任务，并且在上一次任务开始后隔一秒再执行一次；
//        stpe.scheduleWithFixedDelay(task, 2, 1, TimeUnit.SECONDS);
        //隔6秒后执行一次，但只会执行一次。
        stpe.schedule(task, 6, TimeUnit.SECONDS);
    }
    private static String getTimes() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E");
        Date date = new Date();
        date.setTime(System.currentTimeMillis());
        return format.format(date);
    }
    private static class MyTask implements Runnable {
        @Override
        public void run() {
            index++;
            System.out.println("2= " + getTimes()+" "  +index);
            if(index >=10){
                stpe.shutdown();
                if(stpe.isShutdown()){
                    System.out.println("停止了？？？？");
                }
            }
        }
    }
}
```
在使用这两个类的时候，要特别注意异常处理问题。以下是一个模拟程序：

```
public class ScheduledThreadPoolExecutorTest {
    public static void main(String[] args) {
        ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1);
        BusinessTask task = new BusinessTask();
        //1秒后开始执行任务，以后每隔2秒执行一次
        executorService.scheduleWithFixedDelay(task, 1000, 2000,TimeUnit.MILLISECONDS);
    }
    private static class BusinessTask implements Runnable{
        @Override
        public void run() {
            System.out.println("任务开始...");
            //doBusiness();
            System.out.println("任务结束...");
        }
    }
}
```
程序输出结果跟相像中一样：


可是执行了一段时间后，发现定时任务不再执行了，去查看后台打印的日志，原来在doBusiness()方法中抛出了异常。为什么doBusiness()抛出异常就会中止定时任务的执行呢？去查看JDK的ScheduledThreadPoolExecutor.scheduleWithFixedDelay(Runnable command,long initialDelay,long delay,TimeUnit unit)的方法说明：
> Creates and executes a periodic action that becomes enabled first after the given initial delay, and subsequently with the given delay between the termination of one execution and the commencement of the next. If any execution of the task encounters an exception, subsequent executions are suppressed. Otherwise, the task will only terminate via cancellation or termination of the executor.

这段话翻译成中文是：

> 创建并执行一个在给定初始延迟后首次启用的定期操作，随后，在每一次执行终止和下一次执行开始之间都存在给定的延迟。如果任务的任一执行遇到异常，就会取消后续执行。否则，只能通过执行程序的取消或终止方法来终止该任务。

看到这里，我们明白了原因，这样就需要把doBusiness()方法的所有可能异常捕获，才能保证定时任务继续执行。把代码改成这样：

```
public class ScheduledThreadPoolExecutorTest {
    public static void main(String[] args) {
        ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1);
        BusinessTask task = new BusinessTask();
        //1秒后开始执行任务，以后每隔2秒执行一次
        executorService.scheduleWithFixedDelay(task, 1000, 2000,TimeUnit.MILLISECONDS);
    }
    private static class BusinessTask implements Runnable{
        @Override
        public void run() {
            //捕获所有的异常，保证定时任务能够继续执行
            try{
                System.out.println("任务开始...");
                //doBusiness();
                System.out.println("任务结束...");
            }catch (Throwable e) {
                // donothing
            }
        }
    }
}
```
其实，在JAVAWEB开发中，执行定时任务有一个更好的选择:Quartz

这个开源库提供了丰富的作业调度集，有兴趣的同学可以到官方网站中学习一下其用法。
