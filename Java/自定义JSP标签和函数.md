## 自定义JSP标签
**JSP自定义标签的处理类**

```
public class StringTag extends TagSupport{
    private static final long serialVersionUID = 1L;

    private String content; //字符串内容
    private int count; //字符串个数
    private String append;

    public int doStartTag(){
        try {
            pageContext.getOut().print(substring(content,count,append));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

    public int doEndTag(){
        //执行后续的jsp页面内容
        return EVAL_PAGE;
    }

    public static String substring(String text, int length, String append) {
        if (StringUtils.isBlank(text) || text.length() < length) {
            return text;
        }
        int num = 0, i = 0, len = text.length();
        StringBuilder sb = new StringBuilder();
        for (; i < len; i++) {
            char c = text.charAt(i);
            if (c > 127) {
                num += 2;
            } else {
                num++;
            }
            if (num <= length * 2) {
                sb.append(c);
            }
            if (num >= length * 2) {
                break;
            }
        }
        if (i + 1 < len && StringUtils.isNotBlank(append)) {
            if (text.charAt(i) > 127) {
                sb.setLength(sb.length() - 1);
            } else {
                sb.setLength(sb.length() - 2);
            }
            sb.append(append);
        }
        return sb.toString();
    }

    //省去get,set方法

}
```
utils.tld

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE taglib PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN" "web-jsptaglibrary_1_2.dtd" >

<taglib>
    <tlib-version>utils1.0</tlib-version>
    <jsp-version>2.0</jsp-version>
    <short-name>utils</short-name>
    <uri>http://www.cnblogs.com/zhouxyx/utils</uri>

    <tag>
        <name>str</name>
        <tag-class>org.zxy.tutorial.d130819.StringTag</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <name>content</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>count</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>append</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>
</taglib>
```
接下来我们在jsp页面运用我们定义的标签和函数
```
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="u" uri="http://www.cnblogs.com/zhouxyx/utils" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>custom tag</title>
</head>
<body>
    <u:fmt param="yyyy/MM/dd HH:mm:ss" date="${date}" /><br/>
   <hr>
    <u:str append="....." count="20" content="${str }"/>
</body>
</html>
```
一个复杂点的例子

```
package com.xxd.service;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.xxd.core.BaseDao;
/**
 * 查询标签供JSP使用，无需标签体
 * <%@ taglib uri="http://baicai.me/domaintaglib" prefix="dot" %>
 * example：<dot:query table="nd_system" id="2" item="sysm" />${sysm.name} ${sysm.value}
 * @author waitfox@qq.com
 *
 */
public class DomainTag extends SimpleTagSupport {

	private String table;//表名，必须字段
	private Integer id;//主键，可以为空
	private String condition;//条件，可以为空，但 id和condition字段不能同时胃口
	private String item;//标签属性，指定迭代集合元素，为集合元素指定的名称，必须
	private String order;//排序方式，可为空
	private String limit;//条数，可为空

	public void doTag() throws JspException, IOException {
		BaseDao<?> baseDao=(BaseDao) SpringUtils.getApplicationContext().getBean("baseDao");
		String sql="SELECT * FROM "+table+" WHERE 1024 AND ";
		if(id==null || id==0){
			sql+=condition;
		}else{
			sql +=" id="+id;
		}
		if(order!=null){
			sql+=" ORDER BY "+order;
		}
		if(limit!=null){
			sql+= " LIMIT "+limit;
			List list=baseDao.getJdbcTemplate().queryForList(sql);
			getJspContext().setAttribute(item, list);
		}else{
			sql+=" LIMIT 1 ";
			Map map=baseDao.getJdbcTemplate().queryForMap(sql);
			getJspContext().setAttribute(item, map);
		}
		//输出标签体
        //super.getJspContext().getOut().write("xxoo");
	}

	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}

	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
}
```


```
<?xml version="1.0" encoding="UTF-8"?>
<taglib xmlns="http://java.sun.com/xml/ns/j2ee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee web-jsptaglibrary_2_0.xsd"
	version="2.0">
	<tlib-version>1.0</tlib-version>
	<short-name>dot</short-name>
	<!-- 定义该标签库的URI -->
	<uri>http://baicai.me/domaintaglib</uri>
	<!-- 定义第一个标签 -->
	<tag>
		<!-- 定义标签名 -->
		<name>query</name>
		<!-- 定义标签处理类 -->
		<tag-class>com.xxd.service.DomainTag</tag-class>
		<!-- 定义标签体可以是HTML或表达式 -->
		<!-- <body-content>scriptless</body-content> -->
		<body-content>empty</body-content>
		<attribute>
			<name>table</name>
			<required>true</required>
			<fragment>true</fragment>
		</attribute>
		<attribute>
			<name>id</name>
			<required>false</required>
			<fragment>true</fragment>
		</attribute>
		<attribute>
			<name>condition</name>
			<required>false</required>
			<fragment>true</fragment>
		</attribute>
		<attribute>
			<name>order</name>
			<required>false</required>
			<fragment>true</fragment>
		</attribute>
		<attribute>
			<name>limit</name>
			<required>false</required>
			<fragment>true</fragment>
		</attribute>
		<attribute>
			<name>item</name>
			<required>true</required>
			<fragment>true</fragment>
		</attribute>
	</tag>
</taglib>
```

## 自定义JSTL函数

JSTL函数标签库为我们提供了16常用函数，这些函数的使用大大简化了jsp页面代码，同时提高了程序的可维护性，但是你有没有想过既然称之为标签“库”，那么它肯定是可扩展的。通过对函数标签库的扩展我们可以轻松实现自定义的函数标签功能，使程序开发变得更加简便。下面以通过实例手把手教你怎样DIY自己的函数标签。

1.定义自己的功能类URL。注意在该类中将要被使用的方法必须为静态的全局方法。


```
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
/**
 * URL编码，用于jstl
 * @author lenovo
 *
 */
public class URL {
	public static String encode(String param){
        try {
			return URLEncoder.encode(param,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return param;
		}
    }
}
```
2.自定义tld描述文件，然后将该tld文件放置到WEB-INF或WEB-INF下的任意目录中。本例中tld文件内容如下：

```
<?xml version="1.0" encoding="UTF-8" ?>
<taglib xmlns="http://java.sun.com/xml/ns/j2ee"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
  version="2.0">

  <description>baicai function</description>
  <display-name>url function for jsp</display-name>
  <tlib-version>1.1</tlib-version>
  <short-name>url</short-name>
  <uri>http://tagurl.baicai.me</uri>

  <function>
    <name>encode</name>
    <function-class>com.xxd.util.URL</function-class>
    <function-signature>java.lang.String encode(java.lang.String)</function-signature>
  </function>
</taglib>
```
tld描述文件中uri标签中的内容原则上是可以随便写的，但是必须要保证两点，一是该uri必须与其他tld描述文件中的uri不相同，二是在引入的时候jsp页面中的taglib指令中uri属性的值必须与该描述文件中的uri相同。另外，在function标签中，name标签为自定义的函数名，该函数名为使用EL时候所调用的函数名，function-class标签为真正实现该方法的类的完整路径，function-signature标签为该函数所调用的该类中的真正方法，所需要注意的是在该标签中必须写明该方法的返回值类型，函数名以及参数类型。

3.在jsp中采用taglib指令引入函数库，并在EL表达式中采用前置 + 冒号 + 函数名的方式进行调用。


```
<!--采用taglib指令引入函数库，一定要注意该标签中uri属性值必须与tld文件中的uri相一致 -->
<%@ taglib uri="http://tagurl.baicai.me" prefix="url" %>
<title>${url:encode("张三")}</title>
```
