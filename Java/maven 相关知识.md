#### 安装自己的jar到maven仓库

```
mvn install:install-file -Dpackaging=jar -Dfile=smartUtil.jar -DgroupId=com.baicai -DartifactId=smart -Dversion=1.1 
```
然后就可以在项目下的pom.xml 文件中添加

```xml
<dependency>
<groupId>com.baicai</groupId>
<artifactId>smart</artifactId>
<version>1.1</version>
</dependency>
```
#### maven 部署

```
mvn deploy:deploy-file   -Dfile=baiplus-security-1.0-SNAPSHOT.jar -DgroupId=com.jd.jr -DartifactId=baiplus-security -Dversion=1.0-SNAPSHOT -Dpackaging=jar -Durl=http://192.168.17.252:8081/nexus/content/repositories/snapshots  -DrepositoryId=weidai-snapshots
```


#### maven在pom文件里引用本地jar

```xml
方法1：
<dependency>
    <groupId>org.wltea</groupId>
    <artifactId>IKAnalyzer</artifactId>
    <version>2012_u6</version>
    <scope>system</scope>
    <systemPath>E:/repositories/IKAnalyzer2012_u6.jar</systemPath>
</dependency>
法2：
<dependency>
<groupId>org.wltea</groupId>
<artifactId>IKAnalyzer</artifactId>
<version>3.2.8</version>
<scope>system</scope>
<systemPath>${basedir}/mylib/IKAnalyzer-3.2.8.jar</systemPath>
</dependency>
```
#### maven 跳过测试：

```
clean install -Dmaven.test.skip=true -P local
```
#### Maven eclipse 或者myeclipse 报错问题 Check $M2_HOME environment variable and mvn script match.

1.添加M2_HOME的环境变量

2.Preference->Java->Installed JREs->Edit 选择一个jdk,添加  -Dmaven.multiModuleProjectDirectory=$M2_HOME

#### maven 动态修改配置


```
<profiles>
        <!-- 开发 -->
        <profile>
            <id>dev</id>
            <properties>
                <active.profile>dev</active.profile>
            </properties>
        </profile>
        <!-- 测试 -->
        <profile>
            <id>test</id>
            <properties>
                <active.profile>test</active.profile>
            </properties>
            <!-- 把当前profile设置为默认profile，可以同时这是多个为默认-->
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <!-- 生产 -->
        <profile>
            <id>prod</id>
            <properties>
                <active.profile>prod</active.profile>
            </properties>
        </profile>
    </profiles>
    <build>
        <finalName>bwcrm</finalName>

        <filters>
            <filter>package/resources/sso_${active.profile}.properties</filter>
        </filters>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>

        <plugins>
            <!-- jetty插件 -->
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>9.2.8.v20150217</version>
                <configuration>
                    <scanIntervalSeconds>3</scanIntervalSeconds>
                    <webApp>
                        <contextPath>/</contextPath>
                        <resourceBases>
                            <resourceBase>src/main/webapp</resourceBase>
                        </resourceBases>
                        <parentLoaderPriority>true</parentLoaderPriority>
                    </webApp>
                    <jvmArgs>-Xmx1024m -Xms1024m -XX:PermSize=256m -XX:MaxPermSize=256m</jvmArgs>
                    <httpConnector>
                        <port>8125</port>
                    </httpConnector>
                </configuration>
            </plugin>
        </plugins>
    </build>
```
####  默认配置

```
<mirrors>
  <mirror>
       <id>nexus-aliyun</id>
       <mirrorOf>central</mirrorOf>
       <name>Nexus aliyun</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
    </mirror>
   <mirror>
         <id>repo2</id>
         <mirrorOf>central</mirrorOf>
         <name>Human Readable Name for this Mirror.</name>
         <url>http://repo2.maven.org/maven2/</url>
   </mirror>
   <mirror>
       <id>jboss-public-repository-group</id>
       <mirrorOf>central</mirrorOf>
       <name>JBoss Public Repository Group</name>
       <url>http://repository.jboss.org/nexus/content/groups/public</url>
   </mirror>
   <mirror>
       <id>ui</id>
       <mirrorOf>central</mirrorOf>
       <name>Human Readable Name for this Mirror.</name>
       <url>http://uk.maven.org/maven2/</url>
   </mirror>

  </mirrors>

  <profiles>
    <profile>
                <id>jdk-1.7</id>
                <activation>
                    <jdk>1.7</jdk>
                </activation>

                <repositories>
                    <repository>
                        <id>codehausSnapshots</id>
                        <name>Codehaus Snapshots</name>
                    <releases>
                        <enabled>false</enabled>
                        <updatePolicy>always</updatePolicy>
                        <checksumPolicy>warn</checksumPolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>never</updatePolicy>
                        <checksumPolicy>fail</checksumPolicy>
                    </snapshots>
                    <url>http://snapshots.maven.codehaus.org/maven2</url>
                    <layout>default</layout>
                    </repository>
                </repositories>

            </profile>
  </profiles>
```
#### maven 插件开发

先配置pom
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>baicai.me.plugin</groupId>
    <artifactId>self-maven-plugin</artifactId>
    <packaging>maven-plugin</packaging>
    <version>1.0-SNAPSHOT</version>
    <name>upself Maven Mojo</name>
    <url>http://maven.apache.org</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>


    <dependencies>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-plugin-api</artifactId>
            <version>2.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.plugin-tools</groupId>
            <artifactId>maven-plugin-annotations</artifactId>
            <version>3.5</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>

                <plugin>
                    <groupId>baicai.me.plugin</groupId>
                    <artifactId>self-maven-plugin</artifactId>
                    <version>1.0-SNAPSHOT</version>
                </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-plugin-plugin</artifactId>
                <version>3.5</version>
                <executions>
                    <execution>
                        <id>mojo-descriptor</id>
                        <goals>
                            <goal>descriptor</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <goalPrefix>self</goalPrefix>
                    <skipErrorNoDescriptorsFound>true</skipErrorNoDescriptorsFound>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
```
实现代码:
```
package baicai.me.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "self")
public class VersionMojo
        extends AbstractMojo {

    public void execute()
            throws MojoExecutionException {
        String version=VersionTool.getSelfVersion();
        getLog().info("your local maven version is："+version);
        getLog().info("your local maven path is："+VersionTool.getMavenPath());
        getLog().info("-----------update maven for you-----------");
    }
}
```
