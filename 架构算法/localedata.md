localedata 添加民国纪年支持

编辑 /usr/share/i18n/locales/zh_TW 文件，找到 END LC_TIME，在它之前加入这样三行：
```
era "+:2:1913//01//01:+*:民國:%EC%Ey年";/
    "+:1:1912//01//01:1912//12//31:民國:%EC元年";/
    "+:1:1911//12//31:-*:民前:%EC%Ey年"
```
保存后重新 locale-gen 即可。
如果是centos，命令不同，类似localedef -v -c -i zh_CN -f UTF-8 zh_CN.UTF-8

最后执行 
```
export LC_TIME="zh_CN.utf8"
date +%EY
```

