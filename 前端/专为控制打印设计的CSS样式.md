# 专为控制打印设计的CSS样式
## 为打印机而不是屏幕设计的样式

```
/* 样式将只应用于打印 */
@media print {

}
```
注*  也可通单独的CSS文件, 设置link的 media="print" 属性来指定此样式专用于打印

```
<link type="text/css" rel="stylesheet" href="css/print.css" media="print">
```
为您的网站重塑整个CSS是没有必要的，整体而言，由打印继承默认样;仅对不同的需要加以限定。为了节省打印时的碳粉，大多数浏览器会自动反转颜色。为了达到最佳效果，应使色彩变化明显

```
/*白纸黑字*/
@media print {
   body {
      color: #000;
      background: #fff;
   }
}
```
我们不是在创建整个网页的截图,只是为了展现一个设计良好,可读性强的网站:
```
/*去除背景图片, 节约笔黑 */

h1 {
   color: #fff;
   background: url(banner.jpg);
}

@media print {
   h1 {
      color: #000;
      background: none;
   }

   nav, aside {
      display: none;
   }
}
```
为了使打印机更具效率,应只显示主体内容,去除页眉页脚导航栏

```
@media print {
   h1 {
      color: #000;
      background: none;
   }

   nav, aside {
      display: none;
   }

   body, article {
      width: 100%;
      margin: 0;
      padding: 0;
   }

   @page {
      margin: 2cm;
   }
}
```
## 链接的处理
在打印机上链接是看不到的,应对超链接进行扩展

```
/*在超链接后面添加带<http://XXX>的完整地址*/
@media print {
   article a {
      font-weight: bolder;
      text-decoration: none;
   }

   article a[href^=http]:after {
      content:" <" attr(href) "> ";
   }
}
```
## 控制打印设置选项
该@page规则允许您指定页面的各个方面。例如，你将要指定页面的尺寸。页边 距,页眉页脚等都是非常重要的。[很多浏览器均己支持]

## @PAGE规则纸张大小设置

通过下面这条CSS您可以设置纸张大小,5.5英寸宽,8.5英寸高.
```
@page {
  size: 5.5in 8.5in;
}
```
你还可以通过别名控制纸张大小,如"A4"或“legal.”

```
@page {
  size: A4;
}
```
你还可以控制打印方向，　portrait： 纵向打印地,  landscape: 横向

```
@page {
  size: A4 landscape;
}
```
下面的ＣＳＳ将在底部左边显示标题，在右下角的网页计数器，并在右上角显示一章的标题。

```
@page:right{ 
  @bottom-left {
    margin: 10pt 0 30pt 0;
    border-top: .25pt solid #666;
    content: "Our Cats";
    font-size: 9pt;
    color: #333;
  }

  @bottom-right { 
    margin: 10pt 0 30pt 0;
    border-top: .25pt solid #666;
    content: counter(page);
    font-size: 9pt;
  }

  @top-right {
    content:  string(doctitle);
    margin: 30pt 0 10pt 0;
    font-size: 9pt;
    color: #333;
  }
}
```
注* 此文整理自:Tips And Tricks For Print Style Sheets　和　Designing For Print With CSS　和　css3 page 规
